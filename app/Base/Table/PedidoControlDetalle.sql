﻿CREATE TABLE [dbo].[PedidoControlDetalle]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[operacion] INT NOT NULL REFERENCES [AreaOperacion](id),
	[cOperacion] DECIMAL(12,2) NOT NULL,
	[cTOperacion] DECIMAL(12,2) NOT NULL,
	[persona] INT NULL REFERENCES [Persona](id) DEFAULT NULL,
	[fFin] DATE NULL DEFAULT NULL,
	[estado] BIT NOT NULL DEFAULT 0,
	[pedidoControl] INT NOT NULL REFERENCES [PedidoControl](id),
)
