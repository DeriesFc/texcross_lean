﻿CREATE TABLE [dbo].[PedidoDetalle]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[cod] VARCHAR(4) NOT NULL DEFAULT '',
	[modelo] INT NOT NULL REFERENCES [Modelo](id),
	[fEFin] DATE NOT NULL, -- Fecha estimada de terminado
	[pedido] INT NOT NULL REFERENCES [Pedido](id),
)
