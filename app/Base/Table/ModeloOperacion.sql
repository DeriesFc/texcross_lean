﻿CREATE TABLE [dbo].[ModeloOperacion]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[costo]  DECIMAL(12,2) NOT NULL,
	[tiempo] INT NOT NULL,
	[modelo] INT NOT NULL REFERENCES [Modelo](id),
	[operacion] INT NOT NULL REFERENCES [AreaOperacion](id),
)
