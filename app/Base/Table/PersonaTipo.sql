﻿CREATE TABLE [dbo].[PersonaTipo]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[nombre] VARCHAR(80) NOT NULL,
	[acceso] BIT NOT NULL
)
