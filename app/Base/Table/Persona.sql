﻿CREATE TABLE [dbo].[Persona]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[dni] VARCHAR(8) NOT NULL,
	[nombre] VARCHAR(80) NOT NULL,
	[apellido] VARCHAR(80) NOT NULL,
	[clave] VARCHAR(80) NOT NULL,
	[tipo] INT NOT NULL REFERENCES [PersonaTipo](id),
	[area] INT NOT NULL REFERENCES [Area](id),
	[estado] BIT NOT NULL DEFAULT 1
)
