﻿CREATE TABLE [dbo].[PedidoControl]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[cod] VARCHAR(8) NOT NULL DEFAULT '',
	[cantidad] INT NOT NULL,
	[cInventario] DECIMAL(12,2) NOT NULL,
	[cTInventario] DECIMAL(12,2) NOT NULL,
	[pedidoDetalle] INT NOT NULL REFERENCES [PedidoDetalle](id),

)
