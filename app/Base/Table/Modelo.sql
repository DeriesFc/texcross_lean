﻿CREATE TABLE [dbo].[Modelo]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[tipo] INT NOT NULL REFERENCES [ModeloTipo](id),
	[nombre] VARCHAR(250) NOT NULL,
	[talla] INT NOT NULL REFERENCES [Talla](id),
	[color] INT NOT NULL REFERENCES [Color](id),
	[creador] INT NOT NULL REFERENCES [Cliente](id),
	[estado] BIT NOT NULL DEFAULT 1
)
