﻿CREATE TABLE [dbo].[ModeloSuministro]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[cantidad] DECIMAL(12,2) NOT NULL,
	[modelo] INT NOT NULL REFERENCES [Modelo](id),
	[suministro] INT NOT NULL REFERENCES [Suministro](id),
)
