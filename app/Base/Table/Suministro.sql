﻿CREATE TABLE [dbo].[Suministro]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[tipo] INT NOT NULL REFERENCES [SuministroTipo](id),
	[nombre] VARCHAR(250) NOT NULL,
	[color] INT NOT NULL REFERENCES [Color](id),
	[unidad] INT NOT NULL REFERENCES [Unidad](id),
	[cantidad] DECIMAL(12,2) NOT NULL,
	[precio] DECIMAL(12,2) NOT NULL,
	[estado] BIT NOT NULL DEFAULT 1,
)
