﻿/* Color */
	
	INSERT INTO [dbo].[Color] ([nombre]) VALUES
	('Agua Marino'),
	('Ama'),
	('Amarillo Bandera'),
	('Amarillo Cremoso'),
	('Amarillo Medio'),
	('Amarillo'),
	('Aqua'), 
	('Aruba'), 
	('Azul Marino'), 
	('Azul Navy'), 
	('Azul Oscuro'), 
	('Azul Rey'), 
	('Azul'),
	('Azulino Royal'),
	('Blanco'),
	('CDRT05-Coral'),
	('Celeste'),
	('Chicle Malibu'),
	('Coral'),
	('Crema'),
	('CRT03-ROSADO'),
	('CRT09-Azul'),
	('Fucsia'),
	('Gris Azulado'),
	('Gris Oscuro'),
	('Hortencia'),
	('Hot Pink'),
	('Indigo'),
	('Jaspe Gris'),
	('JS 03'),
	('JS 04'),
	('JS 05'),
	('JS 08'),
	('JS 09'),
	('JS 10'),
	('Lavande'),
	('Lila'),
	('Living Coral'),
	('Mandarina'),
	('Melange'),
	('Melon'),
	('Mostaza'),
	('Negro Caviar'),
	('Negro Intenso'),
	('Negro'),
	('P12 Jaspeado'),
	('Plomo Rata'),
	('Rojo Oscuro'),
	('Rojo Vino'),
	('Rojo'),
	('Rosado Claro'),
	('Rosado Fashion'),
	('Salmon'),
	('Sky Blue'),
	('Turquesa Medio'),
	('Turquesa'),
	('Verde Antioquia'),
	('Verde Azul'),
	('Verde Marino Pascali'),
	('Verde Marino'),
	('Verde Menta'),
	('Verde Militar'),
	('Verde Perico'),
	('Verde Petroleo'),
	('Verde'),
	('Vino Oscuro Pascali'),
	('Vino Pascali'),
	('Vinotinto'),
	('Violeta Pascali'),
	('Violeta'),
	('Perla'),
	('Ninguno');

/* Suministro Tipo */

	INSERT INTO [dbo].[SuministroTipo] ([nombre]) VALUES
	('Agujas'),
	('Balin'),
	('Bolsas'),
	('Boton'),
	('Cinta'),
	('Cuellos'),
	('Etiqueta'),
	('Hantag'),
	('Hilo'),
	('Mangas'),
	('Pasadores'),
	('Pitas'),
	('Tela'),
	('Cierre');


/* Unidad */
	
	INSERT INTO [dbo].[Unidad] ([nombre], [simbolo]) VALUES
	('unidad', 'und'),
	('kilos', 'kg'),
	('metros', 'm');

/* Producto Modelo */
	
	INSERT INTO [dbo].[ModeloTipo]([nombre]) VALUES
	('T-Shirt C/V'),
	('T-Shirt C/R'),
	('Polo Box');

/* Talla Tipo */

	INSERT INTO [dbo].[TallaTipo]([nombre]) VALUES
	('Stantard'),
	('Niños'),
	('Extra'),
	('Colombia'),
	('Brazil');

/* Talla */

	INSERT INTO [dbo].[Talla]([nombre], [tipo]) VALUES
	('XS', 1),
	('S', 1),
	('M', 1),
	('L', 1),
	('XL', 1),
	('8', 2),
	('10', 2),
	('12', 2),
	('14', 2),
	('16', 2),
	('XL', 3),
	('XXL', 3),
	('XXXL', 3),
	('32', 4),
	('34', 4),
	('36', 4),
	('38', 4),
	('40', 4),
	('S/M', 5),
	('L/XL', 5);

/* Cliente */

	INSERT INTO [dbo].[Cliente] ([ruc], [nombre]) VALUES
	('123465789', 'Polo 1'),
	('123456799', 'L.A Nueva York'),
	('123456778', 'Brandon'),
	('123456689', 'Olimpica'),
	('123455678', 'Thelo'),
	('123445678', 'Cross');

/* AreaTipo */

	INSERT INTO [dbo].[AreaTipo] ([nombre]) VALUES ('TI'), ('Administrativa'), ('Producción');

/* Area */

	INSERT INTO [dbo].[Area] ([nombre], [tipo]) VALUES
	('Sistemas', 1), --1
	('Gerencia', 2), -- 2
	('Comercial', 2), -- 3
	('Planeamiento', 2), -- 4
	('Logistica', 2), -- 5
	('Corte', 3), -- 6
	('Costura', 3), -- 7
	('Acabado', 3), -- 8
	('Despacho', 3); -- 9

/* Area Operacion */

	INSERT INTO [dbo].[AreaOperacion] ([nombre], [area]) VALUES
	('Tendido', 6),
	('Tizado', 6),
	('Corte', 6),
	('P.Pechera', 7),
	('E. de Cuello', 7),
	('O. de Cogotera', 7),
	('P. Etiqueta', 7),
	('P. de Cogotera', 7),
	('U. de Hombros', 7),
	('P. de Hombros', 7),
	('P. de Cuello', 7),
	('A. de Cuello', 7),
	('P. de Pechera', 7),
	('A. de Pechera', 7),
	('P. de puño', 7),
	('Ps. de puño', 7),
	('P. Manga', 7),
	('Ps. Siza', 7),
	('C. costado', 7),
	('Venz', 7),
	('B. Faldon Abierto', 7),
	('Ojal,marcado y boton', 7),
	('Limpiesa', 8),
	('Embolsado', 8),
	('Entregado', 9);
	
/* Suministro */
	
	INSERT INTO [dbo].[Suministro]([tipo], [nombre], [color], [unidad], [cantidad], [precio]) VALUES
	(1,	'COS DB Nº 6', 72, 1, 1, 1.00),
	(1,	'COS DB Nº 9', 72, 1, 1, 1.00),
	(1,	'COS DB Nº 10', 72, 1, 1, 1.00),
	(1,	'COS DB Nº 11', 72, 1, 1, 1.00),
	(1,	'REM Nº 500', 72, 1, 1, 1.00),
	(1,	'REM DC x 27', 72, 1, 1, 1.00),
	(2,	'Plastico', 72, 1, 1, 0.01),
	(3,	'15 x 30', 72, 1, 1, 0.01),
	(3,	'12 x 36', 72, 1, 1, 0.01),
	(3,	'8 x 12', 72, 1, 1, 0.01),
	(4,	'B 12', 71,	1, 1, 0.02),
	(4,	'B 101', 71, 1, 1, 0.02),
	(4,	'B 102', 71, 1, 1, 0.02),
	(4,	'B 500', 15, 1, 1, 0.02),
	(4,	'B 501', 71, 1, 1, 0.02),
	(4,	'B200', 71,	1, 1, 0.02),
	(5,	'C 3cm', 13, 1, 1, 0.8),
	(5,	'C 2cm', 50, 1, 1, 0.5),
	(5,	'C 1cm', 13, 1, 1, 0.45),
	(6,	'Cesgo 1cm - Azul', 15, 1, 1, 0.5),
	(6,	'Cesgo 1cm - Negro', 15, 1, 1, 0.5),
	(6,	'Cesgo doble 1cm - Azul', 22, 1, 1, 0.6),
	(7,	'Polo club', 72, 1, 1, 0.15),
	(7,	'L.A nueva York', 72, 1, 1, 0.15),
	(7,	'Pascali', 72, 1, 1, 0.15),
	(7,	'Bruss', 72, 1, 1, 0.15),
	(7,	'Tradizi', 72, 1, 1, 0.15),
	(8,	'Polo club', 72, 1, 1, 0.03),
	(8,	'L.A nueva York', 72, 1, 1, 0.03),
	(8,	'Pascali', 72, 1, 1, 0.03),
	(8,	'Bruss', 72, 1, 1, 0.03),
	(8,	'Tradizi', 72, 1, 1, 0.03),
	(9,	'Bordado', 14, 1, 1, 0.06),
	(9,	'Rino Poliester', 13, 3, 1, 0.04),
	(10, 'Cesgo doble 0,5 cm - Negro', 45, 1, 1, 0.5),
	(10, 'Cesgo doble 0,5 cm - Azul', 22, 1, 1, 0.5),
	(11, '1 m', 45, 1, 1, 0.35),
	(12, 'Brillosa', 4, 1, 1, 0.2),
	(13, 'Pique 26/1', 14, 2, 1, 26.00),
	(13, 'Pique 24/1', 37, 2, 1, 24.00),
	(13, 'Algodón',  13, 2, 1, 32.00),
	(13, 'Jersey 30/1',	54,	2, 1, 28.00),
	(13, 'Lycra', 36, 2, 1, 21.00),
	(14, '3 cm', 45, 1, 1, 0.5),
	(14, '5 cm', 45, 1, 1, 0.6),
	(14, '6 cm', 45, 1, 1, 0.7),
	(14, '8 cm', 45, 1, 1, 0.8),
	(14, '10 cm', 45, 1, 1, 0.9),
	(14, '12 cm', 45, 1, 1, 1.0),
	(14, '15 cm', 45, 1, 1, 1.1);


/* Modelo */

	INSERT INTO [dbo].[Modelo]([tipo], [nombre], [talla], [color], [creador]) VALUES
	(3,	'Casual Hombre', 1, 9, 1),
	(3,	'Casual Hombre', 2, 9, 1),
	(3,	'Casual Hombre', 3, 9, 1),
	(3,	'Casual Hombre', 4, 9, 1),
	(3,	'Casual Hombre', 5, 9, 1),
	(3,	'Casual Hombre - PV', 2, 68, 6),
	(3,	'Casual Hombre - PV', 3, 68, 6),
	(3,	'Casual Hombre - PV', 4, 68, 6),
	(3,	'Casual Hombre - PV', 5, 68, 6),
	(3,	'Polo Box Basico', 1, 4, 1),
	(3,	'Polo Box Basico', 2, 4, 1),
	(3,	'Polo Box Basico', 3, 4, 1),
	(3,	'Polo Box Basico', 4, 4, 1),
	(3,	'Polo Box Basico', 5, 4, 1),
	(3,	'Polo Box Seton', 1, 13, 1),
	(3,	'Polo Box Seton', 2, 13, 1),
	(3,	'Polo Box Seton', 3, 13, 1),
	(3,	'Polo Box Seton', 4, 13, 1),
	(3,	'Polo Box Seton', 5, 13, 1),
	(3,	'Polo Box Franjas', 1, 70, 1),
	(3,	'Polo Box Franjas', 2, 70, 1),
	(3,	'Polo Box Franjas', 3, 70, 1),
	(3,	'Polo Box Franjas', 4, 70, 1),
	(3,	'Polo Box Franjas', 5, 70, 1);


/* Modelo Suministro */
	
	INSERT INTO [dbo].[ModeloSuministro]([cantidad], [modelo], [suministro]) VALUES
	(0.50, 1, 42),
	(0.09, 1, 11),
	(0.04, 1, 34),
	(0.50, 1, 36),
	(0.50, 2, 42),
	(0.09, 2, 11),
	(0.04, 2, 34),
	(0.50, 2, 36),
	(0.50, 3, 42),
	(0.09, 3, 11),
	(0.04, 3, 34),
	(0.50, 3, 36),
	(0.50, 4, 42),
	(0.09, 4, 11),
	(0.04, 4, 34),
	(0.50, 4, 36),
	(0.50, 5, 42),
	(0.09, 5, 11),
	(0.04, 5, 34),
	(0.50, 5, 36),
	(0.50, 6, 41),
	(0.09, 6, 15),
	(0.04, 6, 34),
	(0.50, 6, 22),
	(0.50, 6, 36),
	(0.50, 7, 41),
	(0.09, 7, 15),
	(0.04, 7, 34),
	(0.50, 7, 22),
	(0.50, 7, 36),
	(0.50, 8, 41),
	(0.09, 8, 15),
	(0.04, 8, 34),
	(0.50, 8, 22),
	(0.50, 8, 36),
	(0.50, 9, 41),
	(0.09, 9, 15),
	(0.04, 9, 34),
	(0.50, 9, 22),
	(0.50, 9, 36),
	(0.50, 10, 39),
	(0.09, 10, 15),
	(0.04, 10, 34),
	(0.50, 10, 22),
	(0.50, 10, 36),
	(0.50, 11, 39),
	(0.09, 11, 15),
	(0.04, 11, 34),
	(0.50, 11, 22),
	(0.50, 11, 36),
	(0.50, 12, 39),
	(0.09, 12, 15),
	(0.04, 12, 34),
	(0.50, 12, 22),
	(0.50, 12, 36),
	(0.50, 13, 39),
	(0.09, 13, 15),
	(0.04, 13, 34),
	(0.50, 13, 22),
	(0.50, 13, 36),
	(0.50, 14, 39),
	(0.09, 14, 15),
	(0.04, 14, 34),
	(0.50, 14, 22),
	(0.50, 14, 36);


/* Operacion Suministro */

	INSERT INTO [dbo].[ModeloOperacion]([costo], [tiempo], [modelo], [operacion]) VALUES
	(5, 7.2, 1, 1),
	(3, 3.6, 1, 2),
	(4, 5.4, 1, 3),
	(0.09, 7, 1, 4),
	(0.08, 6, 1, 5),
	(0.07, 9, 1, 7),
	(0.045, 8, 1, 9),
	(0.035, 5, 1, 10),
	(0.15, 15, 1, 13),
	(0.13, 12, 1, 14),
	(0.09, 8, 1, 17),
	(0.13, 12, 1, 19),
	(0.11, 15, 1, 21),
	(0.08, 8, 1, 23),
	(0.02, 4, 1, 24),
	(0, 0, 1, 25),
	(5, 7.2, 2, 1),
	(3, 3.6, 2, 2),
	(4, 5.4, 2, 3),
	(0.09, 7, 2, 4),
	(0.08, 6, 2, 5),
	(0.07, 9, 2, 7),
	(0.045, 8, 2, 9),
	(0.035, 5, 2, 10),
	(0.15, 15, 2, 13),
	(0.13, 12, 2, 14),
	(0.09, 8, 2, 17),
	(0.13, 12, 2, 19),
	(0.11, 15, 2, 21),
	(0.08, 8, 2, 23),
	(0.02, 4, 2, 24),
	(0, 0, 2, 25),
	(5, 7.2, 3, 1),
	(3, 3.6, 3, 2),
	(4, 5.4, 3, 3),
	(0.09, 7, 3, 4),
	(0.08, 6, 3, 5),
	(0.07, 9, 3, 7),
	(0.045, 8, 3, 9),
	(0.035, 5, 3, 10),
	(0.15, 15, 3, 13),
	(0.13, 12, 3, 14),
	(0.09, 8, 3, 17),
	(0.13, 12, 3, 19),
	(0.11, 15, 3, 21),
	(0.08, 8, 3, 23),
	(0.02, 4, 3, 24),
	(0, 0, 3, 25),
	(5, 7.2, 4, 1),
	(3, 3.6, 4, 2),
	(4, 5.4, 4, 3),
	(0.09, 7, 4, 4),
	(0.08, 6, 4, 5),
	(0.07, 9, 4, 7),
	(0.045, 8, 4, 9),
	(0.035, 5, 4, 10),
	(0.15, 15, 4, 13),
	(0.13, 12, 4, 14),
	(0.09, 8, 4, 17),
	(0.13, 12, 4, 19),
	(0.11, 15, 4, 21),
	(0.08, 8, 4, 23),
	(0.02, 4, 4, 24),
	(0, 0, 4, 25),
	(5, 7.2, 5, 1),
	(3, 3.6, 5, 2),
	(4, 5.4, 5, 3),
	(0.09, 7, 5, 4),
	(0.08, 6, 5, 5),
	(0.07, 9, 5, 7),
	(0.045, 8, 5, 9),
	(0.035, 5, 5, 10),
	(0.15, 15, 5, 13),
	(0.13, 12, 5, 14),
	(0.09, 8, 5, 17),
	(0.13, 12, 5, 19),
	(0.11, 15, 5, 21),
	(0.08, 8, 5, 23),
	(0.02, 4, 5, 24),
	(0, 0, 5, 25),
	(5, 7.2, 6, 1),
	(3, 3.6, 6, 2),
	(4, 5.4, 6, 3),
	(0.07, 7, 6, 4),
	(0.08, 6, 6, 5),
	(0.07, 9, 6, 7),
	(0.8, 7, 6,	8),
	(0.045, 8, 6, 9),
	(0.035, 5, 6, 10),
	(0.15, 15, 6, 13),
	(0.13, 12, 6, 14),
	(0.07, 8, 6, 17),
	(0.13, 12, 6, 19),
	(0.09, 15, 6, 21),
	(0.06, 6, 6, 22),
	(0.08, 8, 6, 23),
	(0.02, 4, 6, 24),
	(0, 0, 6, 25),
	(5, 7.2, 7, 1),
	(3, 3.6, 7, 2),
	(4, 5.4, 7, 3),
	(0.07, 7, 7, 4),
	(0.08, 6, 7, 5),
	(0.07, 9, 7, 7),
	(0.8, 7, 7,	8),
	(0.045, 8, 7, 9),
	(0.035, 5, 7, 10),
	(0.15, 15, 7, 13),
	(0.13, 12, 7, 14),
	(0.07, 8, 7, 17),
	(0.13, 12, 7, 19),
	(0.09, 15, 7, 21),
	(0.06, 6, 7, 22),
	(0.08, 8, 7, 23),
	(0.02, 4, 7, 24),
	(0, 0, 7, 25),
	(5, 7.2, 8, 1),
	(3, 3.6, 8, 2),
	(4, 5.4, 8, 3),
	(0.07, 7, 8, 4),
	(0.08, 6, 8, 5),
	(0.07, 9, 8, 7),
	(0.8, 7, 8,	8),
	(0.045, 8, 8, 9),
	(0.035, 5, 8, 10),
	(0.15, 15, 8, 13),
	(0.13, 12, 8, 14),
	(0.07, 8, 8, 17),
	(0.13, 12, 8, 19),
	(0.09, 15, 8, 21),
	(0.06, 6, 8, 22),
	(0.08, 8, 8, 23),
	(0.02, 4, 8, 24),
	(0, 0, 8, 25),
	(5, 7.2, 9, 1),
	(3, 3.6, 9, 2),
	(4, 5.4, 9, 3),
	(0.07, 7, 9, 4),
	(0.08, 6, 9, 5),
	(0.07, 9, 9, 7),
	(0.8, 7, 9,	8),
	(0.045, 8, 9, 9),
	(0.035, 5, 9, 10),
	(0.15, 15, 9, 13),
	(0.13, 12, 9, 14),
	(0.07, 8, 9, 17),
	(0.13, 12, 9, 19),
	(0.09, 15, 9, 21),
	(0.06, 6, 9, 22),
	(0.08, 8, 9, 23),
	(0.02, 4, 9, 24),
	(0, 0, 9, 25),
	(5, 7.2, 10, 1),
	(3, 3.6, 10, 2),
	(4, 5.4, 10, 3),
	(0.07, 7, 10, 4),
	(0.08, 6, 10, 5),
	(0.01, 5, 10, 6),
	(0.07, 9, 10, 7),
	(0.8, 7, 10,	8),
	(0.045, 8, 10, 9),
	(0.035, 5, 10, 10),
	(0.1, 8, 10,	11),
	(0.09, 8, 10, 12),
	(0.15, 15, 10, 13),
	(0.13, 12, 10, 14),
	(0.054, 6, 10, 15),
	(0.047, 4, 10, 16),
	(0.07, 8, 10, 17),
	(0.06, 6, 10, 18),
	(0.13, 12, 10, 19),
	(0.11, 12, 10, 20),
	(0.09, 15, 10, 21),
	(0.06, 6, 10, 22),
	(0.08, 8, 10, 23),
	(0.02, 4, 10, 24),
	(0, 0, 10, 25),
	(5, 7.2, 11, 1),
	(3, 3.6, 11, 2),
	(4, 5.4, 11, 3),
	(0.07, 7, 11, 4),
	(0.08, 6, 11, 5),
	(0.01, 5, 11, 6),
	(0.07, 9, 11, 7),
	(0.8, 7, 11,	8),
	(0.045, 8, 11, 9),
	(0.035, 5, 11, 10),
	(0.1, 8, 11,	11),
	(0.09, 8, 11, 12),
	(0.15, 15, 11, 13),
	(0.13, 12, 11, 14),
	(0.054, 6, 11, 15),
	(0.047, 4, 11, 16),
	(0.07, 8, 11, 17),
	(0.06, 6, 11, 18),
	(0.13, 12, 11, 19),
	(0.11, 12, 11, 20),
	(0.09, 15, 11, 21),
	(0.06, 6, 11, 22),
	(0.08, 8, 11, 23),
	(0.02, 4, 11, 24),
	(0, 0, 11, 25),
	(5, 7.2, 12, 1),
	(3, 3.6, 12, 2),
	(4, 5.4, 12, 3),
	(0.07, 7, 12, 4),
	(0.08, 6, 12, 5),
	(0.01, 5, 12, 6),
	(0.07, 9, 12, 7),
	(0.8, 7, 12,	8),
	(0.045, 8, 12, 9),
	(0.035, 5, 12, 10),
	(0.1, 8, 12,	11),
	(0.09, 8, 12, 12),
	(0.15, 15, 12, 13),
	(0.13, 12, 12, 14),
	(0.054, 6, 12, 15),
	(0.047, 4, 12, 16),
	(0.07, 8, 12, 17),
	(0.06, 6, 12, 18),
	(0.13, 12, 12, 19),
	(0.11, 12, 12, 20),
	(0.09, 15, 12, 21),
	(0.06, 6, 12, 22),
	(0.08, 8, 12, 23),
	(0.02, 4, 12, 24),
	(0, 0, 12, 25),
	(5, 7.2, 13, 1),
	(3, 3.6, 13, 2),
	(4, 5.4, 13, 3),
	(0.07, 7, 13, 4),
	(0.08, 6, 13, 5),
	(0.01, 5, 13, 6),
	(0.07, 9, 13, 7),
	(0.8, 7, 13,	8),
	(0.045, 8, 13, 9),
	(0.035, 5, 13, 10),
	(0.1, 8, 13,	11),
	(0.09, 8, 13, 12),
	(0.15, 15, 13, 13),
	(0.13, 12, 13, 14),
	(0.054, 6, 13, 15),
	(0.047, 4, 13, 16),
	(0.07, 8, 13, 17),
	(0.06, 6, 13, 18),
	(0.13, 12, 13, 19),
	(0.11, 12, 13, 20),
	(0.09, 15, 13, 21),
	(0.06, 6, 13, 22),
	(0.08, 8, 13, 23),
	(0.02, 4, 13, 24),
	(0, 0, 13, 25),
	(5, 7.2, 14, 1),
	(3, 3.6, 14, 2),
	(4, 5.4, 14, 3),
	(0.07, 7, 14, 4),
	(0.08, 6, 14, 5),
	(0.01, 5, 14, 6),
	(0.07, 9, 14, 7),
	(0.8, 7, 14, 8),
	(0.045, 8, 14, 9),
	(0.035, 5, 14, 10),
	(0.1, 8, 14, 11),
	(0.09, 8, 14, 12),
	(0.15, 15, 14, 13),
	(0.13, 12, 14, 14),
	(0.054, 6, 14, 15),
	(0.047, 4, 14, 16),
	(0.07, 8, 14, 17),
	(0.06, 6, 14, 18),
	(0.13, 12, 14, 19),
	(0.11, 12, 14, 20),
	(0.09, 15, 14, 21),
	(0.06, 6, 14, 22),
	(0.08, 8, 14, 23),
	(0.02, 4, 14, 24),
	(0, 0, 14, 25);

/* PersonaTipo */

	INSERT INTO [dbo].[PersonaTipo] ([nombre], [acceso]) VALUES
	('Root', 1),			--| 1 adm. sistemas y gerente //Todo
	('Gerencia', 1),		--| 2 adm. sistemas y gerente //Solo puede ver reportes
	('Comercial', 1),		-- 3 registra pedido // Crear pedido
	('Planeamiento', 1),	-- 4 arma el pedido //Crea modelos
	('Logistica', 1),		--| 5 provee suministros de almacen // Ver almacen
	('Despachador', 1),		--| 6 salida de pedido //nada
	('Jefe de área', 1),	-- 7 registre los avances //Proyectos
	('Obrero', 0);			-- 8 

/* Persona */
	
	INSERT INTO [dbo].[Persona] ([dni], [nombre], [apellido], [clave], [tipo], [area]) VALUES
	('00000001', 'Kelly Stephanie',	'Ugarte Gil',	'123456',	1,	1 ),
	('00000002', 'Gerente',			'Gerente',		'123456',	2,	2 ),
	('00000003', 'Comercial',		'Comercial',	'123456',	3,	3 ),
	('00000004', 'Planeamiento',	'Planeamiento', '123456',	4,	4 ),
	('00000005', 'Logistica',		'Logistica',	'123456',	5,	5 ),
	('00000006', 'Jefe',			'Enrique Peña',		'123456',	7,	6 ),
	('00000007', 'Frank',		' dias',		'123456',	8,	6 ),
	('00000008', 'Luis',		'Prospero',		'123456',	8,	6 ),
	('00000009', 'Luis',		'Manrique',		'123456',	8,	6 ),
	('00000010', 'Jefe',			'Raul Castillo',	'123456',	7,	7 ),
	('00000011', 'Mirian',		' Hernandez',	'123456',	8,	7 ),
	('00000012', 'Rosmery',		' Canche',	'123456',	8,	7 ),
	('00000013', 'Victor',		' Quispe',	'123456',	8,	7 ),
	('00000014', 'Carolina',		' Ortiz',	'123456',	8,	7 ),
	('00000015', 'Yessica',		' Tapia',	'123456',	8,	7 ),
	('00000016', 'Jorge',		' Gutierrez',	'123456',	8,	7 ),
	('00000017', 'Fortunato',		' Sierra',	'123456',	8,	7 ),
	('00000018', 'Marco',		'Godoy',	'123456',	8,	7 ),
	('00000019', 'Maria',		' Ratiti',	'123456',	8,	7 ),
	('00000020', 'Sara',		' Silva',	'123456',	8,	7 ),
	('00000021', 'Carlos',		' Marquez',	'123456',	8,	7 ),
	('00000022', 'Joel',		' Ponte',	'123456',	8,	7 ),
	('00000023', 'Carlos',		' Quispe',	'123456',	8,	7 ),
	('00000024', 'Maria',		' Raqui',	'123456',	8,	7 ),
	('00000025', 'Sandra',		'Belarde',	'123456',	8,	7 ),
	('00000026', 'Ronald',		'Tayta',	'123456',	8,	7 ),
	('00000027', 'Carmen',		'Ugaz',	'123456',	8,	7 ),
	('00000028', 'Martin',		'Merced',	'123456',	8,	7 ),
	('00000030', 'Estiben',		'Solis',	'123456',	8,	7 ),
	('00000031', 'Bill',		'Puri',	'123456',	8,	7 ),
	('00000032', 'Jefe',			'Jose Luis',	'123456',	7,	8 ),
	('00000033', 'Jorge',		'Muñoz',	'123456',	8,	8 ),
	('00000034', 'Tony',		'Meneces',	'123456',	8,	8 ),
	('00000035', 'Rita',		'Suarez',	'123456',	8,	8 ),
	('00000036', 'Patricia',			'Larco',	'123456',	7,	9 ),
	('00000037', 'Luis',		' Muñoz',	'123456',	8,	9 ),
	('00000038', 'Noelia',		' Martinez',	'123456',	8,	9 ),
	('00000039', 'Rosendo',		'Carhuancho',	'123456',	8,	9 );


	/* Pedido */

	CREATE TYPE [dbo].[TP_PedidoDetalle] AS TABLE(
		[modelo] int not null,
		[cantidad] int not null,
		[fEfin] varchar(20) not null
	)
	Go

	CREATE PROCEDURE [dbo].[Pedido_Ins]
	(
		@cliente INT,
		@fInicio DATE,
		@detalle TP_PedidoDetalle Readonly,
		@precio DECIMAL(12,2)
	)
	AS
	BEGIN

		-- Configuration of procedure
		SET NOCOUNT ON;
	
		-- Variable to response
		DECLARE @Pedido TABLE ( id INT, cod VARCHAR(20), msg VARCHAR(250) DEFAULT '');

		-- Transaction
		BEGIN TRANSACTION
		BEGIN TRY
		
			-- INSERT Pedido
			INSERT INTO [dbo].[Pedido] (cliente, fInicio, precio) 
			OUTPUT INSERTED.ID, '', '' into @Pedido
			VALUES (@cliente, @fInicio, @precio)
		
			-- Generate Id of Pedido
			DECLARE @P_id int = (SELECT TOP 1 id FROM @Pedido);
		
			-- Update code
			DECLARE @P_cod VARCHAR(20) = (SELECT TOP 1 CONCAT('OP', RIGHT(CONCAT('00000000', @P_id), 8)));
			UPDATE Pedido SET cod = @P_cod WHERE id = @P_id;
			UPDATE @Pedido SET cod = @P_cod WHERE id = @P_id;

			-- PedidoDetalle Vars
			DECLARE @TP_PD_num int = 1;
			DECLARE @TP_PD_modelo int;
			DECLARE @TP_PD_cantidad int;
			DECLARE @TP_PD_fEFin varchar(20);
			DECLARE @T_PedidoDetalle TABLE (id int);

			DECLARE CS_PDetalle CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR SELECT modelo, cantidad, fEfin FROM @detalle
			OPEN CS_PDetalle
			FETCH NEXT FROM CS_PDetalle INTO @TP_PD_modelo, @TP_PD_cantidad, @TP_PD_fEFin
			WHILE @@FETCH_STATUS = 0
			BEGIN 
			
				-- Clean TTable of PedidoDetalle
				DELETE @T_PedidoDetalle;

				-- Insert PedidoDetalle
				INSERT INTO PedidoDetalle (cod, modelo, fEFin, pedido)
				OUTPUT INSERTED.ID into @T_PedidoDetalle
				VALUES (RIGHT(CONCAT('000', @TP_PD_num), 3), @TP_PD_modelo, @TP_PD_fEFin, @P_id);

				-- Generate Id of PedidoDetalle
				DECLARE @PD_id int = (SELECT TOP 1 id FROM @T_PedidoDetalle);

				-- PedidoControl Var
				DECLARE @TP_PC_cInventario DECIMAL(12,2) = (
					Select CAST(SUM(MS.cantidad * S.precio) as DECIMAL(12,2)) 
					FROM 
						[dbo].[ModeloSuministro] MS 
						inner join [dbo].[Suministro] S on S.id = MS.suministro
					WHERE MS.modelo = @TP_PD_modelo
				);

				DECLARE @T_PedidoControl TABLE (id int);

				DECLARE @TP_PC_num int = 1;

				WHILE @TP_PD_cantidad > 0
				BEGIN
				
					-- Clean PedidoControl
					DELETE @T_PedidoControl;

					DECLARE @TP_PC_cantidad int = IIF(@TP_PD_cantidad >= 100 , 100, @TP_PD_cantidad);

					INSERT INTO PedidoControl (cod, cantidad, cInventario, cTInventario, pedidoDetalle) 
					OUTPUT INSERTED.ID INTO @T_PedidoControl
					VALUES (
						RIGHT(CONCAT('000000', @TP_PC_num), 6),
						@TP_PC_cantidad,  
						@TP_PC_cInventario, 
						CAST( @TP_PC_cInventario * @TP_PC_cantidad AS DECIMAL(12,2)),
						@PD_id);

					DECLARE @PC_id INT = (SELECT TOP 1 id FROM @T_PedidoControl);

					INSERT INTO PedidoControlDetalle (operacion, cOperacion, cTOperacion, pedidoControl)
					SELECT MO.operacion, MO.costo, CAST(MO.costo * @TP_PC_cantidad AS DECIMAL(12,2)), @PC_id
					FROM ModeloOperacion MO WHERE MO.modelo = @TP_PD_modelo;

					SET @TP_PC_num = @TP_PC_num + 1;

					SET @TP_PD_cantidad = @TP_PD_cantidad - 100;

				END

				SET @TP_PD_num = @TP_PD_num + 1;

				FETCH NEXT FROM CS_PDetalle INTO @TP_PD_modelo, @TP_PD_cantidad, @TP_PD_fEFin

			END
			CLOSE CS_PDetalle
			DEALLOCATE CS_PDetalle
		
			COMMIT TRANSACTION

		END TRY
		BEGIN CATCH
		
			IF @@TRANCOUNT > 0 ROLLBACK
			DELETE @Pedido;
			INSERT INTO @Pedido (id, cod, msg) VALUES (0, '', ERROR_MESSAGE());

		END CATCH

		-- Response
		SELECT TOP 1 id, cod, msg from @Pedido;

	END;
	Go

	CREATE PROCEDURE [PedidoDetalleControlDetalle_Terminar]
	(
		@id int,
		@persona int
	)
	AS
	BEGIN
		UPDATE PedidoControlDetalle
			SET
				persona = @persona,
				fFin = GETDATE(),
				estado = 1
			WHERE id = @id;
	END