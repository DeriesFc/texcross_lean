﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgColor
    {
        public class Color
        {
            public IList<entColor.Color> Lis()
            {
                try
                {
                    IList<entColor.Color> r = new dbColor.Color().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entColor.Color Get(int id)
            {
                try
                {
                    entColor.Color r = new dbColor.Color().Get(id);
                    if (r is null) { throw new Personalized.PException("Color no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entColor.Color Ins(entColor.Color obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entColor.Color r = new dbColor.Color().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entColor.Color Upd(entColor.Color obj)
            {
                try
                {
                    entColor.Color r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbColor.Color().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Color no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }
    }
}
