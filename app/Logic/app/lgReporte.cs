﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgReporte
    {

        public class General
        {
            public IList<entReporte.General> Lis (DateTime start, DateTime end)
            {
                try { return new dbReporte.General().Lis(start, end); }
                catch (Exception) { throw; }
            }
        }

        public class ColaboradorPago
        {
            public IList<entReporte.ColaboradorPago> Lis(DateTime start, DateTime end)
            {
                try { return new dbReporte.ColaboradorPago().Lis(start, end); }
                catch (Exception) { throw; }
            }
        }

    }
}
