﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgUnidad
    {
        public class Unidad
        {
            public IList<entUnidad.Unidad> Lis()
            {
                try
                {
                    IList<entUnidad.Unidad> r = new dbUnidad.Unidad().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entUnidad.Unidad Get(int id)
            {
                try
                {
                    entUnidad.Unidad r = new dbUnidad.Unidad().Get(id);
                    if (r is null) { throw new Personalized.PException("Unidad no encontrada"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entUnidad.Unidad Ins(entUnidad.Unidad obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entUnidad.Unidad r = new dbUnidad.Unidad().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entUnidad.Unidad Upd(entUnidad.Unidad obj)
            {
                try
                {
                    entUnidad.Unidad r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    if (obj.Simbolo == string.Empty) { throw new Personalized.PException("Debe ingresar un simbolo"); }
                    bool success = new dbUnidad.Unidad().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Unidad no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }
    }
}
