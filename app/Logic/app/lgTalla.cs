﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgTalla
    {

        public class Tipo
        {

            public IList<entTalla.Tipo> Lis()
            {
                try
                {
                    IList<entTalla.Tipo> r = new dbTalla.Tipo().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entTalla.Tipo Get(int id)
            {
                try
                {
                    entTalla.Tipo r = new dbTalla.Tipo().Get(id);
                    if (r is null) { throw new Personalized.PException("Tipo no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entTalla.Tipo Ins(entTalla.Tipo obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entTalla.Tipo r = new dbTalla.Tipo().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entTalla.Tipo Upd(entTalla.Tipo obj)
            {
                try
                {
                    entTalla.Tipo r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbTalla.Tipo().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class Talla
        {

            public IList<entTalla.Talla> Lis()
            {
                try
                {
                    IList<entTalla.Talla> r = new dbTalla.Talla().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entTalla.Talla Get(int id)
            {
                try
                {
                    entTalla.Talla r = new dbTalla.Talla().Get(id);
                    if (r is null) { throw new Personalized.PException("Talla no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entTalla.Talla Ins(entTalla.Talla obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    if (obj.Tipo.Id < 1) { throw new Personalized.PException("Debe seleccionar una tipo"); }
                    entTalla.Talla r = new dbTalla.Talla().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entTalla.Talla Upd(entTalla.Talla obj)
            {
                try
                {
                    entTalla.Talla r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    if (obj.Tipo.Id < 1) { throw new Personalized.PException("Debe seleccionar una tipo"); }
                    bool success = new dbTalla.Talla().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Talla no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

    }
}
