﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgSuministro
    {

        public class Tipo
        {
            public IList<entSuministro.Tipo> Lis()
            {
                try
                {
                    IList<entSuministro.Tipo> r = new dbSuministro.Tipo().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entSuministro.Tipo Get(int id)
            {
                try
                {
                    entSuministro.Tipo r = new dbSuministro.Tipo().Get(id);
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entSuministro.Tipo Ins(entSuministro.Tipo obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entSuministro.Tipo r = new dbSuministro.Tipo().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entSuministro.Tipo Upd(entSuministro.Tipo obj)
            {
                try
                {
                    entSuministro.Tipo r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbSuministro.Tipo().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class Suministro
        {
            public IList<entSuministro.Suministro> Lis()
            {
                try
                {
                    IList<entSuministro.Suministro> r = new dbSuministro.Suministro().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public IList<entSuministro.Suministro> Search(entSuministro.Suministro obj)
            {
                try
                {
                    IList<entSuministro.Suministro> r = new dbSuministro.Suministro().Search(obj);
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entSuministro.Suministro Get(int id)
            {
                try
                {
                    entSuministro.Suministro r = new dbSuministro.Suministro().Get(id);
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entSuministro.Suministro Ins(entSuministro.Suministro obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entSuministro.Suministro r = new dbSuministro.Suministro().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entSuministro.Suministro Upd(entSuministro.Suministro obj)
            {
                try
                {
                    entSuministro.Suministro r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbSuministro.Suministro().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

    }
}
