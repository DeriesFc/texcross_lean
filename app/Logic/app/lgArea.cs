﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgArea
    {


        public class Tipo
        {

            public IList<entArea.Tipo> Lis()
            {
                try
                {
                    IList<entArea.Tipo> r = new dbArea.Tipo().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public IList<entArea.Tipo> Search(entArea.Tipo obj)
            {
                try
                {
                    IList<entArea.Tipo> r = new dbArea.Tipo().Search(obj);
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entArea.Tipo Get(int id)
            {
                try
                {
                    entArea.Tipo r = new dbArea.Tipo().Get(id);
                    if (r is null) { throw new Personalized.PException("Usuario no registrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entArea.Tipo Ins(entArea.Tipo obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entArea.Tipo r = new dbArea.Tipo().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entArea.Tipo Upd(entArea.Tipo obj)
            {
                try
                {
                    entArea.Tipo r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbArea.Tipo().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class Area
        {

            public IList<entArea.Area> Lis()
            {
                try
                {
                    IList<entArea.Area> r = new dbArea.Area().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entArea.Area Get(int id)
            {
                try
                {
                    entArea.Area r = new dbArea.Area().Get(id);
                    if (r is null) { throw new Personalized.PException("Área no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entArea.Area Ins(entArea.Area obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entArea.Area r = new dbArea.Area().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entArea.Area Upd(entArea.Area obj)
            {
                try
                {
                    entArea.Area r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbArea.Area().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Área no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class Operacion
        {

            public IList<entArea.Operacion> Lis()
            {
                try
                {
                    IList<entArea.Operacion> r = new dbArea.Operacion().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entArea.Operacion Get(int id)
            {
                try
                {
                    entArea.Operacion r = new dbArea.Operacion().Get(id);
                    if (r is null) { throw new Personalized.PException("Operación no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entArea.Operacion Ins(entArea.Operacion obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    if (obj.Area.Id < 1) { throw new Personalized.PException("Debe seleccionar un área"); }
                    entArea.Operacion r = new dbArea.Operacion().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entArea.Operacion Upd(entArea.Operacion obj)
            {
                try
                {
                    entArea.Operacion r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    if (obj.Area.Id < 1) { throw new Personalized.PException("Debe seleccionar un área"); }
                    bool success = new dbArea.Operacion().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Operación no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

    }
}
