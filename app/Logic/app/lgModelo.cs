﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgModelo
    {

        public class Tipo
        {

            public IList<entModelo.Tipo> Lis()
            {
                try
                {
                    IList<entModelo.Tipo> r = new dbModelo.Tipo().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Tipo Get(int id)
            {
                try
                {
                    entModelo.Tipo r = new dbModelo.Tipo().Get(id);
                    if (r is null) { throw new Personalized.PException("Tipo no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entModelo.Tipo Ins(entModelo.Tipo obj)
            {
                try
                {
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entModelo.Tipo r = new dbModelo.Tipo().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Tipo Upd(entModelo.Tipo obj)
            {
                try
                {
                    entModelo.Tipo r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbModelo.Tipo().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class Modelo
        {

            public IList<entModelo.Modelo> Lis()
            {
                try
                {
                    IList<entModelo.Modelo> r = new dbModelo.Modelo().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public IList<entModelo.Modelo> LisValid()
            {
                try
                {
                    IList<entModelo.Modelo> r = new dbModelo.Modelo().LisValid();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Modelo Get(int id)
            {
                try
                {
                    entModelo.Modelo r = new dbModelo.Modelo().Get(id);
                    if (r is null) { throw new Personalized.PException("Modelo no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entModelo.Modelo Ins(entModelo.Modelo obj)
            {
                try
                {
                    if (obj.Tipo.Id < 1) { throw new Personalized.PException("Debe seleccionar un tipo"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    if (obj.Talla.Id < 1) { throw new Personalized.PException("Debe seleccionar una talla"); }
                    if (obj.Color.Id < 1) { throw new Personalized.PException("Debe seleccionar un color"); }
                    if (obj.Creador.Id < 1) { throw new Personalized.PException("Debe seleccionar un creador"); }
                    entModelo.Modelo r = new dbModelo.Modelo().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Modelo Upd(entModelo.Modelo obj)
            {
                try
                {
                    entModelo.Modelo r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Tipo.Id < 1) { throw new Personalized.PException("Debe seleccionar un tipo"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    if (obj.Talla.Id < 1) { throw new Personalized.PException("Debe seleccionar una talla"); }
                    if (obj.Color.Id < 1) { throw new Personalized.PException("Debe seleccionar un color"); }
                    if (obj.Creador.Id < 1) { throw new Personalized.PException("Debe seleccionar un creador"); }
                    bool success = new dbModelo.Modelo().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Modelo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class Suministros
        {

            public IList<entModelo.Suministros> Lis(int id)
            {
                try
                {
                    IList<entModelo.Suministros> r = new dbModelo.Suministros().Lis(id);
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Suministros Get(int id)
            {
                try
                {
                    entModelo.Suministros r = new dbModelo.Suministros().Get(id);
                    if (r is null) { throw new Personalized.PException("Modelo no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entModelo.Suministros Ins(entModelo.Suministros obj)
            {
                try
                {
                    if (obj.Cantidad > 0) { } else { throw new Personalized.PException("La cantidad debe ser mayor a 0"); }
                    if (obj.Modelo.Id < 1) { throw new Personalized.PException("Actualicé la pagina"); }
                    if (obj.Suministro.Id < 1) { throw new Personalized.PException("Debe seleccionar un suministro"); }
                    entModelo.Suministros r = new dbModelo.Suministros().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Suministros Upd(entModelo.Suministros obj)
            {
                try
                {
                    entModelo.Suministros r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Cantidad > 0) { } else { throw new Personalized.PException("La cantidad debe ser mayor a 0"); }
                    bool success = new dbModelo.Suministros().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Suministro no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public Object Del(int id) {
                try
                {
                    if (id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    bool success = new dbModelo.Suministros().Del(id);
                    if (!success) { throw new Personalized.PException("Ocurrió un problema"); }
                    return new { success };
                }
                catch (Exception) { throw; }
            }

        }

        public class Operaciones
        {

            public IList<entModelo.Operaciones> Lis(int id)
            {
                try
                {
                    IList<entModelo.Operaciones> r = new dbModelo.Operaciones().Lis(id);
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Operaciones Get(int id)
            {
                try
                {
                    entModelo.Operaciones r = new dbModelo.Operaciones().Get(id);
                    if (r is null) { throw new Personalized.PException("Operación no registrado"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public entModelo.Operaciones Ins(entModelo.Operaciones obj)
            {
                try
                {
                    if (obj.Modelo.Id < 1) { throw new Personalized.PException("Debe seleccionar un modelo"); }
                    if (obj.Operacion.Id < 1) { throw new Personalized.PException("Debe seleccionar una operación"); }
                    entModelo.Operaciones r = new dbModelo.Operaciones().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entModelo.Operaciones Upd(entModelo.Operaciones obj)
            {
                try
                {
                    entModelo.Operaciones r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Costo < 0) { throw new Personalized.PException("El costo no puede ser menor a 0"); }
                    if (obj.Tiempo < 0) { throw new Personalized.PException("El tiempo no puede ser menor a 0"); }
                    bool success = new dbModelo.Operaciones().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Operación no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public Object Del(int id)
            {
                try
                {
                    if (id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    bool success = new dbModelo.Operaciones().Del(id);
                    if (!success) { throw new Personalized.PException("Ocurrió un problema"); }
                    return new { success };
                }
                catch (Exception) { throw; }
            }

        }

    }

}
