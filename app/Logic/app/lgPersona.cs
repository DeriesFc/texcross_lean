﻿using Data;
using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public class lgPersona
    {
        public class Login
        {
            public entPersona.Persona Auth(string dni, string clave)
            {
                try
                {
                    entPersona.Persona r = new dbPersona.Acceso().Auth(dni);
                    if (r is null) { throw new Personalized.PException("Usuario no registrado"); }
                    if (!r.Clave.Equals(clave)) { throw new Personalized.PException("Contraseña incorrecta"); }
                    return r;

                } catch (Exception) { throw; }
            }
        }

        public class Persona
        {

            public IList<entPersona.Persona> Lis()
            {
                try
                {
                    IList<entPersona.Persona> r = new dbPersona.Persona().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entPersona.Persona Get(int id)
            {
                try
                {
                    entPersona.Persona r = new dbPersona.Persona().Get(id);
                    if (r is null) { throw new Personalized.PException("Persona no registrada"); }
                    return r;
                }
                catch (Exception ex) { throw ex; }
            }

            public IList<entPersona.PersonaS2VO> LisByOperacion(int operacion)
            {
                try
                {
                    IList<entPersona.PersonaS2VO> r = new dbPersona.Persona().LisByOperacion(operacion);
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class Tipo
        {

            public IList<entPersona.Tipo> Lis()
            {
                try
                {
                    IList<entPersona.Tipo> r = new dbPersona.Tipo().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entPersona.Tipo Get(int id)
            {
                try
                {
                    entPersona.Tipo r = new dbPersona.Tipo().Get(id);
                    if (r is null) { throw new Personalized.PException("Tipo no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

    }
}
