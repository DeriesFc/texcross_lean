﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgPedido
    {

        public class Pedido
        {

            public IList<entPedido.PedidoVO> Lis()
            {
                try
                {
                    IList<entPedido.PedidoVO> r = new dbPedido.Pedido().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entPedido.Pedido Ins(entPedido.Pedido obj)
            {
                try
                {
                    if (obj.Cliente == null) { throw new Personalized.PException("Debe seleccionar un cliente"); }
                    if (obj.Cliente.Id < 1) { throw new Personalized.PException("Debe seleccionar un cliente"); }
                    if (obj.FInicio == null) { throw new Personalized.PException("Debe ingresar un fecha de inicio"); }
                    if (obj.Detalle == null) { throw new Personalized.PException("Debe ingresar algún modelo"); }
                    if (obj.Detalle.Count < 1) { throw new Personalized.PException("Debe ingresar algún modelo"); }
                    if (!(obj.Precio > 0)) { throw new Personalized.PException("El precio debe ser mayor a 0"); }

                    int i = 1;
                    foreach (entPedido.PedidoDetalle d in obj.Detalle)
                    {
                        if (d.Modelo == null) { throw new Personalized.PException($"Debe seleccionar un modelo, en el registro {i}"); }
                        if (!(d.Modelo.Id > 0)) { throw new Personalized.PException($"Debe seleccionar un modelo, en el registro {i}"); }
                        if (d.Cantidad < 1) { throw new Personalized.PException($"Debe ingresar una cantidad mayor a 0, en el registro {i}"); }
                        if (d.FEFin == null) { throw new Personalized.PException($"Debe ingresar un fecha de fin, en el registro {i}"); }
                        i++;
                    }

                    entPedido.Pedido r = new dbPedido.Pedido().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoVO GetByCode(string code)
            {
                try
                {
                    entPedido.PedidoVO r = new dbPedido.Pedido().GetByCode(code);
                    if (r == null) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

        public class PedidoDetalle
        {
            public IList<entPedido.PedidoDetalleVO> Lis(int id)
            {
                try
                {
                    IList<entPedido.PedidoDetalleVO> r = new dbPedido.PedidoDetalle().Lis(id);
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoDetalleVO GetByCode(int idPedido, string code)
            {
                try
                {
                    entPedido.PedidoDetalleVO r = new dbPedido.PedidoDetalle().GetByCode(idPedido, code);
                    if (r == null) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class PedidoDetalleControl
        {
            public IList<entPedido.PedidoDetalleControlVO> Lis(int id)
            {
                try
                {
                    IList<entPedido.PedidoDetalleControlVO> r = new dbPedido.PedidoDetalleControl().Lis(id);
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoDetalleControlVO GetByCode(int idPedidoDetalle, string code)
            {
                try
                {
                    entPedido.PedidoDetalleControlVO r = new dbPedido.PedidoDetalleControl().GetByCode(idPedidoDetalle, code);
                    if (r == null) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class PedidoDetalleControlDetalle
        {
            public IList<entPedido.PedidoDetalleControlDetalleVO> Lis(int id)
            {
                try
                {
                    IList<entPedido.PedidoDetalleControlDetalleVO> r = new dbPedido.PedidoDetalleControlDetalle().Lis(id);
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoDetalleControlDetalleVO GetByCode(int id)
            {
                try
                {
                    entPedido.PedidoDetalleControlDetalleVO r = new dbPedido.PedidoDetalleControlDetalle().GetByCode(id);
                    if (r == null) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public Object Terminar(int id, int persona) {

                try {

                    bool r = new dbPedido.PedidoDetalleControlDetalle().Terminar(id, persona);
                    if (!r) { throw new Exception("Dificultad"); }
                    return new { rpt = r };
                }
                catch (Exception) { throw; }

            }

        }

    }
}
