﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class lgCliente
    {
        public class Cliente
        {

            public IList<entCliente.Cliente> Lis()
            {
                try
                {
                    IList<entCliente.Cliente> r = new dbCliente.Cliente().Lis();
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entCliente.Cliente Get(int id)
            {
                try
                {
                    entCliente.Cliente r = new dbCliente.Cliente().Get(id);
                    if (r is null) { throw new Personalized.PException("Color no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entCliente.Cliente Ins(entCliente.Cliente obj)
            {
                try
                {
                    if (obj.Ruc == string.Empty) { throw new Personalized.PException("Debe ingresar el ruc del cliente"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    entCliente.Cliente r = new dbCliente.Cliente().Ins(obj);
                    if (r.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entCliente.Cliente Upd(entCliente.Cliente obj)
            {
                try
                {
                    entCliente.Cliente r = null;

                    if (obj.Id < 1) { throw new Personalized.PException("Vuelva a intentar"); }
                    if (obj.Ruc == string.Empty) { throw new Personalized.PException("Debe ingresar el ruc del cliente"); }
                    if (obj.Nombre == string.Empty) { throw new Personalized.PException("Debe ingresar un nombre"); }
                    bool success = new dbCliente.Cliente().Upd(obj);
                    if (success) { r = Get(obj.Id); }
                    else { throw new Personalized.PException("Ocurrió un problema"); }
                    if (r is null) { throw new Personalized.PException("Cliente no encontrado"); }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }
    }
}
