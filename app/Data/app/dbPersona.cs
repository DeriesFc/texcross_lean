﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbPersona : Conexion
    {

        public class Acceso
        {

            public entPersona.Persona Auth (string dni)
            {
                try
                {
                    entPersona.Persona r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText =
                                @"SELECT
                                    P.id,
                                    P.dni,
                                    P.nombre,
                                    P.apellido,
                                    P.clave,
                                    PT.id as tipoId,
                                    PT.nombre as tipoNombre,
                                    A.id as areaId,
                                    A.nombre as areaNombre,
                                    AT.id as areaTipoId,
                                    AT.nombre as areaTipoNombre
                                FROM Persona P
                                INNER JOIN PersonaTipo PT on PT.id = P.tipo
                                INNER JOIN Area A on A.id = P.area
                                INNER JOIN AreaTipo  AT on AT.id = A.tipo
                                WHERE P.dni = @dni and PT.acceso = 1";
                            cmd.Parameters.AddWithValue("@dni", dni);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entPersona.Persona()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Dni = Utilitario.ToString(dr["dni"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Apellido = Utilitario.ToString(dr["apellido"]),
                                        Clave = Utilitario.ToString(dr["clave"]),
                                        Tipo = new entPersona.Tipo() {
                                            Id = Utilitario.ToInt(dr["tipoId"]),
                                            Nombre = Utilitario.ToString(dr["tipoNombre"])
                                        },
                                        Area = new entArea.Area() {
                                            Id = Utilitario.ToInt(dr["areaId"]),
                                            Nombre = Utilitario.ToString(dr["areaNombre"]),
                                            Tipo = new entArea.Tipo() {
                                                Id = Utilitario.ToInt(dr["areaTipoId"]),
                                                Nombre = Utilitario.ToString(dr["areaTipoNombre"])
                                            }
                                        }
                                    };
                                }
                            }
                        }
                    } return r;

                } catch (Exception) { throw; }
            }

        }

        public class Persona
        {
            public IList<entPersona.Persona> Lis()
            {
                try
                {
                    IList<entPersona.Persona> r = new List<entPersona.Persona>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, dni as d, nombre as n, apellido as a, tipo as t, estado as e FROM Persona";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entPersona.Persona()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Dni = Utilitario.ToString(dr["d"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Apellido = Utilitario.ToString(dr["a"]),
                                        Tipo = new entPersona.Tipo() { Id = Utilitario.ToInt("t") },
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entPersona.Persona Get(int id)
            {
                try
                {
                    entPersona.Persona r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, dni as d, nombre as n, apellido as a, tipo as t, estado as e FROM Persona WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entPersona.Persona()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Dni = Utilitario.ToString(dr["d"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Apellido = Utilitario.ToString(dr["a"]),
                                        Tipo = new entPersona.Tipo() { Id = Utilitario.ToInt("t") },
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public IList<entPersona.PersonaS2VO> LisByOperacion(int operacion)
            {
                try
                {
                    IList<entPersona.PersonaS2VO> r = new List<entPersona.PersonaS2VO>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                Select P.id Id, CONCAT(P.dni, ' - ', P.apellido, ', ', P.nombre) Nombre
                                from Persona P
                                INNER JOIN AREA A on A.id = P.area
                                INNER JOIN AreaOperacion O on O.area = A.id
                                WHERE O.id = @Id
                            ";
                            cmd.Parameters.AddWithValue("@Id", operacion);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entPersona.PersonaS2VO()
                                    {
                                        I = Utilitario.ToInt(dr["Id"]),
                                        N = Utilitario.ToString(dr["Nombre"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

        }

        public class Tipo
        {
            public IList<entPersona.Tipo> Lis()
            {
                try
                {
                    IList<entPersona.Tipo> r = new List<entPersona.Tipo>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, acceso as a FROM PersonaTipo";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entPersona.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Acceso = Utilitario.ToBool(dr["a"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entPersona.Tipo Get(int id)
            {
                try
                {
                    entPersona.Tipo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, acceso as a FROM PersonaTipo WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entPersona.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Acceso = Utilitario.ToBool(dr["a"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

        }

    }
}
