﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbSuministro
    {

        public class Suministro : Conexion
        {
            public IList<entSuministro.Suministro> Lis()
            {
                try
                {
                    IList<entSuministro.Suministro> r = new List<entSuministro.Suministro>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT 
	                            S.id as id,
	                            ST.id as tipoId,
	                            ST.nombre as tipoNombre,
	                            S.nombre as nombre,
	                            C.id as colorId,
	                            C.nombre as colorNombre,
	                            U.id as unidadId,
	                            U.nombre as unidadNombre,
	                            U.simbolo as unidadSimbolo,
	                            S.cantidad as cantidad,
	                            S.precio as precio,
                                S.estado as estado
                            FROM Suministro S
                            INNER JOIN SuministroTipo ST on ST.id = S.tipo
                            INNER JOIN Color C on C.id = S.color
                            INNER JOIN Unidad U on U.id = S.unidad
                            ORDER BY s.Nombre asc";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entSuministro.Suministro()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Tipo = new entSuministro.Tipo()
                                        {
                                            Id = Utilitario.ToInt(dr["tipoId"]),
                                            Nombre = Utilitario.ToString(dr["tipoNombre"])
                                        },
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Color = new entColor.Color()
                                        {
                                            Id = Utilitario.ToInt(dr["colorId"]),
                                            Nombre = Utilitario.ToString(dr["colorNombre"])
                                        },
                                        Unidad = new entUnidad.Unidad()
                                        {
                                            Id = Utilitario.ToInt(dr["unidadId"]),
                                            Nombre = Utilitario.ToString(dr["unidadNombre"]),
                                            Simbolo = Utilitario.ToString(dr["unidadSimbolo"])
                                        },
                                        Cantidad = Utilitario.ToDecimal(dr["cantidad"]),
                                        Precio = Utilitario.ToDecimal(dr["precio"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public IList<entSuministro.Suministro> Search(entSuministro.Suministro obj)
            {
                try
                {
                    IList<entSuministro.Suministro> r = new List<entSuministro.Suministro>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT 
	                                S.id as id,
	                                ST.id as tipoId,
	                                ST.nombre as tipoNombre,
	                                S.nombre as nombre,
	                                C.id as colorId,
	                                C.nombre as colorNombre,
	                                U.id as unidadId,
	                                U.nombre as unidadNombre,
	                                U.simbolo as unidadSimbolo,
	                                S.cantidad as cantidad,
	                                S.precio as precio,
                                    S.estado as estado
                                FROM Suministro S
                                INNER JOIN SuministroTipo ST on ST.id = S.tipo
                                INNER JOIN Color C on C.id = S.color
                                INNER JOIN Unidad U on U.id = S.unidad
                                WHERE
	                                (@id = 0 or S.id = @id) and
                                    (@tipo = 0 or S.tipo = @tipo) and
                                    (@nombre = '' or S.nombre like '%' + @nombre + '%') and
	                                (@color = 0 or S.color = @color) and
	                                (@unidad = 0 or S.unidad = @unidad)
                                ORDER BY s.Nombre asc";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@tipo", obj.Tipo.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@color", obj.Color.Id);
                            cmd.Parameters.AddWithValue("@unidad", obj.Unidad.Id);
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entSuministro.Suministro()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Tipo = new entSuministro.Tipo()
                                        {
                                            Id = Utilitario.ToInt(dr["tipoId"]),
                                            Nombre = Utilitario.ToString(dr["tipoNombre"])
                                        },
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Color = new entColor.Color()
                                        {
                                            Id = Utilitario.ToInt(dr["colorId"]),
                                            Nombre = Utilitario.ToString(dr["colorNombre"])
                                        },
                                        Unidad = new entUnidad.Unidad()
                                        {
                                            Id = Utilitario.ToInt(dr["unidadId"]),
                                            Nombre = Utilitario.ToString(dr["unidadNombre"]),
                                            Simbolo = Utilitario.ToString(dr["unidadSimbolo"])
                                        },
                                        Cantidad = Utilitario.ToDecimal(dr["cantidad"]),
                                        Precio = Utilitario.ToDecimal(dr["precio"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entSuministro.Suministro Get(int id)
            {
                try
                {
                    entSuministro.Suministro r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT 
	                            S.id as id,
	                            ST.id as tipoId,
	                            ST.nombre as tipoNombre,
	                            S.nombre as nombre,
	                            C.id as colorId,
	                            C.nombre as colorNombre,
	                            U.id as unidadId,
	                            U.nombre as unidadNombre,
	                            U.simbolo as unidadSimbolo,
	                            S.cantidad as cantidad,
	                            S.precio as precio,
	                            S.estado as estado
                            FROM Suministro S
                            INNER JOIN SuministroTipo ST on ST.id = S.tipo
                            INNER JOIN Color C on C.id = S.color
                            INNER JOIN Unidad U on U.id = S.unidad
                            WHERE S.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entSuministro.Suministro()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Tipo = new entSuministro.Tipo()
                                        {
                                            Id = Utilitario.ToInt(dr["tipoId"]),
                                            Nombre = Utilitario.ToString(dr["tipoNombre"])
                                        },
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Color = new entColor.Color()
                                        {
                                            Id = Utilitario.ToInt(dr["colorId"]),
                                            Nombre = Utilitario.ToString(dr["colorNombre"])
                                        },
                                        Unidad = new entUnidad.Unidad()
                                        {
                                            Id = Utilitario.ToInt(dr["unidadId"]),
                                            Nombre = Utilitario.ToString(dr["unidadNombre"]),
                                            Simbolo = Utilitario.ToString(dr["unidadSimbolo"])
                                        },
                                        Cantidad = Utilitario.ToDecimal(dr["cantidad"]),
                                        Precio = Utilitario.ToDecimal(dr["precio"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception ex) { throw; }

            }

            public entSuministro.Suministro Ins(entSuministro.Suministro obj)
            {
                try
                {
                    entSuministro.Suministro r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            INSERT INTO Suministro(tipo, nombre, color, unidad, cantidad, precio) 
                            OUTPUT INSERTED.* 
                            VALUES(@tipo, @nombre, @color, @unidad, @cantidad, @precio)";
                            cmd.Parameters.AddWithValue("@tipo", obj.Tipo.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@color", obj.Color.Id);
                            cmd.Parameters.AddWithValue("@unidad", obj.Unidad.Id);
                            cmd.Parameters.AddWithValue("@cantidad", obj.Cantidad);
                            cmd.Parameters.AddWithValue("@precio", obj.Precio);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entSuministro.Suministro()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Tipo = new entSuministro.Tipo() { Id = Utilitario.ToInt(dr["tipo"]), },
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Color = new entColor.Color() { Id = Utilitario.ToInt(dr["color"]), },
                                        Unidad = new entUnidad.Unidad() { Id = Utilitario.ToInt(dr["unidad"]), },
                                        Cantidad = Utilitario.ToDecimal(dr["cantidad"]),
                                        Precio = Utilitario.ToDecimal(dr["precio"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entSuministro.Suministro obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            UPDATE Suministro SET
                                  tipo = @tipo,
                                  nombre = @nombre,
                                  color = @color,
                                  unidad = @unidad,
                                  cantidad = @cantidad,
                                  precio = @precio
                            WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@tipo", obj.Tipo.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@color", obj.Color.Id);
                            cmd.Parameters.AddWithValue("@unidad", obj.Unidad.Id);
                            cmd.Parameters.AddWithValue("@cantidad", obj.Cantidad);
                            cmd.Parameters.AddWithValue("@precio", obj.Precio);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class Tipo : Conexion
        {
            public IList<entSuministro.Tipo> Lis()
            {
                try
                {
                    IList<entSuministro.Tipo> r = new List<entSuministro.Tipo>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, estado as e FROM SuministroTipo";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entSuministro.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entSuministro.Tipo Get(int id)
            {
                try
                {
                    entSuministro.Tipo r = new entSuministro.Tipo();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, estado as e FROM SuministroTipo WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entSuministro.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entSuministro.Tipo Ins(entSuministro.Tipo obj)
            {
                try
                {
                    entSuministro.Tipo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO SuministroTipo(nombre) OUTPUT INSERTED.* VALUES(@nombre)";
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entSuministro.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entSuministro.Tipo obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE SuministroTipo SET nombre = @nombre WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

    }
}
