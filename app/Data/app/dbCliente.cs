﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbCliente
    {

        public class Cliente : Conexion
        {

            public IList<entCliente.Cliente> Lis()
            {
                try
                {
                    IList<entCliente.Cliente> r = new List<entCliente.Cliente>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, ruc as r, nombre as n, estado as e FROM Cliente Order by nombre asc";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entCliente.Cliente()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Ruc = Utilitario.ToString(dr["r"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entCliente.Cliente Get(int id)
            {
                try
                {
                    entCliente.Cliente r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, ruc as r, nombre as n, estado as e FROM Cliente Order by nombre asc WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entCliente.Cliente()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Ruc = Utilitario.ToString(dr["r"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entCliente.Cliente Ins(entCliente.Cliente obj)
            {
                try
                {
                    entCliente.Cliente r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO Cliente(ruc, nombre) OUTPUT INSERTED.* VALUES(@ruc, @nombre)";
                            cmd.Parameters.AddWithValue("@ruc", obj.Ruc);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entCliente.Cliente()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Ruc = Utilitario.ToString(dr["ruc"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entCliente.Cliente obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE Cliente SET ruc = @ruc, nombre = @nombre WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@ruc", obj.Ruc);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }

    }
}
