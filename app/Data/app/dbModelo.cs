﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbModelo
    {

        public class Tipo : Conexion
        {
            public IList<entModelo.Tipo> Lis()
            {
                try
                {
                    IList<entModelo.Tipo> r = new List<entModelo.Tipo>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, estado as e FROM ModeloTipo";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entModelo.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entModelo.Tipo Get(int id)
            {
                try
                {
                    entModelo.Tipo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, estado as e FROM ModeloTipo WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entModelo.Tipo Ins(entModelo.Tipo obj)
            {
                try
                {
                    entModelo.Tipo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO ModeloTipo(nombre) OUTPUT INSERTED.* VALUES(@nombre)";
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entModelo.Tipo obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE ModeloTipo SET nombre = @nombre WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class Modelo : Conexion
        {
            public IList<entModelo.Modelo> Lis()
            {
                try
                {
                    IList<entModelo.Modelo> r = new List<entModelo.Modelo>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT
	                                M.id as MId,
	                                Ti.id as TiId,
	                                Ti.nombre as TiNombre,
	                                M.nombre as MNombre,
	                                Ta.id as TaId,
	                                Ta.nombre as TaNombre,
                                    Tt.id as TtId,
	                                Tt.nombre as TtNombre,
	                                Co.id as CoId,
	                                Co.nombre as CoNombre,
	                                Cr.id as CrId,
	                                Cr.ruc as CrRuc,
	                                Cr.nombre as CrNombre,
	                                M.estado as MEstado,
	                                ISNULL(MO.CA, 0) as MOCant,
	                                ISNULL(MO.CO, 0) as MOCosto,
	                                ISNULL(MO.TI, 0) as MOTiempo,
	                                ISNULL(MS.CA, 0) as MSCant,
	                                ISNULL(MS.CO, 0) as MSPrecio
                                FROM Modelo M
                                INNER JOIN ModeloTipo Ti on Ti.id = M.tipo
                                INNER JOIN Talla Ta on Ta.id = M.talla
                                INNER JOIN TallaTipo Tt on Tt.id = Ta.tipo
                                INNER JOIN Color Co on Co.id = M.color
                                INNER JOIN Cliente Cr on Cr.id = M.creador
                                OUTER APPLY (SELECT count(1) CA, SUM(MO.costo) CO, SUM(MO.tiempo) TI FROM ModeloOperacion MO WHERE MO.modelo = M.id) MO
                                OUTER APPLY (
	                                SELECT COUNT(1) CA, CAST(SUM(MS.cantidad * S.precio) as decimal(12,2)) CO
	                                FROM ModeloSuministro MS
	                                INNER JOIN Suministro S on S.id = MS.suministro
	                                WHERE MS.modelo = M.id
                                ) MS
                                ORDER BY M.nombre asc
                            ";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entModelo.Modelo()
                                    {
                                        Id = Utilitario.ToInt(dr["MId"]),
                                        Tipo = new entModelo.Tipo()
                                        {
                                            Id = Utilitario.ToInt(dr["TiId"]),
                                            Nombre = Utilitario.ToString(dr["TiNombre"])
                                        },
                                        Nombre = Utilitario.ToString(dr["MNombre"]),
                                        Talla = new entTalla.Talla()
                                        {
                                            Id = Utilitario.ToInt(dr["TaId"]),
                                            Nombre = Utilitario.ToString(dr["TaNombre"]),
                                            Tipo = new entTalla.Tipo()
                                            {
                                                Id = Utilitario.ToInt(dr["TtId"]),
                                                Nombre = Utilitario.ToString(dr["TtNombre"])
                                            }
                                        },
                                        Color = new entColor.Color()
                                        {
                                            Id = Utilitario.ToInt(dr["CoId"]),
                                            Nombre = Utilitario.ToString(dr["CoNombre"])
                                        },
                                        Creador = new entCliente.Cliente()
                                        {
                                            Id = Utilitario.ToInt(dr["CrId"]),
                                            Ruc = Utilitario.ToString(dr["CrRuc"]),
                                            Nombre = Utilitario.ToString(dr["CrNombre"]),
                                        },
                                        Estado = Utilitario.ToBool(dr["MEstado"]),

                                        Suministro = new entModelo.Suministros()
                                        {
                                            Cantidad = Utilitario.ToInt(dr["MSCant"]),
                                            Precio = Utilitario.ToDecimal(dr["MSPrecio"])
                                        },
                                        Operacion = new entModelo.Operaciones()
                                        {
                                            Cantidad = Utilitario.ToInt(dr["MOCant"]),
                                            Costo = Utilitario.ToDecimal(dr["MOCosto"]),
                                            Tiempo = Utilitario.ToInt(dr["MOTiempo"])
                                        }
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public IList<entModelo.Modelo> LisValid()
            {
                try
                {
                    IList<entModelo.Modelo> r = new List<entModelo.Modelo>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT
	                            M.id as id,
	                            Ti.nombre as Tipo,
	                            M.nombre as Nombre,
	                            Ta.nombre as Talla,
	                            Tt.nombre as TallaTipo,
	                            Co.nombre as Color,
	                            CONCAT(Cr.ruc, ' - ', Cr.nombre) as Creador
                            FROM Modelo M
		                            INNER JOIN ModeloTipo Ti on Ti.id = M.tipo
		                            INNER JOIN Talla Ta on Ta.id = M.talla
		                            INNER JOIN TallaTipo Tt on Tt.id = Ta.tipo
		                            INNER JOIN Color Co on Co.id = M.color
		                            INNER JOIN Cliente Cr on Cr.id = M.creador
                            WHERE
		                            EXISTS (Select 1 from ModeloSuministro MS WHERE MS.modelo = M.id )
	                            and EXISTS (Select 1 from ModeloOperacion MO WHERE MO.modelo = M.id )
                            ORDER BY M.id desc";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entModelo.Modelo()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Tipo = new entModelo.Tipo()
                                        {
                                            Nombre = Utilitario.ToString(dr["Tipo"])
                                        },
                                        Nombre = Utilitario.ToString(dr["Nombre"]),
                                        Talla = new entTalla.Talla()
                                        {
                                            Nombre = Utilitario.ToString(dr["Talla"]),
                                            Tipo = new entTalla.Tipo()
                                            {
                                                Nombre = Utilitario.ToString(dr["TallaTipo"])
                                            }
                                        },
                                        Color = new entColor.Color()
                                        {
                                            Nombre = Utilitario.ToString(dr["Color"])
                                        },
                                        Creador = new entCliente.Cliente()
                                        {
                                            Nombre = Utilitario.ToString(dr["Creador"]),
                                        }
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entModelo.Modelo Get(int id)
            {
                try
                {
                    entModelo.Modelo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT
	                            M.id as MId,
	                            Ti.id as TiId,
	                            Ti.nombre as TiNombre,
	                            M.nombre as MNombre,
	                            Ta.id as TaId,
	                            Ta.nombre as TaNombre,
                                Tt.id as TtId,
	                            Tt.nombre as TtNombre,
	                            Co.id as CoId,
	                            Co.nombre as CoNombre,
	                            Cr.id as CrId,
	                            Cr.ruc as CrRuc,
	                            Cr.nombre as CrNombre,
	                            M.estado as MEstado,
	                            ISNULL(MO.CA, 0) as MOCant,
	                            ISNULL(MO.CO, 0) as MOCosto,
	                            ISNULL(MO.TI, 0) as MOTiempo,
	                            ISNULL(MS.CA, 0) as MSCant,
	                            ISNULL(MS.CO, 0) as MSPrecio
                            FROM Modelo M
                            INNER JOIN ModeloTipo Ti on Ti.id = M.tipo
                            INNER JOIN Talla Ta on Ta.id = M.talla
                            INNER JOIN TallaTipo Tt on Tt.id = Ta.tipo
                            INNER JOIN Color Co on Co.id = M.color
                            INNER JOIN Cliente Cr on Cr.id = M.creador
                            OUTER APPLY (SELECT count(1) CA, SUM(MO.costo) CO, SUM(MO.tiempo) TI FROM ModeloOperacion MO WHERE MO.modelo = M.id) MO
                            OUTER APPLY (
	                            SELECT COUNT(1) CA, CAST(SUM(MS.cantidad * S.precio) as decimal(12,2)) CO
	                            FROM ModeloSuministro MS
	                            INNER JOIN Suministro S on S.id = MS.suministro
	                            WHERE MS.modelo = M.id
                            ) MS
                            WHERE M.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Modelo()
                                    {
                                        Id = Utilitario.ToInt(dr["MId"]),
                                        Tipo = new entModelo.Tipo()
                                        {
                                            Id = Utilitario.ToInt(dr["TiId"]),
                                            Nombre = Utilitario.ToString(dr["TiNombre"])
                                        },
                                        Nombre = Utilitario.ToString(dr["MNombre"]),
                                        Talla = new entTalla.Talla()
                                        {
                                            Id = Utilitario.ToInt(dr["TaId"]),
                                            Nombre = Utilitario.ToString(dr["TaNombre"]),
                                            Tipo = new entTalla.Tipo()
                                            {
                                                Id = Utilitario.ToInt(dr["TtId"]),
                                                Nombre = Utilitario.ToString(dr["TtNombre"])
                                            }
                                        },
                                        Color = new entColor.Color()
                                        {
                                            Id = Utilitario.ToInt(dr["CoId"]),
                                            Nombre = Utilitario.ToString(dr["CoNombre"])
                                        },
                                        Creador = new entCliente.Cliente()
                                        {
                                            Id = Utilitario.ToInt(dr["CrId"]),
                                            Ruc = Utilitario.ToString(dr["CrRuc"]),
                                            Nombre = Utilitario.ToString(dr["CrNombre"]),
                                        },
                                        Estado = Utilitario.ToBool(dr["MEstado"]),
                                        Suministro = new entModelo.Suministros()
                                        {
                                            Cantidad = Utilitario.ToInt(dr["MSCant"]),
                                            Precio = Utilitario.ToDecimal(dr["MSPrecio"])
                                        },
                                        Operacion = new entModelo.Operaciones()
                                        {
                                            Cantidad = Utilitario.ToInt(dr["MOCant"]),
                                            Costo = Utilitario.ToDecimal(dr["MOCosto"]),
                                            Tiempo = Utilitario.ToInt(dr["MOTiempo"])
                                        }
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entModelo.Modelo Ins(entModelo.Modelo obj)
            {
                try
                {
                    entModelo.Modelo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                INSERT INTO Modelo(tipo, nombre, talla, color, creador) 
                                OUTPUT INSERTED.* 
                                VALUES(@tipo, @nombre, @talla, @color, @creador)";
                            cmd.Parameters.AddWithValue("@tipo", obj.Tipo.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@talla", obj.Talla.Id);
                            cmd.Parameters.AddWithValue("@color", obj.Color.Id);
                            cmd.Parameters.AddWithValue("@creador", obj.Creador.Id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Modelo()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Tipo = new entModelo.Tipo() { Id = Utilitario.ToInt(dr["tipo"]), },
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Talla = new entTalla.Talla() { Id = Utilitario.ToInt(dr["talla"]), },
                                        Color = new entColor.Color() { Id = Utilitario.ToInt(dr["color"]), },
                                        Creador = new entCliente.Cliente() { Id = Utilitario.ToInt(dr["creador"]), },
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entModelo.Modelo obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                UPDATE Modelo SET
                                    tipo = @tipo,
                                    nombre = @nombre,
		                            talla = @talla,
                                    color = @color,
		                            creador = @creador
                                WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@tipo", obj.Tipo.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@talla", obj.Talla.Id);
                            cmd.Parameters.AddWithValue("@color", obj.Color.Id);
                            cmd.Parameters.AddWithValue("@creador", obj.Creador.Id);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class Suministros : Conexion
        {
            public IList<entModelo.Suministros> Lis(int id)
            {
                try
                {
                    IList<entModelo.Suministros> r = new List<entModelo.Suministros>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT
	                            MS.id as MSId,
	                            MS.cantidad as MSCantidad,
	                            M.id  as MId,
	                            S.id as SId,
	                            ST.id as STId,
	                            ST.nombre as STNombre,
	                            S.nombre  as SNombre,
	                            CO.id as COColor,
	                            CO.Nombre as CONombre,
	                            U.id as UId,
	                            U.nombre as UNombre,
	                            U.simbolo as USimbolo,
	                            S.precio as SPrecio
                            FROM ModeloSuministro MS
                            INNER JOIN Modelo M on M.id = MS.Modelo
                            INNER JOIN Suministro S on S.id = MS.suministro
                            INNER JOIN SuministroTipo ST on ST.id = S.tipo
                            INNER JOIN Color CO on CO.id = S.color
                            INNER JOIN Unidad U on U.id = S.unidad
                            WHERE M.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entModelo.Suministros()
                                    {
                                        Id = Utilitario.ToInt(dr["MSId"]),
                                        Cantidad = Utilitario.ToDecimal(dr["MSCantidad"]),
                                        Modelo = new entModelo.Modelo()
                                        {
                                            Id = Utilitario.ToInt(dr["MId"])

                                        },
                                        Suministro = new entSuministro.Suministro() {
                                            Id = Utilitario.ToInt(dr["SId"]),
                                            Tipo = new entSuministro.Tipo()
                                            {
                                                Id = Utilitario.ToInt(dr["STId"]),
                                                Nombre = Utilitario.ToString(dr["STNombre"])
                                            },
                                            Nombre = Utilitario.ToString(dr["SNombre"]),
                                            Color = new entColor.Color()
                                            {
                                                Id = Utilitario.ToInt(dr["COColor"]),
                                                Nombre = Utilitario.ToString(dr["CONombre"])
                                            },
                                            Unidad = new entUnidad.Unidad()
                                            {
                                                Id = Utilitario.ToInt(dr["UId"]),
                                                Nombre = Utilitario.ToString(dr["UNombre"]),
                                                Simbolo = Utilitario.ToString(dr["USimbolo"])
                                            },
                                            Precio = Utilitario.ToDecimal(dr["SPrecio"])
                                        }
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entModelo.Suministros Get(int id)
            {
                try
                {
                    entModelo.Suministros r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT
	                            MS.id as MSId,
	                            MS.cantidad as MSCantidad,
	                            M.id  as MId,
	                            S.id as SId,
	                            ST.id as STId,
	                            ST.nombre as STNombre,
	                            S.nombre  as SNombre,
	                            CO.id as COColor,
	                            CO.Nombre as CONombre,
	                            U.id as UId,
	                            U.nombre as UNombre,
	                            U.simbolo as USimbolo,
	                            S.precio as SPrecio
                            FROM ModeloSuministro MS
                            INNER JOIN Modelo M on M.id = MS.Modelo
                            INNER JOIN Suministro S on S.id = MS.suministro
                            INNER JOIN SuministroTipo ST on ST.id = S.tipo
                            INNER JOIN Color CO on CO.id = S.color
                            INNER JOIN Unidad U on U.id = S.unidad
                            WHERE MS.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Suministros()
                                    {
                                        Id = Utilitario.ToInt(dr["MId"]),
                                        Cantidad = Utilitario.ToDecimal(dr["MSCantidad"]),
                                        Modelo = new entModelo.Modelo()
                                        {
                                            Id = Utilitario.ToInt(dr["MId"])

                                        },
                                        Suministro = new entSuministro.Suministro()
                                        {
                                            Id = Utilitario.ToInt(dr["SId"]),
                                            Tipo = new entSuministro.Tipo()
                                            {
                                                Id = Utilitario.ToInt(dr["STId"]),
                                                Nombre = Utilitario.ToString(dr["STNombre"])
                                            },
                                            Nombre = Utilitario.ToString(dr["SNombre"]),
                                            Color = new entColor.Color()
                                            {
                                                Id = Utilitario.ToInt(dr["COColor"]),
                                                Nombre = Utilitario.ToString(dr["CONombre"])
                                            },
                                            Unidad = new entUnidad.Unidad()
                                            {
                                                Id = Utilitario.ToInt(dr["UId"]),
                                                Nombre = Utilitario.ToString(dr["UNombre"]),
                                                Simbolo = Utilitario.ToString(dr["USimbolo"])
                                            },
                                            Precio = Utilitario.ToDecimal(dr["SPrecio"])
                                        }
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entModelo.Suministros Ins(entModelo.Suministros obj)
            {
                try
                {
                    entModelo.Suministros r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                INSERT INTO ModeloSuministro(cantidad, modelo, suministro) 
                                OUTPUT INSERTED.* 
                                VALUES(@cantidad, @modelo, @suministro)";
                            cmd.Parameters.AddWithValue("@cantidad", obj.Cantidad);
                            cmd.Parameters.AddWithValue("@modelo", obj.Modelo.Id);
                            cmd.Parameters.AddWithValue("@suministro", obj.Suministro.Id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Suministros()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Cantidad = Utilitario.ToDecimal(dr["cantidad"]),
                                        Modelo = new entModelo.Modelo() { Id= Utilitario.ToInt(dr["modelo"])},
                                        Suministro = new entSuministro.Suministro() { Id = Utilitario.ToInt(dr["suministro"]) },
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception ex) { throw; }
            }

            public bool Upd(entModelo.Suministros obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            UPDATE ModeloSuministro SET
	                            cantidad = @cantidad
                            WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@cantidad", obj.Cantidad);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public bool Del(int id)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"DELETE ModeloSuministro WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class Operaciones : Conexion
        {
            public IList<entModelo.Operaciones> Lis(int id)
            {
                try
                {
                    IList<entModelo.Operaciones> r = new List<entModelo.Operaciones>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT 
	                            MO.id as MOId,
	                            MO.costo as MOCosto,
	                            MO.tiempo as MOTiempo,
	                            M.id as MId,
	                            AO.id as AOId,
	                            AO.nombre as AONombre,
	                            A.id as AId,
	                            A.Nombre as ANombre
                            FROM ModeloOperacion MO
                            INNER JOIN Modelo M on M.id = MO.modelo
                            INNER JOIN AreaOperacion AO on AO.id  = MO.operacion
                            INNER JOIN Area A on A.id = AO.area
                            WHERE M.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entModelo.Operaciones()
                                    {
                                        Id = Utilitario.ToInt(dr["MOId"]),
                                        Costo = Utilitario.ToDecimal(dr["MOCosto"]),
                                        Tiempo = Utilitario.ToInt(dr["MOTiempo"]),
                                        Modelo = new entModelo.Modelo()
                                        {
                                            Id = Utilitario.ToInt(dr["MId"])
                                        },
                                        Operacion = new entArea.Operacion()
                                        {
                                            Id = Utilitario.ToInt(dr["AOId"]),
                                            Nombre = Utilitario.ToString(dr["AONombre"]),
                                            Area = new entArea.Area()
                                            {
                                                Id = Utilitario.ToInt(dr["AId"]),
                                                Nombre = Utilitario.ToString(dr["ANombre"])
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entModelo.Operaciones Get(int id)
            {
                try
                {
                    entModelo.Operaciones r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            SELECT 
	                            MO.id as MOId,
	                            MO.costo as MOCosto,
	                            MO.tiempo as MOTiempo,
	                            M.id as MId,
	                            AO.id as AOId,
	                            AO.nombre as AONombre,
	                            A.id as AId,
	                            A.Nombre as ANombre
                            FROM ModeloOperacion MO
                            INNER JOIN Modelo M on M.id = MO.modelo
                            INNER JOIN AreaOperacion AO on AO.id  = MO.operacion
                            INNER JOIN Area A on A.id = AO.area
                            WHERE MO.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Operaciones()
                                    {
                                        Id = Utilitario.ToInt(dr["MOId"]),
                                        Costo = Utilitario.ToDecimal(dr["MOCosto"]),
                                        Tiempo = Utilitario.ToInt(dr["MOTiempo"]),
                                        Modelo = new entModelo.Modelo()
                                        {
                                            Id = Utilitario.ToInt(dr["MId"])
                                        },
                                        Operacion = new entArea.Operacion()
                                        {
                                            Id = Utilitario.ToInt(dr["AOId"]),
                                            Nombre = Utilitario.ToString(dr["AONombre"]),
                                            Area = new entArea.Area()
                                            {
                                                Id = Utilitario.ToInt(dr["AId"]),
                                                Nombre = Utilitario.ToString(dr["ANombre"])
                                            }
                                        }
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entModelo.Operaciones Ins(entModelo.Operaciones obj)
            {
                try
                {
                    entModelo.Operaciones r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            INSERT INTO ModeloOperacion(costo, tiempo, modelo, operacion) 
                            OUTPUT INSERTED.* 
                            VALUES(@costo, @tiempo, @modelo, @operacion)";
                            cmd.Parameters.AddWithValue("@costo", obj.Costo);
                            cmd.Parameters.AddWithValue("@tiempo", obj.Tiempo);
                            cmd.Parameters.AddWithValue("@modelo", obj.Modelo.Id);
                            cmd.Parameters.AddWithValue("@operacion", obj.Operacion.Id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entModelo.Operaciones()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Costo = Utilitario.ToDecimal(dr["costo"]),
                                        Tiempo = Utilitario.ToInt(dr["tiempo"]),
                                        Modelo = new entModelo.Modelo() { Id = Utilitario.ToInt(dr["modelo"]) },
                                        Operacion = new entArea.Operacion() { Id = Utilitario.ToInt(dr["operacion"]) },
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception ex) { throw; }
            }

            public bool Upd(entModelo.Operaciones obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                            UPDATE ModeloOperacion SET
	                            costo = @costo,
	                            tiempo = @tiempo
                            WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@costo", obj.Costo);
                            cmd.Parameters.AddWithValue("@tiempo", obj.Tiempo);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public bool Del(int id)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"DELETE ModeloOperacion WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

    }
}
