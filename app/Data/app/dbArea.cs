﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbArea : Conexion
    {

        public class Tipo
        {
            public IList<entArea.Tipo> Lis()
            {
                try
                {
                    IList<entArea.Tipo> r = new List<entArea.Tipo>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, estado as e FROM AreaTipo";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entArea.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public IList<entArea.Tipo> Search(entArea.Tipo obj)
            {
                try
                {
                    IList<entArea.Tipo> r = new List<entArea.Tipo>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT A.id as i, A.nombre as n, A.estado as e FROM AreaTipo A
                                WHERE (@id = 0 or A.id = @id) and
                                      (@nombre = '' or A.nombre like '%' + @nombre + '%') and
                                      (@estado = 0 or A.estado = @estado)
                                ORDER BY A.nombre asc";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@estado", obj.Estado);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entArea.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entArea.Tipo Get(int id)
            {
                try
                {
                    entArea.Tipo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, estado as e FROM AreaTipo WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entArea.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entArea.Tipo Ins(entArea.Tipo obj)
            {
                try
                {
                    entArea.Tipo r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO AreaTipo(nombre) OUTPUT INSERTED.* VALUES(@nombre)";
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entArea.Tipo()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entArea.Tipo obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE AreaTipo SET nombre = @nombre, estado = @estado WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@estado", obj.Estado);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class Area
        {
            public IList<entArea.Area> Lis()
            {
                try
                {
                    IList<entArea.Area> r = new List<entArea.Area>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT A.id as i, A.nombre as n, T.id as ti, T.nombre as tn, A.estado as e FROM Area A INNER JOIN AreaTipo T on T.id = A.tipo";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entArea.Area()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Tipo = new entArea.Tipo()
                                        {
                                            Id = Utilitario.ToInt(dr["ti"]),
                                            Nombre = Utilitario.ToString(dr["tn"])
                                        },
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entArea.Area Get(int id)
            {
                try
                {
                    entArea.Area r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT A.id as i, A.nombre as n, T.id as ti, T.nombre as tn, A.estado as e FROM Area A INNER JOIN AreaTipo T on T.id = A.tipo WHERE A.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entArea.Area()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Tipo = new entArea.Tipo()
                                        {
                                            Id = Utilitario.ToInt(dr["ti"]),
                                            Nombre = Utilitario.ToString(dr["tn"])
                                        },
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entArea.Area Ins(entArea.Area obj)
            {
                try
                {
                    entArea.Area r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO Area(nombre, tipo) OUTPUT INSERTED.* VALUES(@nombre, @tipo)";
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@tipo", obj.Tipo.Id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entArea.Area()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Tipo = new entArea.Tipo() { Id = Utilitario.ToInt(dr["tipo"]) },
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entArea.Area obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE Area SET nombre = @nombre, tipo = @tipo, estado = @estado WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@tipo", obj.Tipo.Id);
                            cmd.Parameters.AddWithValue("@estado", obj.Estado);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

        public class Operacion
        {
            public IList<entArea.Operacion> Lis()
            {
                try
                {
                    IList<entArea.Operacion> r = new List<entArea.Operacion>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT O.id as i, O.nombre as n, A.id as ti, A.nombre as tn, O.estado as e FROM AreaOperacion O INNER JOIN Area A on A.id = O.area";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entArea.Operacion()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Area = new entArea.Area()
                                        {
                                            Id = Utilitario.ToInt(dr["ti"]),
                                            Nombre = Utilitario.ToString(dr["tn"])
                                        },
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entArea.Operacion Get(int id)
            {
                try
                {
                    entArea.Operacion r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT O.id as i, O.nombre as n, A.id as ti, A.nombre as tn, O.estado as e FROM AreaOperacion O INNER JOIN Area A on A.id = O.area WHERE A.id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entArea.Operacion()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Area = new entArea.Area()
                                        {
                                            Id = Utilitario.ToInt(dr["ti"]),
                                            Nombre = Utilitario.ToString(dr["tn"])
                                        },
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

            public entArea.Operacion Ins(entArea.Operacion obj)
            {
                try
                {
                    entArea.Operacion r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO AreaOperacion(nombre, area) OUTPUT INSERTED.* VALUES(@nombre, @area)";
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@area", obj.Area.Id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entArea.Operacion()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Area = new entArea.Area() { Id = Utilitario.ToInt(dr["area"]) },
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entArea.Operacion obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE Area SET nombre = @nombre, area = @area WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@area", obj.Area.Id);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }
        }

    }
}
