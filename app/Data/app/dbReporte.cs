﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbReporte
    {

        public class General : Conexion
        {
            public IList<entReporte.General> Lis(DateTime start, DateTime end)
            {
                try
                {
                    IList<entReporte.General> r = new List<entReporte.General>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("[dbo].[Rep_General]", con))
                        {

                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Start", start);
                            cmd.Parameters.AddWithValue("@End", end);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entReporte.General()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        Codigo = Utilitario.ToString(dr["Codigo"]),
                                        ClienteRUC = Utilitario.ToString(dr["ClienteRuc"]),
                                        ClienteNombre = Utilitario.ToString(dr["ClienteNombre"]),
                                        Inventario = Utilitario.ToDecimal(dr["Inventario"]),
                                        Operacion = Utilitario.ToDecimal(dr["Operacion"]),
                                        Costo = Utilitario.ToDecimal(dr["Costo"]),
                                        Precio = Utilitario.ToDecimal(dr["Precio"]),
                                        Throughput = Utilitario.ToDecimal(dr["Throughput"]),
                                        Porcentaje = Utilitario.ToString(dr["Porcentaje"]),
                                        FInicio = Utilitario.ToString(dr["FInicio"]),
                                        FEFin = Utilitario.ToString(dr["FEFin"]),
                                        FFin = Utilitario.ToString(dr["FFin"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }
        }

        public class ColaboradorPago : Conexion
        {
            public IList<entReporte.ColaboradorPago> Lis(DateTime start, DateTime end)
            {
                try
                {
                    IList<entReporte.ColaboradorPago> r = new List<entReporte.ColaboradorPago>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("[dbo].[Rep_ColaboradorPago]", con))
                        {

                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Start", start);
                            cmd.Parameters.AddWithValue("@End", end);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entReporte.ColaboradorPago()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        DNI = Utilitario.ToString(dr["Dni"]),
                                        Nombre = Utilitario.ToString(dr["Nombre"]),
                                        Area = Utilitario.ToString(dr["Area"]),
                                        Cantidad = Utilitario.ToInt(dr["Cantidad"]),
                                        Costo = Utilitario.ToDecimal(dr["Costo"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }
        }

    }
}
