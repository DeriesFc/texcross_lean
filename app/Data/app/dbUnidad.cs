﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbUnidad
    {
        public class Unidad : Conexion
        {
            public IList<entUnidad.Unidad> Lis()
            {
                try
                {
                    IList<entUnidad.Unidad> r = new List<entUnidad.Unidad>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, simbolo as s, estado as e FROM Unidad";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r.Add(new entUnidad.Unidad()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Simbolo = Utilitario.ToString(dr["s"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    });
                                }
                            }
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entUnidad.Unidad Get(int id)
            {
                try
                {
                    entUnidad.Unidad r = new entUnidad.Unidad();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "SELECT id as i, nombre as n, simbolo as s, estado as e FROM Unidad WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entUnidad.Unidad()
                                    {
                                        Id = Utilitario.ToInt(dr["i"]),
                                        Nombre = Utilitario.ToString(dr["n"]),
                                        Simbolo = Utilitario.ToString(dr["s"]),
                                        Estado = Utilitario.ToBool(dr["e"])
                                    };
                                }
                            }
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

            public entUnidad.Unidad Ins(entUnidad.Unidad obj)
            {
                try
                {
                    entUnidad.Unidad r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "INSERT INTO Unidad(nombre, simbolo) OUTPUT INSERTED.* VALUES(@nombre, @simbolo)";
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@simbolo", obj.Simbolo);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entUnidad.Unidad()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Nombre = Utilitario.ToString(dr["nombre"]),
                                        Simbolo = Utilitario.ToString(dr["simbolo"]),
                                        Estado = Utilitario.ToBool(dr["estado"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Upd(entUnidad.Unidad obj)
            {
                try
                {
                    bool r = false;
                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("", con))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = "UPDATE Unidad SET nombre = @nombre, simbolo = @simbolo, estado = @estado WHERE id = @id";
                            cmd.Parameters.AddWithValue("@id", obj.Id);
                            cmd.Parameters.AddWithValue("@nombre", obj.Nombre);
                            cmd.Parameters.AddWithValue("@simbolo", obj.Simbolo);
                            cmd.Parameters.AddWithValue("@estado", obj.Estado);
                            r = cmd.ExecuteNonQuery() > 0 ? true : false;
                        }
                    }
                    return r;
                }
                catch (Exception) { throw; }
            }

        }
    }
}
