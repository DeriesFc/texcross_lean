﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dbPedido
    {

        public class Pedido : Conexion
        {

            public IList<entPedido.PedidoVO> Lis()
            {
                try
                {
                    IList<entPedido.PedidoVO> r = new List<entPedido.PedidoVO>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT
	                                P.id Id,
	                                P.cod Codigo,
	                                CONCAT(C.ruc, ' - ', C.nombre) Cliente,
	                                PDC.CInventario + PDCD.cOperacion Costo,
	                                P.precio Precio,
	                                PD.cant Cantidad,
	                                PDCD.porcentaje Porcentaje,
	                                CONVERT(varchar, P.fInicio, 103) FInicio,
	                                CONVERT(varchar, PD.fin, 103) FEFin,
	                                ISNULL(CONVERT(varchar, PDCD.ffin, 103), '---') FFin,
	                                CONCAT(
		                                CONVERT(varchar, P.fCreacion, 103), ' ',
		                                CONVERT(varchar, P.fCreacion, 8)
	                                ) FCreacion,
	                                PDCD.efin Completo
                                FROM
	                                Pedido P
                                inner join Cliente C on C.id = P.cliente
                                OUTER APPLY (SELECT count(1) cant, MAX(fEFin) fin FROM PedidoDetalle WHERE pedido = P.id) PD
                                OUTER APPLY (
	                                SELECT SUM(cTInventario) CInventario
	                                FROM Pedido P_
		                                INNER JOIN PedidoDetalle PD_ on PD_.pedido = P_.id
		                                INNER JOIN PedidoControl PDC_ on PDC_.pedidoDetalle = PD_.id
	                                WHERE P_.id = P.id
                                ) PDC
                                OUTER APPLY (
	                                SELECT
		                                CAST((SUM(CAST(PDCD_.estado as decimal(5,2))) * 100) / SUM(1) as decimal(12,2)) as porcentaje,
		                                SUM(PDCD_.cTOperacion) cOperacion,
		                                IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), MAX(PDCD_.fFin), NULL) ffin,
		                                CAST(IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), 1, 0) as bit) efin
	                                FROM Pedido P_
		                                    inner join PedidoDetalle PD_ on PD_.pedido = P_.id
		                                    inner join PedidoControl PDC_ on PDC_.pedidoDetalle = PD_.id
		                                    inner join PedidoControlDetalle PDCD_ on PDCD_.pedidoControl = PDC_.id
	                                WHERE P_.id = P.id
                                ) PDCD
                                WHERE P.estado = 1
                                ORDER BY P.fCreacion desc, Porcentaje asc
                            ";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entPedido.PedidoVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        Cod = Utilitario.ToString(dr["Codigo"]),
                                        Cliente = Utilitario.ToString(dr["Cliente"]),
                                        Costo = Utilitario.ToString(dr["Costo"]),
                                        Precio = Utilitario.ToString(dr["Precio"]),
                                        Cantidad = Utilitario.ToString(dr["Cantidad"]),
                                        Porcentaje = Utilitario.ToString(dr["Porcentaje"]),
                                        FInicio = Utilitario.ToString(dr["FInicio"]),
                                        FEFin = Utilitario.ToString(dr["FEFin"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        FCreacion = Utilitario.ToString(dr["FCreacion"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entPedido.Pedido Ins(entPedido.Pedido obj)
            {
                try
                {
                    entPedido.Pedido r = null;

                    DataTable TY_Detalle = new DataTable() { Columns = { "modelo", "cantidad", "fEfin" } };
                    foreach (entPedido.PedidoDetalle d in obj.Detalle) { TY_Detalle.Rows.Add(d.Modelo.Id, d.Cantidad, d.FEFin.ToString("yyyy-MM-dd")); }

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("[dbo].[Pedido_Ins]", con))
                        {

                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@cliente", obj.Cliente.Id);
                            cmd.Parameters.AddWithValue("@fInicio", obj.FInicio);
                            cmd.Parameters.AddWithValue("@detalle", TY_Detalle);
                            cmd.Parameters.AddWithValue("@precio", obj.Precio);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    string err = Utilitario.ToString(dr["msg"]);
                                    if (err != string.Empty) { throw new Personalized.PException(err); }

                                    r = new entPedido.Pedido()
                                    {
                                        Id = Utilitario.ToInt(dr["id"]),
                                        Cod = Utilitario.ToString(dr["cod"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoVO GetByCode(string code)
            {
                try
                {
                    entPedido.PedidoVO r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT
	                                P.id Id,
	                                P.cod Codigo,
	                                CONCAT(C.ruc, ' - ', C.nombre) Cliente,
	                                PDC.CInventario + PDCD.cOperacion Costo,
	                                P.precio Precio,
	                                PD.cant Cantidad,
	                                PDCD.porcentaje Porcentaje,
	                                CONVERT(varchar, P.fInicio, 103) FInicio,
	                                CONVERT(varchar, PD.fin, 103) FEFin,
	                                ISNULL(CONVERT(varchar, PDCD.ffin, 103), '---') FFin,
	                                CONCAT(
		                                CONVERT(varchar, P.fCreacion, 103), ' ',
		                                CONVERT(varchar, P.fCreacion, 8)
	                                ) FCreacion,
	                                PDCD.efin Completo
                                FROM
	                                Pedido P
                                inner join Cliente C on C.id = P.cliente
                                OUTER APPLY (SELECT count(1) cant, MAX(fEFin) fin FROM PedidoDetalle WHERE pedido = P.id) PD
                                OUTER APPLY (
	                                SELECT SUM(cTInventario) CInventario
	                                FROM Pedido P_
		                                INNER JOIN PedidoDetalle PD_ on PD_.pedido = P_.id
		                                INNER JOIN PedidoControl PDC_ on PDC_.pedidoDetalle = PD_.id
	                                WHERE P_.id = P.id
                                ) PDC
                                OUTER APPLY (
	                                SELECT
		                                CAST((SUM(CAST(PDCD_.estado as decimal(5,2))) * 100) / SUM(1) as decimal(12,2)) as porcentaje,
		                                SUM(PDCD_.cTOperacion) cOperacion,
		                                IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), MAX(PDCD_.fFin), NULL) ffin,
		                                CAST(IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), 1, 0) as bit) efin
	                                FROM Pedido P_
		                                    inner join PedidoDetalle PD_ on PD_.pedido = P_.id
		                                    inner join PedidoControl PDC_ on PDC_.pedidoDetalle = PD_.id
		                                    inner join PedidoControlDetalle PDCD_ on PDCD_.pedidoControl = PDC_.id
	                                WHERE P_.id = P.id
                                ) PDCD
                                WHERE P.cod = @Code
                                ORDER BY P.fCreacion desc, Porcentaje asc
                            ";
                            cmd.Parameters.AddWithValue("@Code", code);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entPedido.PedidoVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        Cod = Utilitario.ToString(dr["Codigo"]),
                                        Cliente = Utilitario.ToString(dr["Cliente"]),
                                        Costo = Utilitario.ToString(dr["Costo"]),
                                        Precio = Utilitario.ToString(dr["Precio"]),
                                        Cantidad = Utilitario.ToString(dr["Cantidad"]),
                                        Porcentaje = Utilitario.ToString(dr["Porcentaje"]),
                                        FInicio = Utilitario.ToString(dr["FInicio"]),
                                        FEFin = Utilitario.ToString(dr["FEFin"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        FCreacion = Utilitario.ToString(dr["FCreacion"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

        }

        public class PedidoDetalle : Conexion
        {

            public IList<entPedido.PedidoDetalleVO> Lis(int id)
            {
                try
                {
                    IList<entPedido.PedidoDetalleVO> r = new List<entPedido.PedidoDetalleVO>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT
	                                PD.id Id,
	                                PD.cod Cod,
	                                M.Tipo Tipo,
	                                M.Nombre Nombre,
	                                M.Talla Talla,
	                                M.Color Color,
	                                CONVERT(varchar, PD.fEFin, 103) FEFin,
	                                ISNULL(CONVERT(varchar, PDCD.ffin, 103), '---') FFin,
	                                PDC.Paq Paquete,
	                                PDC.Cant Cantidad,
	                                PDCD.cOperacion Operacion,
	                                PDC.CInventario Inventario,
	                                PDCD.cOperacion + PDC.CInventario Total,
	                                PDCD.Porcentaje Porcentaje,
	                                PDCD.efin Completo
                                FROM PedidoDetalle PD
                                OUTER APPLY (
	                                SELECT MT_.nombre Tipo, M_.nombre Nombre, CONCAT(TT_.nombre, ' - ', T_.nombre) Talla, C_.nombre Color
	                                FROM Modelo M_
	                                INNER JOIN ModeloTipo MT_ on MT_.id = M_.tipo
	                                INNER JOIN Color C_ on C_.id = M_.color
	                                INNER JOIN Talla T_ on T_.id = M_.talla
	                                INNER JOIN TallaTipo TT_ on TT_.id = T_.tipo
	                                WHERE M_.id = PD.modelo
                                ) M
                                OUTER APPLY (
	                                SELECT count(1) Paq, SUM(cantidad) Cant, SUM(cTInventario) CInventario FROM PedidoControl PDC_ WHERE PDC_.pedidoDetalle = PD.id
                                ) PDC
                                OUTER APPLY (
	                                SELECT
		                                CAST((SUM(CAST(PDCD_.estado as decimal(5,2))) * 100) / SUM(1) as decimal(12,2)) as porcentaje, 
		                                SUM(PDCD_.cTOperacion) cOperacion,
		                                IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), MAX(PDCD_.fFin), NULL) ffin,
		                                CAST(IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), 1, 0) as bit) efin
	                                FROM	PedidoDetalle PD_
			                                inner join PedidoControl PDC_ on PDC_.pedidoDetalle = PD_.id
			                                inner join PedidoControlDetalle PDCD_ on PDCD_.pedidoControl = PDC_.id
	                                WHERE PD_.id = PD.id
                                ) PDCD
                                WHERE PD.pedido = @id
                            ";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entPedido.PedidoDetalleVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        Cod = Utilitario.ToString(dr["Cod"]),
                                        Tipo = Utilitario.ToString(dr["Tipo"]),
                                        Nombre = Utilitario.ToString(dr["Nombre"]),
                                        Talla = Utilitario.ToString(dr["Talla"]),
                                        Color = Utilitario.ToString(dr["Color"]),
                                        FEFin = Utilitario.ToString(dr["FEFin"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        Paquete = Utilitario.ToInt(dr["Paquete"]),
                                        Cantidad = Utilitario.ToInt(dr["Cantidad"]),
                                        Operacion = Utilitario.ToDecimal(dr["Operacion"]),
                                        Inventario = Utilitario.ToDecimal(dr["Inventario"]),
                                        Total = Utilitario.ToDecimal(dr["Total"]),
                                        Porcentaje = Utilitario.ToString(dr["Porcentaje"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoDetalleVO GetByCode(int id, string code)
            {
                try
                {
                    entPedido.PedidoDetalleVO r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT
	                                PD.id Id,
	                                PD.cod Cod,
	                                M.Tipo Tipo,
	                                M.Nombre Nombre,
	                                M.Talla Talla,
	                                M.Color Color,
	                                CONVERT(varchar, PD.fEFin, 103) FEFin,
	                                ISNULL(CONVERT(varchar, PDCD.ffin, 103), '---') FFin,
	                                PDC.Paq Paquete,
	                                PDC.Cant Cantidad,
	                                PDCD.cOperacion Operacion,
	                                PDC.CInventario Inventario,
	                                PDCD.cOperacion + PDC.CInventario Total,
	                                PDCD.Porcentaje Porcentaje,
	                                PDCD.efin Completo
                                FROM PedidoDetalle PD
                                OUTER APPLY (
	                                SELECT MT_.nombre Tipo, M_.nombre Nombre, CONCAT(TT_.nombre, ' - ', T_.nombre) Talla, C_.nombre Color
	                                FROM Modelo M_
	                                INNER JOIN ModeloTipo MT_ on MT_.id = M_.tipo
	                                INNER JOIN Color C_ on C_.id = M_.color
	                                INNER JOIN Talla T_ on T_.id = M_.talla
	                                INNER JOIN TallaTipo TT_ on TT_.id = T_.tipo
	                                WHERE M_.id = PD.modelo
                                ) M
                                OUTER APPLY (
	                                SELECT count(1) Paq, SUM(cantidad) Cant, SUM(cTInventario) CInventario FROM PedidoControl PDC_ WHERE PDC_.pedidoDetalle = PD.id
                                ) PDC
                                OUTER APPLY (
	                                SELECT
		                                CAST((SUM(CAST(PDCD_.estado as decimal(5,2))) * 100) / SUM(1) as decimal(12,2)) as porcentaje, 
		                                SUM(PDCD_.cTOperacion) cOperacion,
		                                IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), MAX(PDCD_.fFin), NULL) ffin,
		                                CAST(IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), 1, 0) as bit) efin
	                                FROM	PedidoDetalle PD_
			                                inner join PedidoControl PDC_ on PDC_.pedidoDetalle = PD_.id
			                                inner join PedidoControlDetalle PDCD_ on PDCD_.pedidoControl = PDC_.id
	                                WHERE PD_.id = PD.id
                                ) PDCD
                                WHERE 
                                        PD.cod = @Code
                                    and PD.pedido = @Id
                            ";
                            cmd.Parameters.AddWithValue("@Code", code);
                            cmd.Parameters.AddWithValue("@Id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entPedido.PedidoDetalleVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        Cod = Utilitario.ToString(dr["Cod"]),
                                        Tipo = Utilitario.ToString(dr["Tipo"]),
                                        Nombre = Utilitario.ToString(dr["Nombre"]),
                                        Talla = Utilitario.ToString(dr["Talla"]),
                                        Color = Utilitario.ToString(dr["Color"]),
                                        FEFin = Utilitario.ToString(dr["FEFin"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        Paquete = Utilitario.ToInt(dr["Paquete"]),
                                        Cantidad = Utilitario.ToInt(dr["Cantidad"]),
                                        Operacion = Utilitario.ToDecimal(dr["Operacion"]),
                                        Inventario = Utilitario.ToDecimal(dr["Inventario"]),
                                        Total = Utilitario.ToDecimal(dr["Total"]),
                                        Porcentaje = Utilitario.ToString(dr["Porcentaje"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

        }

        public class PedidoDetalleControl : Conexion
        {

            public IList<entPedido.PedidoDetalleControlVO> Lis(int id)
            {
                try
                {
                    IList<entPedido.PedidoDetalleControlVO> r = new List<entPedido.PedidoDetalleControlVO>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT
	                                PDC.id Id,
	                                PDC.cod Cod,
	                                PDC.cantidad Cantidad,
	                                PDC.cTInventario Inventario,
	                                PDCD.cOperacion Operacion,
	                                PDC.cTInventario + PDCD.cOperacion Total,
	                                ISNULL(CONVERT(varchar, PDCD.ffin, 103), '---') FFin,
	                                PDCD.Porcentaje Porcentaje,
	                                PDCD.efin Completo
                                FROM PedidoControl PDC
                                OUTER APPLY (
	                                SELECT
		                                CAST((SUM(CAST(PDCD_.estado as decimal(5,2))) * 100) / SUM(1) as decimal(12,2)) as porcentaje, 
		                                SUM(PDCD_.cTOperacion) cOperacion,
		                                IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), MAX(PDCD_.fFin), NULL) ffin,
		                                CAST(IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), 1, 0) as bit) efin
	                                FROM PedidoControlDetalle PDCD_
	                                WHERE PDCD_.pedidoControl = PDC.id
                                ) PDCD
                                WHERE 
	                                PDC.pedidoDetalle = @id
                            ";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entPedido.PedidoDetalleControlVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        Cod = Utilitario.ToString(dr["Cod"]),
                                        Cantidad = Utilitario.ToString(dr["Cantidad"]),
                                        Inventario = Utilitario.ToString(dr["Inventario"]),
                                        Operacion = Utilitario.ToString(dr["Operacion"]),
                                        Total = Utilitario.ToString(dr["Total"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        Porcentaje = Utilitario.ToString(dr["Porcentaje"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoDetalleControlVO GetByCode(int id, string code)
            {
                try
                {
                    entPedido.PedidoDetalleControlVO r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT
	                                PDC.id Id,
	                                PDC.cod Cod,
	                                PDC.cantidad Cantidad,
	                                PDC.cTInventario Inventario,
	                                PDCD.cOperacion Operacion,
	                                PDC.cTInventario + PDCD.cOperacion Total,
	                                ISNULL(CONVERT(varchar, PDCD.ffin, 103), '---') FFin,
	                                PDCD.Porcentaje Porcentaje,
	                                PDCD.efin Completo
                                FROM PedidoControl PDC
                                OUTER APPLY (
	                                SELECT
		                                CAST((SUM(CAST(PDCD_.estado as decimal(5,2))) * 100) / SUM(1) as decimal(12,2)) as porcentaje, 
		                                SUM(PDCD_.cTOperacion) cOperacion,
		                                IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), MAX(PDCD_.fFin), NULL) ffin,
		                                CAST(IIF(COUNT(1) = SUM(CAST(PDCD_.estado as decimal(5,2))), 1, 0) as bit) efin
	                                FROM PedidoControlDetalle PDCD_
	                                WHERE PDCD_.pedidoControl = PDC.id
                                ) PDCD
                                WHERE 
                                        PDC.cod = @Code
                                    and PDC.pedidoDetalle = @Id
                            ";
                            cmd.Parameters.AddWithValue("@Code", code);
                            cmd.Parameters.AddWithValue("@Id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entPedido.PedidoDetalleControlVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        Cod = Utilitario.ToString(dr["Cod"]),
                                        Cantidad = Utilitario.ToString(dr["Cantidad"]),
                                        Inventario = Utilitario.ToString(dr["Inventario"]),
                                        Operacion = Utilitario.ToString(dr["Operacion"]),
                                        Total = Utilitario.ToString(dr["Total"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        Porcentaje = Utilitario.ToString(dr["Porcentaje"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

        }

        public class PedidoDetalleControlDetalle : Conexion
        {

            public IList<entPedido.PedidoDetalleControlDetalleVO> Lis(int id)
            {
                try
                {
                    IList<entPedido.PedidoDetalleControlDetalleVO> r = new List<entPedido.PedidoDetalleControlDetalleVO>();

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT 
	                                PDCD.id Id,
                                    PDCD.operacion OperacionId,
                                    CONCAT(A.nombre, ' - ', O.nombre) Operacion,
	                                PDCD.cTOperacion Costo,
	                                IIF(PDCD.estado = 1, (IIF(p.apellido is null, '---', CONCAT(p.apellido, ', ', p.nombre))), '---') Persona,
	                                IIF(PDCD.estado = 1, (ISNULL(CONVERT(varchar, PDCD.fFin, 103), '---')), '---') FFin,
	                                PDCD.estado Completo
                                FROM PedidoControlDetalle PDCD
                                INNER JOIN AreaOperacion O on O.id = PDCD.operacion
                                INNER JOIN Area A on A.id = O.area
                                LEFT JOIN Persona P on P.id = PDCD.persona
                                WHERE PDCD.pedidoControl = @id
                                ORDER BY PDCD.id asc
                            ";
                            cmd.Parameters.AddWithValue("@id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {

                                    r.Add(new entPedido.PedidoDetalleControlDetalleVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        OperacionId = Utilitario.ToInt(dr["OperacionId"]),
                                        Operacion = Utilitario.ToString(dr["Operacion"]),
                                        Costo = Utilitario.ToString(dr["Costo"]),
                                        Persona = Utilitario.ToString(dr["Persona"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    });
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public entPedido.PedidoDetalleControlDetalleVO GetByCode(int id)
            {
                try
                {
                    entPedido.PedidoDetalleControlDetalleVO r = null;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("", con))
                        {

                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.CommandText = @"
                                SELECT 
	                                PDCD.id Id,
                                    PDCD.operacion OperacionId,
                                    CONCAT(A.nombre, ' - ', O.nombre) Operacion,
	                                PDCD.cTOperacion Costo,
	                                IIF(PDCD.estado = 1, (IIF(p.apellido is null, '---', CONCAT(p.apellido, ' - ', p.nombre))), '---') Persona,
	                                IIF(PDCD.estado = 1, (ISNULL(CONVERT(varchar, PDCD.fFin, 103), '---')), '---') FFin,
	                                PDCD.estado Completo
                                FROM PedidoControlDetalle PDCD
                                INNER JOIN AreaOperacion O on O.id = PDCD.operacion
                                INNER JOIN Area A on A.id = O.area
                                LEFT JOIN Persona P on P.id = PDCD.persona
                                WHERE PDCD.id = @Id
                            ";
                            cmd.Parameters.AddWithValue("@Id", id);

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    r = new entPedido.PedidoDetalleControlDetalleVO()
                                    {
                                        Id = Utilitario.ToInt(dr["Id"]),
                                        OperacionId = Utilitario.ToInt(dr["OperacionId"]),
                                        Operacion = Utilitario.ToString(dr["Operacion"]),
                                        Costo = Utilitario.ToString(dr["Costo"]),
                                        Persona = Utilitario.ToString(dr["Persona"]),
                                        FFin = Utilitario.ToString(dr["FFin"]),
                                        Completo = Utilitario.ToBool(dr["Completo"])
                                    };
                                }
                            }
                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }
            }

            public bool Terminar(int id, int persona)
            {

                try
                {
                    bool r = false;

                    using (SqlConnection con = new SqlConnection(Conexion.cnString))
                    {
                        con.Open();

                        using (SqlCommand cmd = new SqlCommand("[PedidoDetalleControlDetalle_Terminar]", con))
                        {

                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@id", id);
                            cmd.Parameters.AddWithValue("@persona", persona);

                            r = (cmd.ExecuteNonQuery() > 0) ? true : false;

                        }
                    }
                    return r;

                }
                catch (Exception) { throw; }

            }

        }

    }
}
