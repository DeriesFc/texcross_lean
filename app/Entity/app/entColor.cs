﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class entColor
    {
        public class Color
        {
            public int Id { get; set; } = 0;
            public string Nombre { get; set; } = string.Empty;
            public bool Estado { get; set; } = false;
        }
    }
}
