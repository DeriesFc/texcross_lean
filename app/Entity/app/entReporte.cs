﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class entReporte
    {

        public class General
        {
            public int Id { get; set; }
            public string Codigo { get; set; }
            public string ClienteRUC { get; set; }
            public string ClienteNombre { get; set; }
            public decimal Inventario { get; set; }
            public decimal Operacion { get; set; }
            public decimal Costo { get; set; }
            public decimal Precio { get; set; }
            public decimal Throughput { get; set; }
            public string Porcentaje { get; set; }
            public string FInicio { get; set; }
            public string FEFin { get; set; }
            public string FFin { get; set; }
        }

        public class ColaboradorPago
        {
            public int Id { get; set; }
            public string DNI { get; set; }
            public string Nombre { get; set; }
            public string Area { get; set; }
            public int Cantidad { get; set; }
            public decimal Costo { get; set; }
        }

    }
}
