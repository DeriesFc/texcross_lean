﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class entPedido
    {

        public class Pedido
        {
            public int Id { get; set; }
            public string Cod { get; set; }
            public entCliente.Cliente Cliente { get; set; }
            public DateTime FCreación { get; set; }
            public DateTime FInicio { get; set; }
            public DateTime? FFin { get; set; }
            public decimal Costo { get; set; }
            public decimal Precio { get; set; }
            public IList<PedidoDetalle> Detalle { get; set; }
        }

        public class PedidoVO
        {
            public int Id { get; set; }
            public string Cod { get; set; }
            public string Cliente { get; set; }
            public string Cantidad { get; set; }
            public string Costo { get; set; }
            public string Precio { get; set; }
            public string Porcentaje { get; set; }
            public string FInicio { get; set; }
            public string FEFin { get; set; }
            public string FFin { get; set; }
            public string FCreacion { get; set; }
            public bool Completo { get; set; }
        }

        public class PedidoDetalle
        {
            public int Id { get; set; }
            public string Cod { get; set; }
            public entModelo.Modelo Modelo { get; set; }
            public int Cantidad { get; set; }
            public DateTime FEFin { get; set; }
            public int Pedido { get; set; }
        }

        public class PedidoDetalleVO
        {
            public int Id { get; set; }
            public string Cod { get; set; }
            public string Tipo { get; set; }
            public string Nombre { get; set; }
            public string Talla { get; set; }
            public string Color { get; set; }
            public string FEFin { get; set; }
            public string FFin { get; set; }
            public int Paquete { get; set; }
            public int Cantidad { get; set; }
            public decimal Operacion { get; set; }
            public decimal Inventario { get; set; }
            public decimal Total { get; set; }
            public string Porcentaje { get; set; }
            public bool Completo { get; set; }
        }

        public class Control
        {
            public int Id { get; set; }
            public string Cod { get; set; }
            public int Cantidad { get; set; }
            public decimal CInventario { get; set; }
            public decimal CTInventario { get; set; }
            public int Detalle { get; set; }
        }

        public class PedidoDetalleControlVO
        {
            public int Id { get; set; }
            public string Cod { get; set; }
            public string Cantidad { get; set; }
            public string Inventario { get; set; }
            public string Operacion { get; set; }
            public string Total { get; set; }
            public string FFin { get; set; }
            public string Porcentaje { get; set; }
            public bool Completo { get; set; }
        }

        public class ControlDetalle
        {
            public int Id { get; set; }
            public int Operacion { get; set; }
            public decimal COperacion { get; set; }
            public decimal CTOperacion { get; set; }
            public entPersona.Persona Persona { get; set; }
            public DateTime? FFin { get; set; }
            public bool Estado { get; set; }
            public int Control { get; set; }
        }

        public class PedidoDetalleControlDetalleVO
        {
            public int Id { get; set; }
            public int OperacionId { get; set; }
            public string Operacion { get; set; }
            public string Costo { get; set; }
            public string Persona { get; set; }
            public string FFin { get; set; }
            public bool Completo { get; set; }
        }

    }
}
