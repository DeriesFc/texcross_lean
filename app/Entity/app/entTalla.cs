﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class entTalla
    {

        public class Talla
        {
            public int Id { get; set; } = 0;
            public string Nombre { get; set; } = string.Empty;
            public Tipo Tipo { get; set; }
            public bool Estado { get; set; } = false;
        }

        public class Tipo
        {
            public int Id { get; set; } = 0;
            public string Nombre { get; set; } = string.Empty;
            public bool Estado { get; set; } = false;
        }

    }
}
