﻿using static Entity.entCliente;
using static Entity.entColor;
using static Entity.entTalla;
using static Entity.entSuministro;
using static Entity.entArea;
using System.Collections.Generic;

namespace Entity
{
    public class entModelo
    {

        public class Tipo
        {
            public int Id { get; set; } = 0;
            public string Nombre { get; set; } = string.Empty;
            public bool Estado { get; set; } = false;
        }

        public class Modelo
        {
            public int Id { get; set; }
            public Tipo Tipo { get; set; }
            public string Nombre { get; set; }
            public Talla Talla { get; set; }
            public Color Color { get; set; }
            public Cliente Creador { get; set; }
            public bool Estado { get; set; }

            public Operaciones Operacion { get; set; }
            public Suministros Suministro { get; set; }

        }

        public class Suministros
        {
            public int Id { get; set; }
            public decimal Cantidad { get; set; }
            public decimal Precio { get; set; }
            public Modelo Modelo { get; set; }
            public Suministro Suministro { get; set; }
        }

        public class Operaciones
        {
            public int Id { get; set; }
            public int Cantidad { get; set; }
            public decimal Costo { get; set; }
            public int Tiempo { get; set; }
            public Modelo Modelo { get; set; }
            public Operacion Operacion { get; set; }
        }

    }
}
