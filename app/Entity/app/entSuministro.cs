﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class entSuministro
    {
        public class Suministro
        {
            public int Id { get; set; }
            public Tipo Tipo { get; set; }
            public string Nombre { get; set; }
            public entColor.Color Color { get; set; }
            public entUnidad.Unidad Unidad { get; set; }
            public decimal Cantidad { get; set; }
            public decimal Precio { get; set; }
            public bool Estado { get; set; }
        }

        public class Tipo
        {
            public int Id { get; set; } = 0;
            public string Nombre { get; set; } = string.Empty;
            public bool Estado { get; set; } = false;
        }
    }
}

