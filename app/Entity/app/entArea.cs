﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class entArea
    {

        public class Tipo
        {
            public int Id { get; set; } = 0;
            public string Nombre { get; set; } = string.Empty;
            public bool Estado { get; set; } = false;
        }

        public class Area
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
            public Tipo Tipo { get; set; }
            public bool Estado { get; set; } = false;
        }

        public class Operacion
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
            public Area Area { get; set; }
            public bool Estado { get; set; }
        }
    }
}