﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class entUnidad
    {
        public class Unidad
        {
            public int Id { get; set; } = 0;
            public string Nombre { get; set; } = string.Empty;
            public string Simbolo { get; set; } = string.Empty;
            public bool Estado { get; set; } = false;
        }
    }
}
