﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class entPersona
    {
        public class Persona
        {
            public int Id { get; set; }
            public string Dni { get; set; }
            public string Nombre { get; set; }
            public string Apellido { get; set; }
            public string Clave { get; set; }
            public Tipo Tipo { get; set; }
            public entArea.Area Area { get; set; }
            public bool Estado { get; set; }
        }

        public class PersonaS2VO
        {
            public int I { get; set; }
            public string N { get; set; }
        }

        public class Tipo
        {
            public int Id { get; set; }
            public string Nombre { get; set; }
            public bool Acceso { get; set; }
        }
    }

}
