﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class entCliente
    {

        public class Cliente
        {
            public int Id { get; set; }
            public string Ruc { get; set; }
            public string Nombre { get; set; }
            public bool Estado { get; set; }
        }

    }
}
