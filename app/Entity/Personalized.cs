﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Personalized
    {

        public class PException : Exception
        {
            public PClass Obj { get; set; }
            public PException(string mensaje) : base(mensaje) => Obj = new PClass() { Mensaje = mensaje };
        }

        public class PClass
        {
            public bool SvcError { get; set; } = true;
            public string Mensaje { get; set; } = string.Empty;
        }

    }
}
