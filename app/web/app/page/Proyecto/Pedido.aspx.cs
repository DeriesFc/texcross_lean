﻿using Entity;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.app.page.Proyecto
{
    public partial class Pedido : System.Web.UI.Page
    {
        public string CPedido { get; set; }
        public entPedido.PedidoVO OCPedido;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CPedido = Page.RouteData.Values["cPedido"] as string ?? null;
                if (CPedido == null) { throw new Exception(); }
                OCPedido = new lgPedido.Pedido().GetByCode(CPedido);
                if (OCPedido == null) { throw new Exception(); }
            }
            catch (Exception)
            {
                HttpContext.Current.Response.Redirect("/Proyecto/Listar");
            }

        }
    }
}