﻿using Entity;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.app.page.Proyecto
{
    public partial class PedidoDetalleControl : System.Web.UI.Page
    {

        public string CPedido { get; set; }
        public entPedido.PedidoVO OCPedido;

        public string CPedidoDetalle { get; set; }
        public entPedido.PedidoDetalleVO OCPedidoDetalle;

        public string CPedidoDetalleControl { get; set; }
        public entPedido.PedidoDetalleControlVO OCPedidoDetalleControl;

        protected void Page_Load(object sender, EventArgs e)
        {
            string r = string.Empty;

            try
            {
                CPedido = Page.RouteData.Values["cPedido"] as string ?? null;

                if (CPedido == null)
                {
                    r = "/Proyecto/Listar";
                    throw new Exception();
                }

                OCPedido = new lgPedido.Pedido().GetByCode(CPedido);
                if (OCPedido == null)
                {
                    r = "/Proyecto/Listar";
                    throw new Exception();
                }

                CPedidoDetalle = Page.RouteData.Values["cPedidoDetalle"] as string ?? null;
                if (CPedidoDetalle == null)
                {
                    r = $"/Proyecto/{CPedido}";
                    throw new Exception();
                }

                OCPedidoDetalle = new lgPedido.PedidoDetalle().GetByCode(OCPedido.Id, CPedidoDetalle);
                if (OCPedidoDetalle == null)
                {
                    r = $"/Proyecto/{CPedido}";
                    throw new Exception();
                }

                CPedidoDetalleControl = Page.RouteData.Values["cPedidoDetalleControl"] as string ?? null;
                if (CPedidoDetalleControl == null)
                {
                    r = $"/Proyecto/{CPedido}/{CPedidoDetalle}";
                    throw new Exception();
                }

                OCPedidoDetalleControl = new lgPedido.PedidoDetalleControl().GetByCode(OCPedidoDetalle.Id, CPedidoDetalleControl);
                if (OCPedidoDetalleControl == null)
                {
                    r = $"/Proyecto/{CPedido}/{CPedidoDetalle}";
                    throw new Exception();
                }

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect(r == string.Empty ? "/Proyecto/Listar" : r);
            }
        }
    }
}