﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/admin.Master" AutoEventWireup="true" CodeBehind="Crear.aspx.cs" Inherits="web.app.page.Proyecto.Crear" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Crear proyecto • TexCross
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Crear un proyecto</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="javascript:;">Proyecto(s)</a></li>
                <li class="active"><strong>Crear</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Orden de pedido</h5>
                    <div class="ibox-tools">
                        <a class="tbl_btnAdd" href="javascript:;">
                            <i class="fa fa-plus color-black"></i><span class="color-black font-bold pl-2">Agregar Detalle</span>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">

                    <div class="row">

                        <div class="col-xs-12 col-md-4">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Cliente</h5>
                                <select class="s2_Cliente form-control input-sm" name="state"></select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Costo</h5>
                                <input type="number" class="num_Costo form-control input-sm" disabled />
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Precio (Costo * 1.25)</h5>
                                <input type="number" class="num_Precio form-control input-sm" />
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Fecha inicio</h5>
                                <input type="text" class="dt_Inicio form-control input-sm" disabled />
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col xs 12">
                            <div class="form-group mb-10">
                                
                                <div class="col-xs-12">
                                    <h5 class="font-bold mt-0 mb-5">Modelo(s)</h5>
                                </div>

                                <div id="tblDetalle" class="col-xs-12 mb-10">

                                    <div class="col-xs-12 p-0 oFlow-x-auto">

                                        <table class="table table-striped table-bordered table-hover m-0">

                                            <thead>
                                                <tr>
                                                    <th class="tCenter bg-white" colspan="4">Modelo</th>
                                                    <th class="tCenter bg-white" colspan="2">Unidad</th>
                                                    <th class="tCenter bg-white" colspan="1">Pedido</th>
                                                    <th class="tCenter bg-white" colspan="3">Fecha</th>
                                                    <th class="tCenter bg-white" rowspan="2"></th>
                                                </tr>
                                                <tr>
                                                    <th>Tipo</th>
                                                    <th>Nombre</th>
                                                    <th>Talla</th>
                                                    <th>Color</th>
                                                    <th class="tRight">Costo</th>
                                                    <th class="tRight">Tiempo</th>
                                                    <th class="tRight">Cantidad</th>
                                                    <th class="tCenter">Dia(s)</th>
                                                    <th class="tCenter">Estimado</th>
                                                    <th class="tCenter">Final (+2)</th>
                                                </tr>
                                            </thead>

                                            <tbody></tbody>

                                        </table>

                                    </div>

                                </div>
                            
                                <div class="col-xs-12 col-md-offset-9 col-md-3 col-lg-offset-10 col-lg-2">
                                    <button type="button" class="btn_CrearOrden btn btn-block btn-sm btn-info">Crear orden</button>
                                </div>

                            </div>
                        
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- endRegion Modal -->

    <div id="MD_ModeloLis" class="modal modal-dialog-center fade" aria-hidden="true" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body pt-20 pb-20">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="bd-bottom-gray-1 font-bold pb-5 mt-0 mb-10">Seleccionar modelo</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div id="MD_tblDetalle" class="col-xs-12"></div>
                    </div>

                    <hr class="mt-5 mb-10 ccc" />
                    <div class="row">
                        <div class="col-xs-12 tRight">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="MD_Result" class="modal modal-dialog-center fade" aria-hidden="true" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
        </div>
    </div>

    <!-- endRegion Modal -->


</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script id="HB_MD_tblModelo" type="text/x-handlebars-template">

        <div class="col-xs-12 p-0 oFlow-x-auto">

            <table class="table table-striped table-bordered table-hover m-0">
                <thead>
                    <tr>
                        <th class="tCenter bg-white" colspan="5">Modelo</th>
                        <th class="tCenter bg-white" rowspan="2"></th>
                    </tr>
                    <tr>
                        <th>Tipo</th>
                        <th>Nombre</th>
                        <th>Talla</th>
                        <th>Color</th>
                        <th>Creador</th>
                    </tr>
                </thead>
                <tbody>
                    {{#each arr}}
                    <tr data-id="{{Id}}">
                        <td>{{Tipo.Nombre}}</td>
                        <td>{{Nombre}}</td>
                        <td>{{Talla.Tipo.Nombre}} - {{Talla.Nombre}}</td>
                        <td>{{Color.Nombre}}</td>
                        <td>{{Creador.Nombre}}</td>
                        <td class="tCenter">
                            <button type="button" class="btn-SelectModelo btn btn-xs btn-success"><i class="fa fa-check"></i></button>
                        </td>
                    </tr>
                    {{/each}}
                </tbody>
            </table>

        </div>

    </script>

    <script id="HB_tblDetalle_tr" type="text/x-handlebars-template">
        
        <tr data-id="{{r.Id}}" data-uc="{{r.TCosto}}" data-ut="{{r.TTiempo}}"> 
            <td>{{r.Tipo.Nombre}}</td>
            <td>{{r.Nombre}}</td>
            <td>{{r.Talla.Tipo.Nombre}} - {{r.Talla.Nombre}}</td>
            <td>{{r.Color.Nombre}}</td>
            <td class="tRight">{{r.TCosto}}</td>
            <td class="tRight">{{r.TTiempo}}</td>
            <td class="tRight"><input class="iCantidad tRight" style="width: 60px" type="number" value="1"></td>
            <td class="tRight"><span class="iDia"></span></td>
            <td class="tRight"><span class="iFecha"></span></td>
            <td class="tRight"><span class="iFin"></span></td>
            <td class="tCenter">
                <button type="button" class="btn_TRDelete btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
            </td>
        </tr>

    </script>

    <script id="HB_Result" type="text/x-handlebars-template">

        <div class="row">
            <div class="col-xs-12">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="bd-bottom-gray-1 font-bold pb-5 m-0">Resultado</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h6 class="font-bold">Código:</h6>
                <h4 class=" mt-0 tCenter">{{r.Cod}}</h4>
            </div>
        </div>

        <hr class="mt-5 mb-10 ccc" />

        <div class="row">
            <div class="col-xs-12 tCenter">
                <button type="button" class="btn btn-sm btn-warning" onClick="window.location.href='/Proyecto/Listar'"><i class="fa fa-arrow-left" aria-hidden="true"></i> Lista de proyectos</button>
                <button type="button" class="btn btn-sm btn-info" onClick="window.location.href='/Proyecto/Crear'">Crear otro</button>
                <button type="button" class="btn btn-sm btn-primary" onClick="window.location.href='/Proyecto/{{r.Cod}}'"> Ver {{r.Cod}}</button>
            </div>
        </div>

    </script>


    <script type="text/javascript">

        var app = new function () {

            this.obj = {
                fInicio: null,
            }

            this.el = {

                s2_Cliente: '.s2_Cliente',
                num_Costo: '.num_Costo',
                num_Precio: '.num_Precio',
                fInicio: '.dt_Inicio',

                tblDetalle_add: '.tbl_btnAdd',
                tblDetalle: '#tblDetalle',
                tblDetalle_del: '.btn_TRDelete',

                btnCrear: '.btn_CrearOrden',

                md_ModeloLis: '#MD_ModeloLis',
                tbl_MD_Detalle: '#MD_tblDetalle'

            }

            this.lg = {

                getCliente: async () => {
                    try {
                        let r = await jsServer.ARes("Cliente_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getModelLisValid: async () => {
                    try {
                        let r = await jsServer.ARes("Modelo_LisValid");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getModelo: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Modelo_Get", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                insOrden: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Pedido_Ins", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                }

            }

            this.init = async () => {
                try {

                    this.fn.setCliente();
                    this.on.click_openAdd();

                    this.obj.fInicio = this.fn.cal_date_byDays(moment().format('DD/MM/YYYY'), 1, 0);
                    $(this.el.fInicio).val(this.obj.fInicio);

                    this.fn.tbl_Operar();

                    this.on.click_btn_Crear();

                } catch (e) {
                    jsUtil.Toastr.Show('w', 'Dificultad', e);
                }
            }

            this.fn = {

                setCliente: async () => {
                    try {

                        await jsUtil.Select.init(`${this.el.s2_Cliente}`);
                        await jsUtil.Select.Loading(`${this.el.s2_Cliente}`);

                        await jsUtil.Select.LoadArr(`${this.el.s2_Cliente}`, await this.lg.getCliente(), 'Id', 'Nombre', false);

                    } catch (e) { throw 'Al obtener clientes'; }
                },

                tbl_Operar: async () => {

                    try {

                        let tCosto = 0;

                        $(`${this.el.tblDetalle} table tbody`).find('tr').toArray().forEach((o, i) => {
                            let cantidad = $(o).find('.iCantidad').val() || 0;
                            if (cantidad == 0) {
                                $(o).find('.iCantidad').val("1");
                                cantidad = 1;
                                jsUtil.Toastr.Show('w', 'Dificultad', `Modelo [${i}]: Cantidad no válida`);
                            }
                            else if (cantidad > 10000) {
                                $(o).find('.iCantidad').val("10000");
                                cantidad = 10000;
                                jsUtil.Toastr.Show('w', 'Dificultad', `Modelo [${i}]: La cantidad no puede ser mayor a 10k`);
                            }
                            
                            let costo = $(o).data('uc');
                            tCosto = (parseFloat(tCosto) + (parseFloat(costo) * cantidad)).toFixed(2);

                            let days = this.fn.cal_days_bySeg($(o).data('ut') * cantidad);

                            $(o).find('.iDia').text(days);
                            $(o).find('.iFecha').text(this.fn.cal_date_byDays(this.obj.fInicio, days));
                            $(o).find('.iFin').text(this.fn.cal_date_byDays(this.obj.fInicio, days + 2));

                        });

                        $(this.el.num_Costo).val(tCosto);
                        $(this.el.num_Precio).val((tCosto * 1.25).toFixed(2));

                    } catch (e) {
                        $(this.el.md_ModeloLis).modal('hide');
                        jsUtil.Toastr.Remove();
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }

                },

                cal_days_bySeg: (d) => { return Math.ceil(((d/60)/60)/8); },

                cal_date_byDays: (date, day, r = 1) => {

                    let base = moment(date, 'DD/MM/YYYY');
                    day = day - r;
                    while (day > 0) {
                        base.add('days', 1);
                        if (base.day() != 0) { day--; }
                    }
                    return base.format('DD/MM/YYYY');
                },

                md_ModeloLis: async () => {

                    jsUtil.Toastr.Remove();
                    jsUtil.Toastr.Show('i', 'Modal', 'Cargando...');

                    try {

                        let r = await this.lg.getModelLisValid();
                        jsUtil.Toastr.Remove();

                        $(this.el.tbl_MD_Detalle).html(Handlebars.compile($('#HB_MD_tblModelo').html())({ arr: r }));

                        $(`${this.el.tbl_MD_Detalle} table`).DataTable({
                            dom: '<"html5buttons"B>lTfgitp',
                            language: jsUtil.DTable.sp,
                            ordering: true,
                            order: [[1, "asc"]],
                            pageLength: 10,
                            responsive: true,
                            aLengthMenu: [
                                [10, 25, 50, 100, 200, -1],
                                [10, 25, 50, 100, 200, "All"]
                            ],
                            buttons: [],
                            destroy: true
                        });

                        this.on.click_MD_model();

                        $(this.el.md_ModeloLis).modal('show');

                    } catch (e) {

                        $(this.el.md_ModeloLis).modal('hide');
                        jsUtil.Toastr.Remove();
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Algo salió mal');

                    }

                },

                md_DetalleAdd: async (i) => {

                    try {

                        $(this.el.md_ModeloLis).modal('hide');

                        jsUtil.Toastr.Remove();
                        jsUtil.Toastr.Show('i', 'Agregando', 'Cargando...');

                        let id = $(i.target).parents('tr').data('id');
                        let r = await this.lg.getModelo({ id });

                        r.TCosto = (r.Operacion.Costo + r.Suministro.Precio).toFixed(2);
                        r.TTiempo = r.Operacion.Tiempo;
                    
                        $(`${this.el.tblDetalle} tbody`).append(Handlebars.compile($('#HB_tblDetalle_tr').html())({ r }));

                        jsUtil.Toastr.Remove();
                        jsUtil.Toastr.Show('s', 'Modelo', 'Agregado');

                        this.on.click_Detalle_del();
                        this.on.change_Detalle();
                        this.fn.tbl_Operar();

                    } catch (e) {
                        $(this.el.md_ModeloLis).modal('hide');
                        jsUtil.Toastr.Remove();
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Algo salió mal');
                    }

                },

                Crear: () => {
                    try {

                        let obj = new Object();

                        obj.Cliente = { Id: $(this.el.s2_Cliente).val() || null };
                        if (obj.Cliente.Id === null) { throw "Seleccione un Cliente"; }
                        obj.FInicio = moment($(this.el.fInicio).val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
                        obj.Precio = parseFloat($(this.el.num_Precio).val()) || 0;
                        if (obj.Precio === 0) { throw "Precio inválido"; }
                        if (obj.Precio < parseFloat($(this.el.num_Costo).val())) { throw "El Precio no puede ser menor al Costo"; }

                        obj.Detalle = new Array();

                        $(`${this.el.tblDetalle} table tbody`).find('tr').toArray().forEach((o, i) => {
                            let Modelo = { Id: $(o).data('id') };
                            let Cantidad = parseInt($(o).find('.iCantidad').val());
                            let FEFin = moment($(o).find('.iFin').text(), 'DD/MM/YYYY').format('YYYY-MM-DD');

                            obj.Detalle.push({ Modelo, Cantidad, FEFin });
                        });

                        this.fn.resCrear(obj);

                    } catch (e) {
                        jsUtil.Toastr.Remove();
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                },

                resCrear: async (obj) => {
                    try {

                        let r = await this.lg.insOrden({ obj });
                        $('#MD_Result .modal-body').html(Handlebars.compile($('#HB_Result').html())({ r }));
                        $('#MD_Result').modal('show');

                    } catch (e) {
                        jsUtil.Toastr.Remove();
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                }

            }

            this.on = {

                click_openAdd: (life = true) => {
                    $(this.el.tblDetalle_add).off('click');
                    if (life) { $(this.el.tblDetalle_add).on('click', () => this.fn.md_ModeloLis()); }
                },

                click_Detalle_del: (life = true) => {
                    $(this.el.tblDetalle).off('click', this.el.tblDetalle_del);
                    if (life) {
                        $(this.el.tblDetalle).on('click', this.el.tblDetalle_del, function () {
                            $(this).parents('tr').remove();
                            app.fn.tbl_Operar();
                        });
                    }
                },

                change_Detalle: (life = true) => {
                    $(this.el.tblDetalle).off('change', '.iCantidad');
                    if (life) {
                        $(this.el.tblDetalle).on('change', '.iCantidad', function () {
                            app.fn.tbl_Operar();
                        });
                    }
                },

                click_MD_model: (life = true) => {
                    $(this.el.tbl_MD_Detalle).off('click', '.btn-SelectModelo');
                    if (life) {
                        $(this.el.tbl_MD_Detalle).on('click', '.btn-SelectModelo', function (e) {
                            app.fn.md_DetalleAdd(e);
                        });
                    }
                },
                
                click_btn_Crear: (life = true) => {
                    $(this.el.btnCrear).off('click');
                    if (life) { $(this.el.btnCrear).on('click', () => { this.fn.Crear(); }); }
                }

            }
        }

        app.init();

    </script>

</asp:Content>

