﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/admin.Master" AutoEventWireup="true" CodeBehind="PedidoDetalleControl.aspx.cs" Inherits="web.app.page.Proyecto.PedidoDetalleControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Paquete  <%= CPedido %> / <%= CPedidoDetalle %> / <%= CPedidoDetalleControl %> • TexCross
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">

    <style>
        #tblProyectoDetalleControlDetalle .dataTables_wrapper {
            padding-bottom: 0 !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Paquete</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="/Proyecto/Listar">Proyectos</a></li>
                <li><a href="/Proyecto/<%= CPedido %>"><%= CPedido %></a></li>
                <li><a href="/Proyecto/<%= CPedido %>/<%= CPedidoDetalle %>"><%= CPedidoDetalle %></a></li>
                <li class="active"><strong><%= CPedidoDetalleControl %></strong></li>
            </ol>
        </div>
    </div>

    <div class="row  border-bottom white-bg dashboard-header pb-10">

        <div class="col-xs-12">

            <div class="row">

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Cantidad</h5>
                        <span class="dBlock"><%= OCPedidoDetalleControl.Cantidad %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Costo inventario</h5>
                        <span class="dBlock"><%= OCPedidoDetalleControl.Inventario %></span>
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Costo operacion</h5>
                        <span class="dBlock"><%= OCPedidoDetalleControl.Operacion %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Costo total</h5>
                        <span class="dBlock"><%= OCPedidoDetalleControl.Total %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Fecha fin</h5>
                        <span class="dBlock"><%= OCPedidoDetalleControl.FFin %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Completado</h5>
                        <span class="dBlock"><%= OCPedidoDetalleControl.Porcentaje %></span>
                    </div>
                </div>

            </div>

        </div>

    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listado de paquetes</h5>
                    <div class="ibox-tools">
                        <a class="tbl_btnRefresh" href="javascript:;">
                            <i class="fa fa-refresh"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div id="tblProyectoDetalleControlDetalle" class="col-xs-12"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 mb-5">
                                    <span class="font-bold">Leyenda:</span>
                                </div>
                                <div class="col-xs-12 mb-5">
                                    <i class="fa fa-check text-success"></i>
                                    <span>Operación completada</span>
                                </div>
                                <div class="col-xs-12 mb-5">
                                    <button type="button" class="btn-alter btn btn-xs btn-danger"><i class="fa fa-industry"></i></button>
                                    <span>Operación sin realizar</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Modal -->

    <div id="MD_Update" class="modal modal-dialog-center fade" aria-hidden="true" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <!-- HandleBars-->
    <script id="HB_tblProyectoDetalleControlDetalle_Load" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </script>

    <script id="HB_tblProyectoDetalleControlDetalle_Msg" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <span class="font-14">{{msg}}</span>
        </div>
    </script>

    <script id="HB_tblProyectoDetalleControlDetalle" type="text/x-handlebars-template">

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Operación</th>
                    <th>Costo</th>
                    <th>Colaborador</th>
                    <th>Fecha fin</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {{#each arr}}
                <tr data-id="{{Id}}" data-operacion="{{OperacionId}}">
                    <td>{{Operacion}}</td>
                    <td>{{Costo}}</td>
                    <td>{{Persona}}</td>
                    <td>{{FFin}}</td>
                    <td class="tCenter">
                        {{#if Completo}}
                            <i class="fa fa-check text-success"></i>
                        {{else}}
                            <button type="button" class="btn-terminar btn btn-xs btn-danger"><i class="fa fa-industry"></i></button>
                        {{/if}}
                    </td>
                </tr>
                {{/each}}
            </tbody>
        </table>
        
    </script>

    <script id="HB_ModalContent" type="text/x-handlebars-template">

        <div class="row">
            <div class="col-xs-12 mb-10">
                <h4 class="bd-bottom-gray-1 font-bold pb-5 m-0">{{ Operacion }}</h4>
            </div>
        </div>

        <div class="row" data-id="{{Id}}">
            
            <div class="col-xs-12">
                <div class="form-group mb-10">
                    <h5 class="font-bold mt-0 mb-5">Colaborador</h5>
                    <select class="s2_Colaborador form-control input-sm"></select>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 tRight">

                <div class="col-xs-12 bd-top-gray-1 mb-5"></div>

                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn-Guardar btn btn-sm btn-danger">Terminar</button>
            </div>
        </div>

    </script>

    <script type="text/javascript">

        Handlebars.registerHelper('cLog', function (d) {
            console.log(d);
            return d;
        });

        var app = new function () {

            this.idPedidoDetalleControl = <%= OCPedidoDetalleControl.Id  %>;

            this.el = {
                divTbl: '#tblProyectoDetalleControlDetalle',
                tblRefresh: '.tbl_btnRefresh',
                btn_terminar: '.btn-terminar',
                md_up: '#MD_Update',
                md_upContent: '#MD_Update .modal-body',
                s2_Colaborador: '.s2_Colaborador',
                btn_guardar: '.btn-Guardar'
            }

            this.lg = {
                getLis: async () => {
                    try {
                        let r = await jsServer.ARes("PedidoDetalleControlDetalle_Lis", { id: this.idPedidoDetalleControl });
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getItem: async (obj) => {
                    try {
                        let r = await jsServer.ARes("PedidoDetalleControlDetalle_GetByCode", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getPersonas: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Persona_LisByOperacion", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                setTerminar: async (obj) => {
                    try {
                        let r = await jsServer.ARes("PedidoDetalleControlDetalle_Terminar", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                }

            }

            this.init = async () => {
                this.fn.getLis();
                this.on.click_refresh();
            }

            this.fn = {

                getLis: async () => {

                    try {

                        let html = Handlebars.compile($('#HB_tblProyectoDetalleControlDetalle_Load').html());
                        $(this.el.divTbl).html(html());

                        let r = await this.lg.getLis();

                        html = Handlebars.compile($('#HB_tblProyectoDetalleControlDetalle').html());

                        $(this.el.divTbl).html(html({ arr: r }));

                        util.dataTable.init(`${this.el.divTbl} table`);

                    } catch (e) {
                        let html = Handlebars.compile($('#HB_tblProyectoDetalleControlDetalle_Msg').html());
                        $(this.el.divTbl).html(html({ msg: e }));
                    } finally { this.on.click_update(); }

                },

                startModal: async (e) => {

                    try {

                        let id = $(e).parents('tr').data('id');

                        let rItem = await this.lg.getItem({ id });
                        let rPersona = await this.lg.getPersonas({ id: rItem.OperacionId });

                        $(this.el.md_upContent).html(Handlebars.compile($('#HB_ModalContent').html())(rItem));

                        await jsUtil.Select.init(`${this.el.s2_Colaborador}`);
                        await jsUtil.Select.Loading(`${this.el.s2_Colaborador}`);
                        await jsUtil.Select.LoadArr(`${this.el.s2_Colaborador}`, rPersona, 'I', 'N', false);

                        $(this.el.md_up).modal('show');

                        this.on.click_guardar();

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Ocurrio un problema, vuelta a intentar.');
                        $(this.el.md_up).modal('hide');
                    }
                },

                up_Operacion: async () => {

                    try {

                        let persona = $(this.el.s2_Colaborador).val();
                        let operacion = $(this.el.s2_Colaborador).parents('div.row').data('id');

                        let r = await this.lg.setTerminar({id: operacion, persona: persona})

                        jsUtil.Toastr.Show('s', 'Terminado', 'Se actualizó correctamente');

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Ocurrio un problema, vuelta a intentar.');
                    } finally {
                        this.fn.getLis();
                        $(this.el.md_up).modal('hide');
                    }

                }

            }

            this.on = {

                click_refresh: (life = true) => {
                    $(this.el.tblRefresh).off('click');
                    if (life) { $(this.el.tblRefresh).on('click', () => { this.fn.getLis(); }); }
                },

                click_update: (life = true) => {
                    $(this.el.divTbl).off('click', this.el.btn_terminar);
                    if (life) { $(this.el.divTbl).on('click', this.el.btn_terminar, function (e) { app.fn.startModal(this); }); }
                },

                click_guardar: (life = true) => {
                    $(this.el.btn_guardar).off('click');
                    if (life) { $(this.el.btn_guardar).on('click', () => { this.fn.up_Operacion(); }); }
                }

            }

        }

        var util = new function () {

            this.dataTable = {

                init: async (item) => {

                    $(item).DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        language: jsUtil.DTable.sp,
                        ordering: false,
                        pageLength: 25,
                        responsive: false,
                        aLengthMenu: [
                            [10, 25, 50, 100, 200, -1],
                            [10, 25, 50, 100, 200, "All"]
                        ],
                        buttons: [
                            { extend: 'copy' }, { extend: 'csv' }, { extend: 'excel', title: 'ExampleFile' }, { extend: 'pdf', title: 'ExampleFile' },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ],
                        destroy: true
                    });
                }
            }
        }

        app.init();

    </script>

</asp:Content>
