﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/admin.Master" AutoEventWireup="true" CodeBehind="Pedido.aspx.cs" Inherits="web.app.page.Proyecto.Pedido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Proyecto <%= CPedido  %> • TexCross
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">

    <style>
        #tblProyectoDetalle .dataTables_wrapper {
            padding-bottom: 0 !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Proyecto</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="/Proyecto/Listar">Proyectos</a></li>
                <li class="active"><strong><%= CPedido %></strong></li>
            </ol>
        </div>
    </div>

    <div class="row  border-bottom white-bg dashboard-header pb-10">

        <div class="col-xs-12">

            <div class="row">

                <div class="col-xs-12 col-md-6">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Cliente</h5>
                        <span class="dBlock"><%= OCPedido.Cliente %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Costo</h5>
                        <span class="dBlock"><%= OCPedido.Costo %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Precio</h5>
                        <span class="dBlock"><%= OCPedido.Precio %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Completado</h5>
                        <span class="dBlock"><%= OCPedido.Porcentaje %></span>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-md-3">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Fecha de inicio</h5>
                        <span class=""><%= OCPedido.FInicio %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-3">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Fecha de fin estimado </h5>
                        <span class=""><%= OCPedido.FEFin %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-3">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Fecha de fin real</h5>
                        <span class=""><%= OCPedido.FFin is null ? "---" : OCPedido.FFin %></span>
                    </div>
                </div>

                <div class="col-xs-12 col-md-3">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Fecha de creación</h5>
                        <span class=""><%= OCPedido.FCreacion %></span>
                    </div>
                </div>

            </div>

        </div>

    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">
    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listado de modelos</h5>
                    <div class="ibox-tools">
                        <a class="tbl_btnRefresh" href="javascript:;">
                            <i class="fa fa-refresh"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div id="tblProyectoDetalle" class="col-xs-12"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 mb-5">
                                    <span class="font-bold">Leyenda:</span>
                                </div>
                                <div class="col-xs-12 mb-5">
                                    <button type="button" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                    <span>Modelo completado</span>
                                </div>
                                <div class="col-xs-12 mb-5">
                                    <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-eye"></i></button>
                                    <span>Modelo en progreso</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <!-- HandleBars-->
    <script id="HB_tblProyectoDetalle_Load" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </script>

    <script id="HB_tblProyectoDetalle_Msg" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <span class="font-14">{{msg}}</span>
        </div>
    </script>

    <script id="HB_tblProyectoDetalle" type="text/x-handlebars-template">

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="tCenter bg-white" colspan="2"></th>
                    <th class="tCenter bg-white" colspan="3">Modelo</th>
                    <th class="tCenter bg-white" colspan="2">Fecha</th>
                    <th class="tCenter bg-white" colspan="2">Cantidad</th>
                    <th class="tCenter bg-white" colspan="3">Costo</th>
                    <th class="tCenter bg-white" colspan="1"></th>
                    <th class="tCenter bg-white" colspan="1" rowspan="2"></th>
                </tr>
                <tr>
                    <th>Cod</th>
                    <th>Tipo</th>
                    <th>Nombre</th>
                    <th>Talla</th>
                    <th>Color</th>
                    <th>FEFin</th>
                    <th>FFin</th>
                    <th>Paquete</th>
                    <th>Cantidad</th>
                    <th>Operacion</th>
                    <th>Inventario</th>
                    <th>Total</th>
                    <th>Porcentaje</th>
                </tr>
            </thead>
            <tbody>
                {{#each arr}}
                <tr data-id="{{Id}}">
                    <td>{{Cod}}</td>
                    <td>{{Tipo}}</td>
                    <td>{{Nombre}}</td>
                    <td>{{Talla}}</td>
                    <td>{{Color}}</td>
                    <td>{{FEFin}}</td>
                    <td>{{FFin}}</td>
                    <td>{{Paquete}}</td>
                    <td>{{Cantidad}}</td>
                    <td>{{Operacion}}</td>
                    <td>{{Inventario}}</td>
                    <td>{{Total}}</td>
                    <td>{{Porcentaje}}</td>
                    <td class="tCenter">
                        <a href="/Proyecto/<%= CPedido %>/{{Cod}}" class="btn btn-xs btn-{{#if Completo}}success{{else}}danger{{/if}}"><i class="fa fa-eye"></i></a>
                    </td>
                </tr>
                {{/each}}
            </tbody>
        </table>

    </script>

    <script type="text/javascript">

        var app = new function () {

            this.idPedido = <%= OCPedido.Id  %>;

            this.el = {
                divTbl: '#tblProyectoDetalle',
                tblRefresh: '.tbl_btnRefresh'
            }

            this.lg = {
                getLis: async () => {
                    try {
                        let r = await jsServer.ARes("PedidoDetalle_Lis", { id: this.idPedido });
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                }
            }

            this.init = async () => {
                this.fn.getLis();
                this.on.click_refresh();
            }

            this.fn = {

                getLis: async () => {

                    try {

                        let html = Handlebars.compile($('#HB_tblProyectoDetalle_Load').html());
                        $(this.el.divTbl).html(html());

                        let r = await this.lg.getLis();

                        html = Handlebars.compile($('#HB_tblProyectoDetalle').html());

                        $(this.el.divTbl).html(html({ arr: r }));

                        util.dataTable.init(`${this.el.divTbl} table`);

                    } catch (e) {
                        let html = Handlebars.compile($('#HB_tblProyectoDetalle_Msg').html());
                        $(this.el.divTbl).html(html({ msg: e }));
                    }

                }
            }

            this.on = {

                click_refresh: (life = true) => {
                    $(this.el.tblRefresh).off('click');
                    if (life) { $(this.el.tblRefresh).on('click', () => { this.fn.getLis(); }); }
                }

            }

        }

        var util = new function () {

            this.dataTable = {

                init: async (item) => {

                    $(item).DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        language: jsUtil.DTable.sp,
                        ordering: false,
                        pageLength: 25,
                        responsive: false,
                        aLengthMenu: [
                            [10, 25, 50, 100, 200, -1],
                            [10, 25, 50, 100, 200, "All"]
                        ],
                        buttons: [
                            { extend: 'copy' }, { extend: 'csv' }, { extend: 'excel', title: 'ExampleFile' }, { extend: 'pdf', title: 'ExampleFile' },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ],
                        destroy: true
                    });
                }
            }
        }

        app.init();

    </script>

</asp:Content>
