﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/admin.Master" AutoEventWireup="true" CodeBehind="Suministro.aspx.cs" Inherits="web.app.page.Modelo.Suministro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Suministro - Modelo - Diseño • TexCross
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Suministro</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="javascript:;">Diseño</a></li>
                <li><a href="/Diseno/Modelo">Modelo</a></li>
                <li class="active"><strong>Suministro</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Modelo</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5">Tipo</h5>
                                        <h5 class="md_tipo mt-0 mb-5">Tipo</h5>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5">Nombre</h5>
                                        <h5 class="md_nombre mt-0 mb-5">Nombre</h5>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5">Talla</h5>
                                        <h5 class="md_talla mt-0 mb-5">Talla</h5>
                                    </div>
                                </div>
                                
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5">Color</h5>
                                        <h5 class="md_color mt-0 mb-5">Color</h5>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5">Creador</h5>
                                        <h5 class="md_creador mt-0 mb-5">Creador</h5>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    
    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Agregar</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-7">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5">Tipo</h5>
                                        <select class="addSuministro_tipo form-control"></select>
                                    </div>
                                </div>

                                <div class="col-xs-8 col-md-3">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5 tRight">Cantidad</h5>
                                        <input type="number" class="addSuministro_cantidad tRight form-control" value="0" />
                                    </div>
                                </div>

                                <div class="col-xs-4 col-md-2">
                                    <div class="form-group mb-10">
                                        <h5 class="font-bold mt-0 mb-5">Unidad</h5>
                                        <span class="addSuministro_unidad form-control"></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-offset-9 col-md-3 col-lg-offset-10 col-lg-2">
                            <button type="button" class="addSuministro_button btn btn-block btn-sm btn-info">Agregar</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listado</h5>
                    <div class="ibox-tools">
                        <a class="tbl_btnRefresh" href="javascript:;">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div id="tblModeloSuministro" class="col-xs-12"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <!-- HandleBars-->
    <script id="HB_tblModeloSuministro_Load" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </script>

    <script id="HB_tblModeloSuministro_Msg" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <span class="font-14">{{msg}}</span>
        </div>
    </script>

    <script id="HB_tblModeloSuministro" type="text/x-handlebars-template">

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Nombre</th>
                    <th>Color</th>
                    <th class="tRight">Costo/Unidad</th>
                    <th class="tRight">Cantidad</th>
                    <th class="tRight">Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {{#each arr}}
                <tr data-id="{{Id}}">
                    <td>{{Suministro.Tipo.Nombre}}</td>
                    <td>{{Suministro.Nombre}}</td>
                    <td>{{Suministro.Color.Nombre}}</td>
                    <td class="tRight">{{Suministro.Precio}}/{{Suministro.Unidad.Simbolo}}</td>
                    <td class="tRight">
                        <input class="trCantidad tRight" style="width: 60px" type="number" value="{{Cantidad}}" />
                        {{Suministro.Unidad.Simbolo}}
                    </td>
                    <td class="tRight">{{Total}}</td>
                    <td class="tCenter">
                        <button type="button" class="btn_update btn btn-xs btn-info"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn_delete btn btn-xs btn-warning"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                {{/each}}
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5" class="tRight font-bold">Total</td>
                    <td class="font-bold tRight">{{r.Total}}</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>

    </script>

    <script>

        var app = new function () {

            this.id = <%= Id %>

                this.el = {
                    addTipo: '.addSuministro_tipo',
                    addCantidad: '.addSuministro_cantidad',
                    addUnidad: '.addSuministro_unidad',
                    addButton: '.addSuministro_button',
                    divTbl: '#tblModeloSuministro',
                    tblRefresh: '.tbl_btnRefresh',
                    tblAdd: '.tbl_btnAdd',
                    md_InsModeloSuministro: '#MD_InsModeloSuministro',
                    md_UpdModeloSuministro: '#MD_UpdModeloSuministro',
                }

            this.lg = {

                getModelo: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Modelo_Get", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getSuministroLis: async () => {
                    try {
                        let r = await jsServer.ARes("Suministro_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getSuministroGet: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Suministro_Get", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getLis: async () => {
                    try {
                        let r = await jsServer.ARes("ModeloSuministro_Lis", { id: app.id });
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                getModeloSuministro: async (obj) => {
                    try {
                        let r = await jsServer.ARes("ModeloSuministro_Get", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                addModeloSuministro: async (obj) => {
                    try {
                        let r = await jsServer.ARes("ModeloSuministro_Ins", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                updModeloSuministro: async (obj) => {
                    try {
                        let r = await jsServer.ARes("ModeloSuministro_Upd", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                delModeloSuministro: async (obj) => {
                    try {
                        let r = await jsServer.ARes("ModeloSuministro_Del", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
            }

            this.init = async () => {
                await this.fn.getModelo();
                await this.fn.getSuministroLis();
                await this.on.change_addTipo();
                await this.on.click_addSuministro();
                await this.fn.getLis();
                await this.on.click_refresh();
            }

            this.fn = {

                getModelo: async () => {
                    try {

                        let r = await this.lg.getModelo({ id: this.id });
                        $('.md_tipo').text(r.Tipo.Nombre);
                        $('.md_nombre').text(r.Nombre);
                        $('.md_talla').text(`${r.Talla.Tipo.Nombre} - ${r.Talla.Nombre}`);
                        $('.md_color').text(r.Color.Nombre);
                        $('.md_creador').text(r.Creador.Nombre);

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                },

                getSuministroLis: async () => {
                    try {

                        await jsUtil.Select.init(this.el.addTipo);
                        await jsUtil.Select.Loading(this.el.addTipo);

                        let r = await this.lg.getSuministroLis();
                        $(r).each((i, o) => { o.Nombre = `${o.Tipo.Nombre} - ${o.Nombre} - ${o.Color.Nombre}` });
                        await jsUtil.Select.LoadArr(this.el.addTipo, r, 'Id', 'Nombre', false);
                        await jsUtil.Select.Refresh(this.el.addTipo);

                        this.fn.getSuministroGet($(this.el.addTipo).val());

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                },

                getSuministroGet: async () => {
                    try {

                        let id = $(this.el.addTipo).val();

                        let r = await this.lg.getSuministroGet({id});
                        $(this.el.addUnidad).text(r.Unidad.Simbolo);

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                },

                getLis: async () => {

                    try {

                        let html = Handlebars.compile($('#HB_tblModeloSuministro_Load').html());
                        $(this.el.divTbl).html(html());

                        let r = await this.lg.getLis();

                        $(r).each((i, o) => {
                            o.Total = parseFloat((o.Cantidad * o.Suministro.Precio).toFixed(2));
                        });

                        let rResumen = new Object();
                        rResumen.Total = parseFloat(r.reduce((a, i) => { return a + i.Total }, 0).toFixed(2));

                        html = Handlebars.compile($('#HB_tblModeloSuministro').html());
                        $(this.el.divTbl).html(html({ arr: r, r: rResumen }));

                        util.dataTable.init(`${this.el.divTbl} table`);

                        this.on.click_delete();
                        this.on.click_update();

                    } catch (e) {
                        let html = Handlebars.compile($('#HB_tblModeloSuministro_Msg').html());
                        $(this.el.divTbl).html(html({ msg: e }));
                    }

                },

                getDel: async (e) => {

                    try {

                        jsUtil.Toastr.Remove();

                        let id = $(e).parents('tr').data('id');

                        let r = await this.lg.delModeloSuministro({ id });

                        this.fn.getLis();

                        jsUtil.Toastr.Show('s', 'Eliminado', 'con éxito')

                    } catch (e) {
                        this.fn.getLis();
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }

                },

                addItem: async () => {
                    try {

                        jsUtil.Toastr.Remove();

                        let obj = new Object();
                        obj.Cantidad = $(this.el.addCantidad).val() || null;
                        if (obj.Cantidad === null) { throw 'Debe ingresar una cantidad'; }
                        obj.Modelo = { Id: app.id };
                        if (obj.Modelo.Id === null) { throw 'Actualicé la página'; }
                        obj.Suministro = { Id: $(this.el.addTipo).val() };
                        if (obj.Suministro.Id === null) { throw 'Debe seleccionar un suministro'; }

                        let r = await this.lg.addModeloSuministro({ obj });

                        jsUtil.Toastr.Show('s', 'Agregado', 'con éxito')

                        this.fn.getLis();

                    } catch (e) {
                        this.fn.getLis();
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                },

                updItem: async (e) => {

                    try {

                        jsUtil.Toastr.Remove();

                        let tr = $(e).parents('tr');

                        let obj = new Object();
                        obj.Id = $(tr).data('id') || null;
                        if (obj.Id === null) { throw 'Actualicé la página'; }
                        obj.Cantidad = $(tr).find('.trCantidad').val() || null;
                        if (obj.Cantidad === null) { throw 'Debe ingresar una cantidad'; }
                        
                        let r = await this.lg.updModeloSuministro({ obj });

                        jsUtil.Toastr.Show('s', 'Editado', 'con éxito')

                        this.fn.getLis();

                    } catch (e) {
                        this.fn.getLis();
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }

                }

            }

            this.on = {
                click_refresh: (life = true) => {
                    $(this.el.tblRefresh).off('click');
                    if (life) { $(this.el.tblRefresh).on('click', () => { this.fn.getLis(); }); }
                },
                click_update: (life = true) => {
                    $(this.el.divTbl).off('click', '.btn_update');
                    if (life) {
                        $(this.el.divTbl).on('click', '.btn_update', function (e) { app.fn.updItem(e.target); });
                    }
                },
                click_delete: (life = true) => {
                    $(this.el.divTbl).off('click', '.btn_delete');
                    if (life) {
                        $(this.el.divTbl).on('click', '.btn_delete', function (e) { app.fn.getDel(e.target); });
                    }
                },
                change_addTipo: (life = true) => {
                    if (life) { $(this.el.addTipo).on('change', () => { this.fn.getSuministroGet() }); }
                },
                click_addSuministro: (life = true) => {
                    $(this.el.addButton).off('click');
                    if (life) {
                        $(this.el.addButton).on('click', () => { this.fn.addItem() });
                    }
                }
            }

        }

        var util = new function () {

            this.dataTable = {

                init: async (item) => {

                    $(item).DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        language: jsUtil.DTable.sp,
                        ordering: true,
                        order: [[1, "asc"]],
                        pageLength: 10,
                        responsive: true,
                        aLengthMenu: [
                            [10, 25, 50, 100, 200, -1],
                            [10, 25, 50, 100, 200, "All"]
                        ],
                        buttons: [
                            { extend: 'copy' }, { extend: 'csv' }, { extend: 'excel', title: 'ExampleFile' }, { extend: 'pdf', title: 'ExampleFile' },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ],
                        destroy: true
                    });
                }
            }
        }

        app.init();

    </script>
</asp:Content>
