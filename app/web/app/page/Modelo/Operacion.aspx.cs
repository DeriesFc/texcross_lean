﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.app.page.Modelo
{
    public partial class Operacion : System.Web.UI.Page
    {

        public string Id { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            Id = Page.RouteData.Values["id"] as string ?? "0";

        }
    }
}