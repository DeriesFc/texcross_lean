﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/admin.Master" AutoEventWireup="true" CodeBehind="Modelo.aspx.cs" Inherits="web.app.page.Modelo.Modelo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Modelo - Diseño • TexCross
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Modelo</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="javascript:;">Diseño</a></li>
                <li class="active"><strong>Modelo</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listado</h5>
                    <div class="ibox-tools">
                        <a class="tbl_btnRefresh" href="javascript:;">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="tbl_btnAdd" href="javascript:;">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div id="tblModelo" class="col-xs-12"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Region Modal -->
    <div id="MD_InsModelo" class="modal modal-dialog-center fade" aria-hidden="true" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body pt-20 pb-20">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="bd-bottom-gray-1 font-bold pb-5 mt-0 mb-10">Agregar</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Tipo</h5>
                                <select class="MDModelo_tipo form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Nombre</h5>
                                <input type="text" placeholder="Nombre" class="MDModelo_nombre form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Talla</h5>
                                <select class="MDModelo_talla form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Color</h5>
                                <select class="MDModelo_color form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Creador</h5>
                                <select class="MDModelo_creador form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-5 mb-10 ccc" />
                    <div class="row">
                        <div class="col-xs-12 tRight">
                            <button type="button" class="MDModelo_guardar btn btn-sm btn-primary">Guardar</button>
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="MD_UpdModelo" class="modal modal-dialog-center fade" aria-hidden="true" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body pt-20 pb-20">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="bd-bottom-gray-1 font-bold pb-5 mt-0 mb-10">Editar</h4>
                        </div>
                    </div>
                    <input type="hidden" class="MDModelo_id form-control" />
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Tipo</h5>
                                <select class="MDModelo_tipo form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Nombre</h5>
                                <input type="text" placeholder="Nombre" class="MDModelo_nombre form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Talla</h5>
                                <select class="MDModelo_talla form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Color</h5>
                                <select class="MDModelo_color form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Creador</h5>
                                <select class="MDModelo_creador form-control" name="state"></select>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-5 mb-10 ccc" />
                    <div class="row">
                        <div class="col-xs-12 tRight">
                            <button type="button" class="MDModelo_update btn btn-sm btn-primary">Actualizar</button>
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- endRegion Modal -->

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <!-- HandleBars-->
    <script id="HB_tblModelo_Load" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </script>

    <script id="HB_tblModelo_Msg" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <span class="font-14">{{msg}}</span>
        </div>
    </script>

    <script id="HB_tblModelo" type="text/x-handlebars-template">

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="tCenter bg-white" colspan="5">Modelo</th>
                    <th class="tCenter bg-white" colspan="3">Operacion(es)</th>
                    <th class="tCenter bg-white" colspan="2">Suministro(s)</th>
                    <th class="tCenter bg-white"></th>
                </tr>
                <tr>
                    <th>Tipo</th>
                    <th>Nombre</th>
                    <th>Talla</th>
                    <th>Color</th>
                    <th>Creador</th>
                    <th class="tRight">Cant.</th>
                    <th class="tRight">Costo</th>
                    <th class="tRight">Tiempo</th>
                    <th class="tRight">Cant.</th>
                    <th class="tRight">Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {{#each arr}}
                <tr data-id="{{Id}}">
                    <td>{{Tipo.Nombre}}</td>
                    <td>{{Nombre}}</td>
                    <td>{{Talla.Tipo.Nombre}} - {{Talla.Nombre}}</td>
                    <td>{{Color.Nombre}}</td>
                    <td>{{Creador.Nombre}}</td>
                    <td class="tRight">{{Operacion.Cantidad}}</td>
                    <td class="tRight">{{Operacion.Costo}}</td>
                    <td class="tRight">{{Operacion.Tiempo}}</td>
                    <td class="tRight">{{Suministro.Cantidad}}</td>
                    <td class="tRight">{{Suministro.Precio}}</td>
                    <td class="tCenter">
                        <button type="button" class="tbl_upd btn btn-xs btn-info"><i class="fa fa-pencil"></i></button>
                        <a href="/Diseno/Modelo/Operacion/{{Id}}" class="btn btn-xs btn-success"><i class="fa fa-wrench"></i></a>
                        <a href="/Diseno/Modelo/Suministro/{{Id}}" class="btn btn-xs btn-default"><i class="fa fa-cubes"></i></a>
                        <button type="button" class="tbl_estado btn btn-xs {{#if Estado}}btn-warning{{else}}btn-danger{{/if}}"><i class="fa {{#if Estado}}fa-eye{{else}}fa-eye-slash{{/if}}"></i></button>
                    </td>
                </tr>
                {{/each}}
            </tbody>
        </table>

    </script>

    <script type="text/javascript">

        var app = new function () {

            this.el = {
                divTbl: '#tblModelo',
                tblRefresh: '.tbl_btnRefresh',
                tblAdd: '.tbl_btnAdd',
                md_InsModelo: '#MD_InsModelo',
                md_UpdModelo: '#MD_UpdModelo',
            }

            this.lg = {

                getTipo: async () => {
                    try {
                        let r = await jsServer.ARes("ModeloTipo_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getTalla: async () => {
                    try {
                        let r = await jsServer.ARes("Talla_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getColor: async () => {
                    try {
                        let r = await jsServer.ARes("Color_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getCreador: async () => {
                    try {
                        let r = await jsServer.ARes("Cliente_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },

                getLis: async () => {
                    try {
                        let r = await jsServer.ARes("Modelo_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                getModelo: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Modelo_Get", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                addModelo: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Modelo_Ins", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                updModelo: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Modelo_Upd", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
            }

            this.init = async () => {

                this.fn.getLis();
                this.on.click_openAdd();
                this.on.click_refresh();

            }

            this.fn = {

                getLis: async () => {

                    try {

                        let html = Handlebars.compile($('#HB_tblModelo_Load').html());
                        $(this.el.divTbl).html(html());

                        let r = await this.lg.getLis();

                        html = Handlebars.compile($('#HB_tblModelo').html());

                        $(this.el.divTbl).html(html({ arr: r }));

                        util.dataTable.init(`${this.el.divTbl} table`);

                        this.on.click_openUpd();

                    } catch (e) {
                        let html = Handlebars.compile($('#HB_tblModelo_Msg').html());
                        $(this.el.divTbl).html(html({ msg: e }));
                    }

                },

                open_add: async () => {
                    try {

                        this.on.click_saveAdd(false);

                        await jsUtil.Select.init(`${this.el.md_InsModelo} .MDModelo_tipo`);
                        await jsUtil.Select.Loading(`${this.el.md_InsModelo} .MDModelo_tipo`);

                        await jsUtil.Select.init(`${this.el.md_InsModelo} .MDModelo_talla`);
                        await jsUtil.Select.Loading(`${this.el.md_InsModelo} .MDModelo_talla`);

                        await jsUtil.Select.init(`${this.el.md_InsModelo} .MDModelo_color`);
                        await jsUtil.Select.Loading(`${this.el.md_InsModelo} .MDModelo_color`);

                        await jsUtil.Select.init(`${this.el.md_InsModelo} .MDModelo_creador`);
                        await jsUtil.Select.Loading(`${this.el.md_InsModelo} .MDModelo_creador`);

                        $(this.el.md_InsModelo).modal('show');

                        let rTipo = await this.lg.getTipo();
                        await jsUtil.Select.LoadArr(`${this.el.md_InsModelo} .MDModelo_tipo`, rTipo, 'Id', 'Nombre', false);

                        $(`${this.el.md_InsModelo} .MDModelo_nombre`).val('');
                        
                        let rTalla = await this.lg.getTalla();
                        $(rTalla).each(function (i, obj) {
                            obj.Nombre = `${obj.Tipo.Nombre} - ${obj.Nombre}`;
                        });
                        await jsUtil.Select.LoadArr(`${this.el.md_InsModelo} .MDModelo_talla`, rTalla, 'Id', 'Nombre', false);

                        let rColor = await this.lg.getColor();
                        await jsUtil.Select.LoadArr(`${this.el.md_InsModelo} .MDModelo_color`, rColor, 'Id', 'Nombre', false);

                        let rCreador = await this.lg.getCreador();
                        await jsUtil.Select.LoadArr(`${this.el.md_InsModelo} .MDModelo_creador`, rCreador, 'Id', 'Nombre', false);

                        this.on.click_saveAdd();


                    } catch (e) {
                        $(this.el.md_InsModelo).modal('hide');
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Algo salió mal');
                    }
                },

                save_add: async () => {
                    try {

                        jsUtil.Toastr.Remove();

                        let obj = new Object();

                        obj.Tipo = { Id: $(`${this.el.md_InsModelo} .MDModelo_tipo`).val() || null };
                        if (obj.Tipo.Id === null) { throw 'Debe seleccionar un tipo'; }

                        obj.Nombre = $(`${this.el.md_InsModelo} .MDModelo_nombre`).val() || null;
                        if (obj.Nombre === null) { throw 'Debe ingresar un nombre'; }

                        obj.Talla = { Id: $(`${this.el.md_InsModelo} .MDModelo_talla`).val() || null };
                        if (obj.Talla.Id === null) { throw 'Debe seleccionar un tipo'; }

                        obj.Color = { Id: $(`${this.el.md_InsModelo} .MDModelo_color`).val() || null };
                        if (obj.Color.Id === null) { throw 'Debe seleccionar un tipo'; }

                        obj.Creador = { Id: $(`${this.el.md_InsModelo} .MDModelo_creador`).val() || null };
                        if (obj.Creador.Id === null) { throw 'Debe seleccionar un tipo'; }

                        let r = await this.lg.addModelo({ obj });

                        $('#MD_InsModelo').modal('hide')
                        jsUtil.Toastr.Show('s', 'Agregado', r.Nombre);
                        this.fn.getLis();

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                },

                open_upd: async (item) => {
                    try {

                        this.on.click_saveUpd(false);

                        await jsUtil.Select.init(`${this.el.md_UpdModelo} .MDModelo_tipo`);
                        await jsUtil.Select.Loading(`${this.el.md_UpdModelo} .MDModelo_tipo`);

                        await jsUtil.Select.init(`${this.el.md_UpdModelo} .MDModelo_talla`);
                        await jsUtil.Select.Loading(`${this.el.md_UpdModelo} .MDModelo_talla`);

                        await jsUtil.Select.init(`${this.el.md_UpdModelo} .MDModelo_color`);
                        await jsUtil.Select.Loading(`${this.el.md_UpdModelo} .MDModelo_color`);

                        await jsUtil.Select.init(`${this.el.md_UpdModelo} .MDModelo_creador`);
                        await jsUtil.Select.Loading(`${this.el.md_UpdModelo} .MDModelo_creador`);

                        $(this.el.md_UpdModelo).modal('show');

                        let id = $(item).parents('tr').data('id');
                        let r = await this.lg.getModelo({ id });

                        $(`${this.el.md_UpdModelo} .MDModelo_id`).val(r.Id);

                        let rTipo = await this.lg.getTipo();
                        await jsUtil.Select.LoadArr(`${this.el.md_UpdModelo} .MDModelo_tipo`, rTipo, 'Id', 'Nombre', false);
                        await jsUtil.Select.SetVal(`${this.el.md_UpdModelo} .MDModelo_tipo`, r.Tipo.Id);
                        await jsUtil.Select.Refresh(`${this.el.md_UpdModelo} .MDModelo_tipo`);

                        $(`${this.el.md_UpdModelo} .MDModelo_nombre`).val(r.Nombre);
                        
                        let rTalla = await this.lg.getTalla();
                        $(rTalla).each(function (i, obj) {
                            obj.Nombre = `${obj.Tipo.Nombre} - ${obj.Nombre}`;
                        });
                        await jsUtil.Select.LoadArr(`${this.el.md_UpdModelo} .MDModelo_talla`, rTalla, 'Id', 'Nombre', false);
                        await jsUtil.Select.SetVal(`${this.el.md_UpdModelo} .MDModelo_talla`, r.Talla.Id);
                        await jsUtil.Select.Refresh(`${this.el.md_UpdModelo} .MDModelo_talla`);

                        let rColor = await this.lg.getColor();
                        await jsUtil.Select.LoadArr(`${this.el.md_UpdModelo} .MDModelo_color`, rColor, 'Id', 'Nombre', false);
                        await jsUtil.Select.SetVal(`${this.el.md_UpdModelo} .MDModelo_color`, r.Color.Id);
                        await jsUtil.Select.Refresh(`${this.el.md_UpdModelo} .MDModelo_color`);

                        let rCreador = await this.lg.getCreador();
                        await jsUtil.Select.LoadArr(`${this.el.md_UpdModelo} .MDModelo_creador`, rCreador, 'Id', 'Nombre', false);
                        await jsUtil.Select.SetVal(`${this.el.md_UpdModelo} .MDModelo_creador`, r.Creador.Id);
                        await jsUtil.Select.Refresh(`${this.el.md_UpdModelo} .MDModelo_creador`);

                        this.on.click_saveUpd();

                    } catch (e) {
                        $(this.el.md_UpdModelo).modal('hide');
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Algo salió mal');
                    }
                },

                save_upd: async () => {
                    try {

                        jsUtil.Toastr.Remove();

                        let obj = new Object();

                        obj.Id = $(`${this.el.md_UpdModelo} .MDModelo_id`).val() || null;
                        if (obj.Id === null) { throw 'Vuelva a intentar'; }

                        obj.Tipo = { Id: $(`${this.el.md_UpdModelo} .MDModelo_tipo`).val() || null };
                        if (obj.Tipo.Id === null) { throw 'Debe seleccionar un tipo'; }

                        obj.Nombre = $(`${this.el.md_UpdModelo} .MDModelo_nombre`).val() || null;
                        if (obj.Nombre === null) { throw 'Debe ingresar un nombre'; }

                        obj.Talla = { Id: $(`${this.el.md_UpdModelo} .MDModelo_talla`).val() || null };
                        if (obj.Talla.Id === null) { throw 'Debe seleccionar un talla;' }

                        obj.Color = { Id: $(`${this.el.md_UpdModelo} .MDModelo_color`).val() || null };
                        if (obj.Color.Id === null) { throw 'Debe seleccionar un color'; }

                        obj.Creador = { Id: $(`${this.el.md_UpdModelo} .MDModelo_creador`).val() || null };
                        if (obj.Creador.Id === null) { throw 'Debe seleccionar un creador'; }


                        let r = await this.lg.updModelo({ obj });

                        $(this.el.md_UpdModelo).modal('hide')
                        jsUtil.Toastr.Show('s', 'Actualizado', r.Nombre);
                        this.fn.getLis();

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                }

            }

            this.on = {
                click_refresh: (life = true) => {
                    $(this.el.tblRefresh).off('click');
                    if (life) { $(this.el.tblRefresh).on('click', () => { this.fn.getLis(); }); }
                },
                click_openAdd: (life = true) => {
                    $(this.el.tblAdd).off('click');
                    if (life) { $(this.el.tblAdd).on('click', () => this.fn.open_add()); }
                },
                click_saveAdd: (life = true) => {
                    $(`${this.el.md_InsModelo} .MDModelo_guardar`).off('click');
                    $(`${this.el.md_InsModelo} .MDModelo_guardar`).prop('disabled', true);
                    if (life) {
                        $(`${this.el.md_InsModelo} .MDModelo_guardar`).on('click', () => this.fn.save_add());
                        $(`${this.el.md_InsModelo} .MDModelo_guardar`).prop('disabled', false);
                    }
                },
                click_openUpd: (life = true) => {
                    $(`${this.el.divTbl}`).off('click', '.tbl_upd');
                    if (life) { $(`${this.el.divTbl}`).on('click', '.tbl_upd', function (e) { app.fn.open_upd(e.target) }); }
                },
                click_saveUpd: (life = true) => {
                    $(`${this.el.md_UpdModelo} .MDModelo_update`).off('click');
                    $(`${this.el.md_UpdModelo} .MDModelo_update`).prop('disabled', true);
                    if (life) {
                        $(`${this.el.md_UpdModelo} .MDModelo_update`).on('click', () => this.fn.save_upd());
                        $(`${this.el.md_UpdModelo} .MDModelo_update`).prop('disabled', false);
                    }
                },
            }

        }

        var util = new function () {

            this.dataTable = {

                init: async (item) => {

                    $(item).DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        language: jsUtil.DTable.sp,
                        ordering: true,
                        order: [[1, "asc"]],
                        pageLength: 10,
                        responsive: true,
                        aLengthMenu: [
                            [10, 25, 50, 100, 200, -1],
                            [10, 25, 50, 100, 200, "All"]
                        ],
                        buttons: [
                            { extend: 'copy' }, { extend: 'csv' }, { extend: 'excel', title: 'ExampleFile' }, { extend: 'pdf', title: 'ExampleFile' },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ],
                        destroy: true
                    });
                }
            }
        }

        app.init();

    </script>

</asp:Content>
