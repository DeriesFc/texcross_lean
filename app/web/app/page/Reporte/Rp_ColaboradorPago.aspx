﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/admin.Master" AutoEventWireup="true" CodeBehind="Rp_ColaboradorPago.aspx.cs" Inherits="web.app.page.Reporte.Rp_ColaboradorPago" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Reporte General • TexCross
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Pago a colaboradores</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="javascript:;">Reporte</a></li>
                <li class="active"><strong>General</strong></li>
            </ol>
        </div>
    </div>

    <div class="row  border-bottom white-bg dashboard-header pb-10">

        <div class="col-xs-12">

            <div class="row">

                <div class="col-xs-12 col-md-5">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Inicio de rango</h5>
                        <input id="cbo_DateStart" placeholder="dd/mm/yyyy" type="text" class="form-control s2-in-dark border-grayDark"/>
                    </div>
                </div>

                <div class="col-xs-12 col-md-5">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5">Fin de rango</h5>
                        <input id="cbo_DateEnd" placeholder="dd/mm/yyyy" type="text" class="form-control s2-in-dark border-grayDark"/>
                    </div>
                </div>

                <div class="col-xs-12 col-md-2">
                    <div class="form-group mb-10">
                        <h5 class="font-bold mt-0 mb-5" style="color:white">text</h5>
                        <button type="button" id="btn_Buscar" class="btn btn-md btn-block btn-success">Buscar</button>
                    </div>
                </div>

            </div>

        </div>

    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Resultado</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div id="tblProyectoDetalleControl" class="col-xs-12"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <!-- HandleBars-->
    <script id="HB_tblProyectoDetalleControl_Load" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </script>

    <script id="HB_tblProyectoDetalleControl_Msg" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <span class="font-14">{{msg}}</span>
        </div>
    </script>

    <script id="HB_tblProyectoDetalleControl" type="text/x-handlebars-template">

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Area</th>
                    <th>Cantidad</th>
                    <th>Costo</th>
                </tr>
            </thead>
            <tbody>
                {{#each arr}}
                <tr data-id="{{Id}}">
                    <td>{{DNI}}</td>
                    <td>{{Nombre}}</td>
                    <td>{{Area}}</td>
                    <td>{{Cantidad}}</td>
                    <td>{{Costo}}</td>
                </tr>
                {{/each}}
            </tbody>
        </table>

    </script>

    <script>

        var app = new function () {

            this.el = {

                dtStart: '#cbo_DateStart',
                dtEnd: '#cbo_DateEnd',
                btnBuscar: '#btn_Buscar',
                divTbl: '#tblProyectoDetalleControl',
            }

            this.lg = {

                getResult: async (obj) => {

                    try {
                        let r = await jsServer.ARes("Reporte_ColaboradorPago", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }

                }

            }

            this.init = async () => { this.fn.init(); }

            this.fn = {

                init: async () => { this.on.click_Buscar(); },

                getResult: async () => {

                    try {

                        let start = moment($(this.el.dtStart).val(), 'DD/MM/YYYY').toDate();
                        let end = moment($(this.el.dtEnd).val(), 'DD/MM/YYYY').toDate();

                        let html = Handlebars.compile($('#HB_tblProyectoDetalleControl_Load').html());
                        $(this.el.divTbl).html(html());

                        let r = await this.lg.getResult({ start: start, end: end });

                        html = Handlebars.compile($('#HB_tblProyectoDetalleControl').html());

                        $(this.el.divTbl).html(html({ arr: r }));

                        util.dataTable.init(`${this.el.divTbl} table`);



                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Ocurrió un error');
                        console.log(e);
                    }

                }

            }

            this.on = {

                click_Buscar: (life = true) => {
                    $(this.el.btnBuscar).off('click');
                    if (life) { $(this.el.btnBuscar).on('click', () => { this.fn.getResult(); }); }
                }

            }

        }

        var util = new function () {

            this.dataTable = {

                init: async (item) => {

                    $(item).DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        language: jsUtil.DTable.sp,
                        ordering: false,
                        pageLength: 25,
                        responsive: false,
                        aLengthMenu: [
                            [10, 25, 50, 100, 200, -1],
                            [10, 25, 50, 100, 200, "All"]
                        ],
                        buttons: [
                            { extend: 'copy' }, { extend: 'csv' }, { extend: 'excel', title: 'ExampleFile' }, { extend: 'pdf', title: 'ExampleFile' },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ],
                        destroy: true
                    });
                }
            }
        }

        app.init(); 


    </script>

</asp:Content>

