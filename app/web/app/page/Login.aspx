﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="web.app.page.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        
        <!-- Meta -->
        <!-- #include file="../meta.html" -->
        <!-- End Meta -->

        <!-- Title -->
        <title>Acceso • TexCross</title>
        <!-- End Title -->

        <!-- Style -->
        <!-- #include file="../style.html" -->
        <!-- End Style -->
        
    </head>

    <body  class="gray-bg">

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <h1><img style="width:70%;" alt="image" src="/src/img/TexCross.png" /></h1>
                </div>
                <h3>TexCross</h3>
                <p></p>
                <form class="m-t">
                    <div class="form-group">
                        <input id="txtDni" type="text" class="form-control" placeholder="DNI" required="required" value="00000001"/>
                    </div>
                    <div class="form-group">
                        <input id="pwdClave" type="password" class="form-control" placeholder="Clave" required="required" value="123456"/>
                    </div>
                    <button id="btnAcceso" type="button" class="btn btn-primary block full-width m-b">Ingresar</button>

                    <p id="txtResult"></p>
                    <a href="#"><small>¿Olvidaste tu Contraseña?</small></a>
                </form>
                <p class="m-t"><small>TexCross - Copyright 2018</small></p>
            </div>
        </div>


        <!-- Footer -->
        <!-- #include file="../footer.html" -->
        <!-- End Footer -->

    </body>

    <script>

        var app = new function () {

            this.el =
            {
                iDni: '#txtDni',
                iClave: '#pwdClave',
                btnAcceso: '#btnAcceso',
                txtResult: '#txtResult'
            }

            this.url = {
                default: '/'
            }

            this.lg = {

                Login: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Login", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; } 
                        return r;
                    } catch (e) { throw e; }
                }

            }

            this.init = async () => {
                this.on.click_acceso();
            }

            this.fn = {

                Login: async () => {

                    $(this.el.txtResult).text('');
                    this.on.click_acceso(false);
                    $(this.el.btnAcceso).html("<i class='fa fa-circle-o-notch fa-spin' aria-hidden='true'></i>")

                    try {

                        let obj = new Object();
                        obj.dni = $(this.el.iDni).val();
                        obj.clave = $(this.el.iClave).val();

                        let r = await this.lg.Login(obj);

                        if ('acceso' in r) {
                            window.location.href = this.url.default;
                        } else {
                            throw 'Dificultad vuelva a intentar';
                        }

                    } catch (e) {
                        $(this.el.txtResult).text(e);
                        this.on.click_acceso(true);
                        $(this.el.btnAcceso).text("Ingresar")
                    }

                }

            }

            this.on = {

                click_acceso: (life = true) => {
                    $(this.el.btnAcceso).off('click');
                    $(this.el.btnAcceso).prop('disabled', true);
                    if (life) {
                        $(this.el.btnAcceso).on('click', () => { this.fn.Login(); } );
                        $(this.el.btnAcceso).prop('disabled', false);
                    }
                }

            }
        }

        app.init();

    </script>

</html>