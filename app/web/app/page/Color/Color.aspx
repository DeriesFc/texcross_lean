﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/admin.Master" AutoEventWireup="true" CodeBehind="Color.aspx.cs" Inherits="web.app.page.Color.Color" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Color - Config • TexCross
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Color</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="javascript:;">Configuración</a></li>
                <li class="active"><strong>Color</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listado</h5>
                    <div class="ibox-tools">
                        <a class="tbl_btnRefresh" href="javascript:;">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="tbl_btnAdd" href="javascript:;">
                            <i class="fa fa-plus"></i>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div id="tblColor" class="col-xs-12"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Region Modal -->
    <div id="MD_InsColor" class="modal modal-dialog-center fade" aria-hidden="true" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body pt-20 pb-20">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="bd-bottom-gray-1 font-bold pb-5 mt-0 mb-10">Agregar</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Nombre</h5>
                                <input type="text" placeholder="Nombre" class="MDColor_nombre form-control" />
                            </div>
                        </div>
                    </div>
                    <hr class="mt-5 mb-10 ccc" />
                    <div class="row">
                        <div class="col-xs-12 tRight">
                            <button type="button" class="MDColor_guardar btn btn-sm btn-primary">Guardar</button>
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="MD_UpdColor" class="modal modal-dialog-center fade" aria-hidden="true" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body pt-20 pb-20">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="bd-bottom-gray-1 font-bold pb-5 mt-0 mb-10">Editar</h4>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" class="MDColor_id form-control" />
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Nombre</h5>
                                <input type="text" placeholder="Nombre" class="MDColor_nombre form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group mb-10">
                                <h5 class="font-bold mt-0 mb-5">Estado</h5>
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="MDColor_estado onoffswitch-checkbox" id="MD_UpdColorEstado">
                                        <label class="onoffswitch-label" for="MD_UpdColorEstado">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-5 mb-10 ccc" />
                    <div class="row">
                        <div class="col-xs-12 tRight">
                            <button type="button" class="MDColor_update btn btn-sm btn-primary">Actualizar</button>
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- endRegion Modal -->



</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <!-- HandleBars-->
    <script id="HB_tblColor_Load" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </script>

    <script id="HB_tblColor_Msg" type="text/x-handlebars-template">
        <div class="col-xs-12 p-0 mt-10 tCenter">
            <span class="font-14">{{msg}}</span>
        </div>
    </script>

    <script id="HB_tblColor" type="text/x-handlebars-template">

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {{#each arr}}
                <tr data-id="{{Id}}">
                    <td>{{Nombre}}</td>
                    <td class="tCenter">
                        <button type="button" class="tbl_upd btn btn-xs btn-info"><i class="fa fa-pencil"></i></button>
                        <button type="button" class="tbl_estado btn btn-xs {{#if Estado}}btn-warning{{else}}btn-danger{{/if}}"><i class="fa {{#if Estado}}fa-eye{{else}}fa-eye-slash{{/if}}"></i></button>
                    </td>
                </tr>
                {{/each}}
            </tbody>
        </table>

    </script>

    <script type="text/javascript">

        var app = new function () {

            this.el = {
                divTbl: '#tblColor',
                tblRefresh: '.tbl_btnRefresh',
                tblAdd: '.tbl_btnAdd',
                md_InsColor: '#MD_InsColor',
                md_UpdColor: '#MD_UpdColor',
            }

            this.lg = {
                getLis: async () => {
                    try {
                        let r = await jsServer.ARes("Color_Lis");
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                getColor: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Color_Get", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                addColor: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Color_Ins", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
                updColor: async (obj) => {
                    try {
                        let r = await jsServer.ARes("Color_Upd", obj);
                        if (!jsServer.Valid(r)) { throw r.Mensaje; }
                        return r;
                    } catch (e) { throw e; }
                },
            }

            this.init = async () => {

                this.fn.getLis();
                this.on.click_openAdd();
                this.on.click_refresh();

            }

            this.fn = {

                getLis: async () => {

                    try {

                        let html = Handlebars.compile($('#HB_tblColor_Load').html());
                        $(this.el.divTbl).html(html());

                        let r = await this.lg.getLis();

                        html = Handlebars.compile($('#HB_tblColor').html());
                        $(this.el.divTbl).html(html({ arr: r }));

                        util.dataTable.init(`${this.el.divTbl} table`);

                        this.on.click_openUpd();

                    } catch (e) {
                        let html = Handlebars.compile($('#HB_tblColor_Msg').html());
                        $(this.el.divTbl).html(html({ msg: e }));
                    }

                },

                open_add: async () => {
                    try {

                        $(`${this.el.md_InsColor} .MDColor_nombre`).val('');

                        this.on.click_saveAdd();

                        $(this.el.md_InsColor).modal('show');

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Algo salió mal');
                    }
                },

                save_add: async () => {
                    try {

                        jsUtil.Toastr.Remove();

                        let obj = new Object();
                        obj.Nombre = $(`${this.el.md_InsColor} .MDColor_nombre`).val() || null;

                        if (obj.Nombre === null) { throw 'Debe ingresar un nombre'; }

                        let r = await this.lg.addColor({ obj });

                        $('#MD_InsColor').modal('hide')
                        jsUtil.Toastr.Show('s', 'Agregado', r.Nombre);
                        this.fn.getLis();

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                },

                open_upd: async (item) => {
                    try {

                        let id = $(item).parents('tr').data('id');
                        let r = await this.lg.getColor({ id });

                        $(`${this.el.md_UpdColor} .MDColor_id`).val(r.Id);
                        $(`${this.el.md_UpdColor} .MDColor_nombre`).val(r.Nombre);
                        $(`${this.el.md_UpdColor} .MDColor_estado`).prop('checked', r.Estado);

                        this.on.click_saveUpd();
                        $(this.el.md_UpdColor).modal('show');
                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', 'Algo salió mal');
                    }
                },

                save_upd: async () => {
                    try {

                        jsUtil.Toastr.Remove();

                        let obj = new Object();
                        obj.Id = $(`${this.el.md_UpdColor} .MDColor_id`).val() || null;
                        if (obj.Id === null) { throw 'Vuelva a intentar'; }
                        obj.Nombre = $(`${this.el.md_UpdColor} .MDColor_nombre`).val() || null;
                        if (obj.Nombre === null) { throw 'Debe ingresar un nombre'; }
                        obj.Estado = $(`${this.el.md_UpdColor} .MDColor_estado`).is(':checked');

                        let r = await this.lg.updColor({ obj });

                        $(this.el.md_UpdColor).modal('hide')
                        jsUtil.Toastr.Show('s', 'Actualizado', r.Nombre);
                        this.fn.getLis();

                    } catch (e) {
                        jsUtil.Toastr.Show('w', 'Dificultad', e);
                    }
                }

            }

            this.on = {
                click_refresh: (life = true) => {
                    $(this.el.tblRefresh).off('click');
                    if (life) { $(this.el.tblRefresh).on('click', () => { this.fn.getLis(); }); }
                },
                click_openAdd: (life = true) => {
                    $(this.el.tblAdd).off('click');
                    if (life) { $(this.el.tblAdd).on('click', () => this.fn.open_add()); }
                },
                click_saveAdd: (life = true) => {
                    $(`${this.el.md_InsColor} .MDColor_guardar`).off('click');
                    if (life) { $(`${this.el.md_InsColor} .MDColor_guardar`).on('click', () => this.fn.save_add()); }
                },
                click_openUpd: (life = true) => {
                    $(`${this.el.divTbl}`).off('click', '.tbl_upd');
                    if (life) { $(`${this.el.divTbl}`).on('click', '.tbl_upd', function (e) { app.fn.open_upd(e.target) }); }
                },
                click_saveUpd: (life = true) => {
                    $(`${this.el.md_UpdColor} .MDColor_update`).off('click');
                    if (life) { $(`${this.el.md_UpdColor} .MDColor_update`).on('click', () => this.fn.save_upd()); }
                },
            }

        }

        var util = new function () {

            this.dataTable = {

                init: async (item) => {

                    $(item).DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        language: jsUtil.DTable.sp,
                        ordering: true,
                        order: [[0, "asc"]],
                        pageLength: 10,
                        responsive: true,
                        aLengthMenu: [
                            [10, 25, 50, 100, 200, -1],
                            [10, 25, 50, 100, 200, "All"]
                        ],
                        buttons: [
                            { extend: 'copy' }, { extend: 'csv' }, { extend: 'excel', title: 'ExampleFile' }, { extend: 'pdf', title: 'ExampleFile' },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');
                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ],
                        destroy: true
                    });
                }
            }
        }

        app.init();

    </script>

</asp:Content>
