﻿using System;
using System.Web;

namespace web.code
{
    public class Session
    {

        public enum list
        {
            #region Persona
            Id,
            Dni,
            Nombre,
            Apellido,
            Estado,
            #endregion
            #region Tipo
            TipoId,
            TipoNombre,
            #endregion
            #region Area
            AreaId,
            AreaNombre,
            TipoAreaId,
            TipoAreaNombre
            #endregion
        }

        public static object Get(list item)
        {
            try
            {
                var ss = HttpContext.Current.Session[Enum.GetName(typeof(list), item)];
                if (ss == null) { return null; } else { return ss; }
            }
            catch (Exception) { throw; }
        }

        public static void Set(list name, object value)
        {
            try { HttpContext.Current.Session[Enum.GetName(typeof(list), name)] = value; }
            catch (Exception) { throw; }
        }

        internal static void Clean()
        {
            try { HttpContext.Current.Session.Clear(); }
            catch (Exception) { throw; }
        }

        public static void Exists()
        {
            try {
                if (Session.Get(list.Id) == null) {
                    HttpContext.Current.Response.RedirectToRoute("Login");
                }
            }
            catch (Exception) { throw; }
        } 

    }

}