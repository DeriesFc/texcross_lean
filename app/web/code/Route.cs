﻿using System.Web.Routing;

namespace web.code
{
    public class Route {

        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.MapPageRoute("Default", "", "~/app/page/Dashboard/Default.aspx");
            routes.MapPageRoute("Inicio", "Inicio", "~/app/page/Dashboard/Default.aspx");

            routes.MapPageRoute("Login", "Login", "~/app/page/Login.aspx");

            #region Color
            routes.MapPageRoute("Config/Color", "Config/Color", "~/app/page/Color/Color.aspx");
            #endregion

            #region Unidad
            routes.MapPageRoute("Config/Unidad", "Config/Unidad", "~/app/page/Unidad/Unidad.aspx");
            #endregion

            #region Suministro
            routes.MapPageRoute("Almacen/Suministro", "Almacen/Suministro", "~/app/page/Suministro/Suministro.aspx");
            #endregion

            #region SuministroTipo
            routes.MapPageRoute("Config/Suministro/Tipo", "Config/Suministro/Tipo", "~/app/page/Suministro/Tipo.aspx");
            #endregion

            #region Area
            routes.MapPageRoute("Config/Area/Tipo", "Config/Area/Tipo", "~/app/page/Area/Tipo.aspx");
            routes.MapPageRoute("TexCross/Area", "TexCross/Area", "~/app/page/Area/Area.aspx");
            #endregion


            #region Modelo
            routes.MapPageRoute("Diseno/Modelo", "Diseno/Modelo", "~/app/page/Modelo/Modelo.aspx");
            routes.MapPageRoute("Diseno/Modelo/Suministro", "Diseno/Modelo/Suministro/{id}", "~/app/page/Modelo/Suministro.aspx");
            routes.MapPageRoute("Diseno/Modelo/Operacion", "Diseno/Modelo/Operacion/{id}", "~/app/page/Modelo/Operacion.aspx");
            #endregion

            #region Pedido
            routes.MapPageRoute("Proyecto/Listar", "Proyecto/Listar", "~/app/page/Proyecto/Listar.aspx");
            routes.MapPageRoute("Proyecto/Crear", "Proyecto/Crear", "~/app/page/Proyecto/Crear.aspx");
            routes.MapPageRoute("Proyecto/Pedido", "Proyecto/{cPedido}", "~/app/page/Proyecto/Pedido.aspx");
            routes.MapPageRoute("Proyecto/Pedido/PedidoDetalle", "Proyecto/{cPedido}/{cPedidoDetalle}", "~/app/page/Proyecto/PedidoDetalle.aspx");
            routes.MapPageRoute("Proyecto/Pedido/PedidoDetalle/PedidoDetalleControl", "Proyecto/{cPedido}/{cPedidoDetalle}/{cPedidoDetalleControl}", "~/app/page/Proyecto/PedidoDetalleControl.aspx");
            #endregion

            #region Reporte
            routes.MapPageRoute("Reporte/General", "Reporte/General", "~/app/page/Reporte/Rp_General.aspx");
            routes.MapPageRoute("Reporte/ColaboradorPago", "Reporte/ColaboradorPago", "~/app/page/Reporte/Rp_ColaboradorPago.aspx");
            #endregion


            //routes.MapPageRoute("Login", "Login", "~/app/html/Login.aspx");
            //routes.MapPageRoute("LockScreen", "LockScreen", "~/app/html/LockScreen.aspx");

            ////Inicio
            //routes.MapPageRoute("Default", "", "~/app/html/Dashboard/Dashboard.aspx");
            //routes.MapPageRoute("Dashboard", "Dashboard", "~/app/html/Dashboard/Dashboard.aspx");

            ///* Incidente */
            //routes.MapPageRoute("Incident", "Incident", "~/app/html/Incident/Incident.aspx"); // Incidente
            //routes.MapPageRoute("Incident-Add", "Incident/Add", "~/app/html/Incident/IncidentAdd.aspx"); // IncidenteAdd
            //routes.MapPageRoute("Incident-Upd", "Incident/Upd/{cod}", "~/app/html/Incident/IncidentUpd.aspx"); // IncidenteUpd
            //routes.MapPageRoute("Incident-Sel", "Incident/Sel/{cod}", "~/app/html/Incident/IncidentSel.aspx"); // IncidenteSel

            ///* Administracion de la configuración */
            //routes.MapPageRoute("Job", "Administration/Config/Job", "~/app/html/Job/Job.aspx"); // Job
            //routes.MapPageRoute("Company", "Administration/Config/Company", "~/app/html/Company/Company.aspx"); // Company
            //routes.MapPageRoute("Company-Service", "Administration/Config/Company/Service", "~/app/html/Company/CompanyService.aspx"); // Company
            //routes.MapPageRoute("Office", "Administration/Config/Office", "~/app/html/Office/Office.aspx"); // Office
            //routes.MapPageRoute("Urgency", "Administration/Config/Urgency", "~/app/html/Urgency/Urgency.aspx"); // Urgency
            //routes.MapPageRoute("Impact", "Administration/Config/Impact", "~/app/html/Impact/Impact.aspx"); // Impact
            //routes.MapPageRoute("Priority", "Administration/Config/Priority", "~/app/html/Priority/Priority.aspx"); // Priority
            //routes.MapPageRoute("Service", "Administration/Config/Service", "~/app/html/Service/Service.aspx"); // Service
            //routes.MapPageRoute("SLA", "Administration/Config/SLA", "~/app/html/SLA/SLA.aspx"); // Service

            ///* Configuración */
            //routes.MapPageRoute("UserType", "Administration/Config/UserType", "~/app/html/UserType/UserType.aspx"); // Priority
            //routes.MapPageRoute("People", "Administration/Config/People", "~/app/html/People/People.aspx"); // Priority

            ////Error
            //routes.MapPageRoute("404", "404", "~/app/html/404.aspx");
            //routes.MapPageRoute("500", "500", "~/app/html/500.aspx");
            //routes.MapPageRoute("Error", "Error", "~/app/html/Error.aspx");
        }

    }
}