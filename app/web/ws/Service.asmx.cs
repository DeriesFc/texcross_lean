﻿using web;
using Entity;
using Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace web.ws
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class Service : WebService
    {

        public Service() { }

        #region Login
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Login(string dni, string clave)
        {
            try
            {
                entPersona.Persona r = new lgPersona.Login().Auth(dni, clave);

                code.Session.Clean();
                code.Session.Set(code.Session.list.Id, r.Id);
                code.Session.Set(code.Session.list.Dni, r.Dni);
                code.Session.Set(code.Session.list.Nombre, r.Nombre);
                code.Session.Set(code.Session.list.Apellido, r.Apellido);
                code.Session.Set(code.Session.list.Estado, r.Estado);
                code.Session.Set(code.Session.list.TipoId, r.Tipo.Id);
                code.Session.Set(code.Session.list.TipoNombre, r.Tipo.Nombre);
                code.Session.Set(code.Session.list.AreaId, r.Area.Id);
                code.Session.Set(code.Session.list.AreaNombre, r.Area.Nombre);
                code.Session.Set(code.Session.list.TipoAreaId, r.Area.Tipo.Id);
                code.Session.Set(code.Session.list.TipoAreaNombre, r.Area.Tipo.Nombre);

                return JsonConvert.SerializeObject(new { acceso = true });
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public void PartnerTarifa_Lis_s2(string q, int idPartner, int idPaquete)
        //{
        //    Context.Response.ContentType = "application/json; charset=utf-8";

        //    var temp = _Session.Validate();
        //    if (temp != string.Empty) { Context.Response.Write(temp); }
        //    else
        //    {
        //        var rpt = new JavaScriptSerializer().Serialize(new NegocioPartner.Tarifa_S().Lis(q, idPartner, idPaquete));
        //        Context.Response.Write(rpt);
        //    }
        //    Context.Response.End();
        //}



        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public string PartnerServicio_Lisc(int idPaquete, int idPartner, int idTarifa)
        //{
        //    try
        //    {
        //        {
        //            var temp = _Session.Validate();
        //            if (temp != string.Empty) { return temp; }
        //        }//Validate Session

        //        var r = new NegocioPartner.Servicio().Lis(idPaquete, idPartner, idTarifa);
        //        return new JavaScriptSerializer().Serialize(r);
        //    }
        //    catch (PersonalizedException ex) { return _Exception.jsonEx(ex.Message, ex.Message); }
        //    catch (Exception ex) { return _Exception.jsonEx("Error...", ex.Message); }
        //    finally { GC.Collect(); }
        //}

        #region Suministro
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Suministro_Lis()
        {
            try
            {
                IList<entSuministro.Suministro> r = new lgSuministro.Suministro().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Suministro_Get(int id)
        {
            try
            {
                entSuministro.Suministro r = new lgSuministro.Suministro().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Suministro_Ins(entSuministro.Suministro obj)
        {
            try
            {
                entSuministro.Suministro r = new lgSuministro.Suministro().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Suministro_Upd(entSuministro.Suministro obj)
        {
            try
            {
                entSuministro.Suministro r = new lgSuministro.Suministro().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region SuministroTipo
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SuministroTipo_Lis()
        {
            try
            {
                IList<entSuministro.Tipo> r = new lgSuministro.Tipo().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SuministroTipo_Get(int id)
        {
            try
            {
                entSuministro.Tipo r = new lgSuministro.Tipo().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SuministroTipo_Ins(entSuministro.Tipo obj)
        {
            try
            {
                entSuministro.Tipo r = new lgSuministro.Tipo().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SuministroTipo_Upd(entSuministro.Tipo obj)
        {
            try
            {
                entSuministro.Tipo r = new lgSuministro.Tipo().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Color
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Color_Lis()
        {
            try
            {
                IList<entColor.Color> r = new lgColor.Color().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Color_Get(int id)
        {
            try
            {
                entColor.Color r = new lgColor.Color().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Color_Ins(entColor.Color obj)
        {
            try
            {
                entColor.Color r = new lgColor.Color().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Color_Upd(entColor.Color obj)
        {
            try
            {
                entColor.Color r = new lgColor.Color().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Unidad
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Unidad_Lis()
        {
            try
            {
                IList<entUnidad.Unidad> r = new lgUnidad.Unidad().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Unidad_Get(int id)
        {
            try
            {
                entUnidad.Unidad r = new lgUnidad.Unidad().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Unidad_Ins(entUnidad.Unidad obj)
        {
            try
            {
                entUnidad.Unidad r = new lgUnidad.Unidad().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Unidad_Upd(entUnidad.Unidad obj)
        {
            try
            {
                entUnidad.Unidad r = new lgUnidad.Unidad().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region AreaTipo
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaTipo_Lis()
        {
            try
            {
                IList<entArea.Tipo> r = new lgArea.Tipo().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaTipo_Search(entArea.Tipo obj)
        {
            try
            {
                IList<entArea.Tipo> r = new lgArea.Tipo().Search(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaTipo_Get(int id)
        {
            try
            {
                entArea.Tipo r = new lgArea.Tipo().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaTipo_Ins(entArea.Tipo obj)
        {
            try
            {
                entArea.Tipo r = new lgArea.Tipo().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaTipo_Upd(entArea.Tipo obj)
        {
            try
            {
                entArea.Tipo r = new lgArea.Tipo().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Area
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Area_Lis()
        {
            try
            {
                IList<entArea.Area> r = new lgArea.Area().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Area_Get(int id)
        {
            try
            {
                entArea.Area r = new lgArea.Area().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Area_Ins(entArea.Area obj)
        {
            try
            {
                entArea.Area r = new lgArea.Area().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Area_Upd(entArea.Area obj)
        {
            try
            {
                entArea.Area r = new lgArea.Area().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Area Operacion
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaOperacion_Lis()
        {
            try
            {
                IList<entArea.Operacion> r = new lgArea.Operacion().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaOperacion_Get(int id)
        {
            try
            {
                entArea.Operacion r = new lgArea.Operacion().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaOperacion_Ins(entArea.Operacion obj)
        {
            try
            {
                entArea.Operacion r = new lgArea.Operacion().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AreaOperacion_Upd(entArea.Operacion obj)
        {
            try
            {
                entArea.Operacion r = new lgArea.Operacion().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Modelo Tipo
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloTipo_Lis()
        {
            try
            {
                IList<entModelo.Tipo> r = new lgModelo.Tipo().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloTipo_Get(int id)
        {
            try
            {
                entModelo.Tipo r = new lgModelo.Tipo().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloTipo_Ins(entModelo.Tipo obj)
        {
            try
            {
                entModelo.Tipo r = new lgModelo.Tipo().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloTipo_Upd(entModelo.Tipo obj)
        {
            try
            {
                entModelo.Tipo r = new lgModelo.Tipo().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Modelo
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Modelo_Lis()
        {
            try
            {
                IList<entModelo.Modelo> r = new lgModelo.Modelo().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Modelo_LisValid()
        {
            try
            {
                IList<entModelo.Modelo> r = new lgModelo.Modelo().LisValid();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Modelo_Get(int id)
        {
            try
            {
                entModelo.Modelo r = new lgModelo.Modelo().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Modelo_Ins(entModelo.Modelo obj)
        {
            try
            {
                entModelo.Modelo r = new lgModelo.Modelo().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Modelo_Upd(entModelo.Modelo obj)
        {
            try
            {
                entModelo.Modelo r = new lgModelo.Modelo().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Modelo Suministro
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloSuministro_Lis(int id)
        {
            try
            {
                IList<entModelo.Suministros> r = new lgModelo.Suministros().Lis(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloSuministro_Get(int id)
        {
            try
            {
                entModelo.Suministros r = new lgModelo.Suministros().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloSuministro_Ins(entModelo.Suministros obj)
        {
            try
            {
                entModelo.Suministros r = new lgModelo.Suministros().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloSuministro_Upd(entModelo.Suministros obj)
        {
            try
            {
                entModelo.Suministros r = new lgModelo.Suministros().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloSuministro_Del(int id)
        {
            try
            {
                Object r = new lgModelo.Suministros().Del(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Modelo Operacion
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloOperacion_Lis(int id)
        {
            try
            {
                IList<entModelo.Operaciones> r = new lgModelo.Operaciones().Lis(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloOperacion_Get(int id)
        {
            try
            {
                entModelo.Operaciones r = new lgModelo.Operaciones().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloOperacion_Ins(entModelo.Operaciones obj)
        {
            try
            {
                entModelo.Operaciones r = new lgModelo.Operaciones().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloOperacion_Upd(entModelo.Operaciones obj)
        {
            try
            {
                entModelo.Operaciones r = new lgModelo.Operaciones().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ModeloOperacion_Del(int id)
        {
            try
            {
                Object r = new lgModelo.Operaciones().Del(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Talla Tipo
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string TallaTipo_Lis()
        {
            try
            {
                IList<entTalla.Tipo> r = new lgTalla.Tipo().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string TallaTipo_Get(int id)
        {
            try
            {
                entTalla.Tipo r = new lgTalla.Tipo().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string TallaTipo_Ins(entTalla.Tipo obj)
        {
            try
            {
                entTalla.Tipo r = new lgTalla.Tipo().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string TallaTipo_Upd(entTalla.Tipo obj)
        {
            try
            {
                entTalla.Tipo r = new lgTalla.Tipo().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Talla
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Talla_Lis()
        {
            try
            {
                IList<entTalla.Talla> r = new lgTalla.Talla().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Talla_Get(int id)
        {
            try
            {
                entTalla.Talla r = new lgTalla.Talla().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Talla_Ins(entTalla.Talla obj)
        {
            try
            {
                entTalla.Talla r = new lgTalla.Talla().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Talla_Upd(entTalla.Talla obj)
        {
            try
            {
                entTalla.Talla r = new lgTalla.Talla().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Cliente
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cliente_Lis()
        {
            try
            {
                IList<entCliente.Cliente> r = new lgCliente.Cliente().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cliente_Get(int id)
        {
            try
            {
                entCliente.Cliente r = new lgCliente.Cliente().Get(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cliente_Ins(entCliente.Cliente obj)
        {
            try
            {
                entCliente.Cliente r = new lgCliente.Cliente().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cliente_Upd(entCliente.Cliente obj)
        {
            try
            {
                entCliente.Cliente r = new lgCliente.Cliente().Upd(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Pedido
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Pedido_Lis()
        {
            try
            {
                IList<entPedido.PedidoVO> r = new lgPedido.Pedido().Lis();
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Pedido_Ins(entPedido.Pedido obj)
        {
            try
            {
                entPedido.Pedido r = new lgPedido.Pedido().Ins(obj);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Pedido_GetByCode(string code)
        {
            try
            {
                entPedido.PedidoVO r = new lgPedido.Pedido().GetByCode(code);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region PedidoDetalle
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PedidoDetalle_Lis(int id)
        {
            try
            {
                IList<entPedido.PedidoDetalleVO> r = new lgPedido.PedidoDetalle().Lis(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PedidoDetalle_GetByCode(int idPedido,string code)
        {
            try
            {
                entPedido.PedidoDetalleVO r = new lgPedido.PedidoDetalle().GetByCode(idPedido, code);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region PedidoDetalleControl
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PedidoDetalleControl_Lis(int id)
        {
            try
            {
                IList<entPedido.PedidoDetalleControlVO> r = new lgPedido.PedidoDetalleControl().Lis(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region PedidoDetalleControlDetalle
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PedidoDetalleControlDetalle_Lis(int id)
        {
            try
            {
                IList<entPedido.PedidoDetalleControlDetalleVO> r = new lgPedido.PedidoDetalleControlDetalle().Lis(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region PedidoDetalleControlDetalle
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PedidoDetalleControlDetalle_GetByCode(int id)
        {
            try
            {
                entPedido.PedidoDetalleControlDetalleVO r = new lgPedido.PedidoDetalleControlDetalle().GetByCode(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PedidoDetalleControlDetalle_Terminar(int id, int persona)
        {
            try
            {
                Object r = new lgPedido.PedidoDetalleControlDetalle().Terminar(id, persona);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Persona
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Persona_LisByOperacion(int id)
        {
            try
            {
                IList<entPersona.PersonaS2VO> r = new lgPersona.Persona().LisByOperacion(id);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Reporte
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Reporte_General(DateTime start, DateTime end)
        {
            try
            {
                IList<entReporte.General> r = new lgReporte.General().Lis(start, end);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Reporte_ColaboradorPago(DateTime start, DateTime end)
        {
            try
            {
                IList<entReporte.ColaboradorPago> r = new lgReporte.ColaboradorPago().Lis(start, end);
                return JsonConvert.SerializeObject(r);
            }
            catch (Personalized.PException ex) { return JsonConvert.SerializeObject(ex.Obj); }
            catch (Exception ex) { return JsonConvert.SerializeObject(new Personalized.PClass() { Mensaje = ex.Message }); }
            finally { GC.Collect(); }
        }
        #endregion

    }
}
