﻿'use strict';

var jsBase = new function () { };

var jsUtil = new function () {

    this.DTable = {
        sp: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } 

    this.Toastr = new function () {

        toastr.options = {
            closeButton: true,
            progressBar: true,
            progressClass: 'toast-progress',
            positionClass: 'toast-top-right',
        }

        // s ::: Status
        // t ::: Type    :::  { 1:Success, 2:Info, 3:Warning, 4:Error }
        // m ::: Message ::: Text to show
        // c ::: Console ::: Text to show in console
        // o ::: TimeOut ::: Time to disappear
        this.Show = (s, t, m, o = 0, c = '') => {

            //jsUtil.Notify.Remove();
            switch (s) {
                case 's': toastr.success(m, t, { timeOut: ((o == 0) ? 4000 : o) }); break;
                case 'i': toastr.info(m, t, { timeOut: ((o == 0) ? 4000 : o) }); break;
                case 'w': toastr.warning(m, t, { timeOut: ((o == 0) ? 4000 : o) }); break;
                case 'd': toastr.error(m, t, { timeOut: ((o == 0) ? 4000 : o) }); break;
            }
            if (c !== '') { console.log(c); }
        }
        this.Remove = () => { toastr.remove(); }
        this.Clear = () => { toastr.clear(); }
    }//Notify

    this.Select = new function () {
        this.init = (id) => { $(id).select2({ width: '100%' }); }
        this.Clear = (id) => { $(id).find('option').remove().end(); }
        this.SetVal = (id, val) => { $(id).val(val) }
        this.Refresh = async (id) => { await $(id).trigger('change'); }
        this.Disabled = async (i, s = true) => { await $(i).prop("disabled", s); }
        this.LoadObj = async function (id, v, n, upd = true) {
            $(id).append(`<option value="${v}">${n}</option>`);
            if (upd) { await this.Refresh(id); }
        }
        this.LoadArr = async (id, data, val, param, upd = true) => {

            this.Disabled(id, false);
            jsUtil.Select.Clear(id);
            let params = arguments;

            $(data).each(function () {
                let rpt = "";
                if (params.length > 4) {
                    for (var x = 3; x < params.length; x++) {
                        if (x != 3) { rpt = rpt + ` - `; }
                        rpt = rpt + `${this[params[x]]}`;
                    }
                } else { rpt = this[param]; }
                $(id).append(`<option value="${this[val]}">${rpt}</option>`);
            });

            if (upd) { await this.Refresh(id); }
        }

        //this.LoadArr = async (id, data, val, param, upd = true) => {

        //    this.Disabled(id, false);
        //    jsUtil.Select.Clear(id);
        //    let params = arguments;

        //    $(data).each(function () {
        //        let rpt = "";
        //        if (params.length > 4) {
        //            for (var x = 3; x < params.length; x++) {
        //                if (x != 3) { rpt = rpt + ` - `; }
        //                rpt = rpt + `${this[params[x]]}`;
        //            }
        //        } else { rpt = this[param]; }
        //        $(id).append(`<option value="${this[val]}">${rpt}</option>`);
        //    });

        //    if (upd) { await this.Refresh(id); }
        //}
        this.Loading = async (id, e = false, r = true) => {
            await $(id)
                .prop("disabled", true)
                .find('option').remove().end()
                .append(`<option value="">${((e) ? 'Error...' : 'Cargando...')}</option>`);
            if (r) { await this.Refresh(id); }
        }
    }

};

var jsServer = new function () {

    this.SReq = (r, d = {}) => {
        try {
            let req = Sync(r, JSON.stringify(d));
            return JSON.parse(req.d);
        } catch (e) { return Error(e); }
    }

    this.ARes = async (r, d = {}) => {
        try {
            let req = await Async(r, JSON.stringify(d));
            return JSON.parse(req.d);
        } catch (e) { return Error(e); }
    }

    this.Valid = (d) => {
        let rpt = true;
        try { rpt = ('SvcError' in d) ? false : true; }
        catch (e) { rpt = true; }
        finally { return rpt; }
    }

    /* Private */

    const Path = "/ws/Service.asmx/";

    const Error = (ex) => { return { SvcError: true, Mensaje: ex.status } };

    const Sync = function (r, d) {

        let rpt = null;

        $.ajaxSetup({
            url: Path.concat(r),
            type: 'POST',
            async: false,
            dataType: 'json',
            data: d,
            contentType: 'application/json; charSet=UTF-8',
        });

        $.ajax({ success: (res) => { rpt = res; }, error: (q, w, e) => { } });
        return rpt;
    }

    const Async = async function (r, d) {

        let rpt = null;

        $.ajaxSetup({
            url: Path.concat(r),
            type: 'POST',
            async: true,
            dataType: 'json',
            data: d,
            contentType: 'application/json; charSet=UTF-8',
        });

        await $.ajax({ success: (res) => { rpt = res; }, error: (q, w, e) => { } });
        return rpt;
    }

}